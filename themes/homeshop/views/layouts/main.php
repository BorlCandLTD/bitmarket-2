<?php

	Yii::import('application.modules.store.components.SCompareProducts');
	Yii::import('application.modules.store.models.wishlist.StoreWishlist');

	$assetsManager = Yii::app()->clientScript;
	$assetsManager->registerCoreScript('jquery');
	$assetsManager->registerCoreScript('jquery.ui');

	// jGrowl notifications
	Yii::import('ext.jgrowl.Jgrowl');
	Jgrowl::register();

	// Disable jquery-ui default theme
	$assetsManager->scriptMap=array(
		'jquery-ui.css'=>false,
	);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo CHtml::encode($this->pageTitle) ?></title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo CHtml::encode($this->pageDescription) ?>">
	<meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords) ?>">
	
	
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,400italic,500,700,700italic,900,500italic&subset=cyrillic-ext,latin' rel='stylesheet' type='text/css'>
	
	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/perfect-scrollbar.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/flexslider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/jquery.nouislider.min.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/fontello.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/settings.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/animation.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/owl.carousel.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/owl.theme.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/chosen.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/style.css" />
	
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/ie.css">
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/fontello-ie7.css">
	<![endif]-->
	
	
	<!-- JavaScript -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.raty.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.liblink.js" data-cover></script>
	
	<!-- Scroll Bar -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/perfect-scrollbar.min.js"></script>
	
	<!-- Cloud Zoom -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/zoomsl-3.0.min.js"></script>
	
	<!-- FancyBox -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.fancybox.pack.js"></script>
	
	<!-- jQuery REVOLUTION Slider  -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.themepunch.revolution.min.js"></script>

	<!-- FlexSlider -->
	<script defer src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/flexslider.min.js"></script>
	
	<!-- IOS Slider -->
	<script src = "<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.iosslider.min.js"></script>
	
	<!-- noUi Slider -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.nouislider.min.js"></script>
	
	<!-- Owl Carousel -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/owl.carousel.min.js"></script>
	
	<!-- Cloud Zoom -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/zoomsl-3.0.min.js"></script>
	
	<!-- SelectJS -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/chosen.jquery.min.js" type="text/javascript"></script>
	
	<!-- Main JS -->
	<script defer src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/main-script.js"></script>
	
	
	
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jqueryui/css/custom-theme/jquery-ui-1.8.19.custom.css">
	<!--link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/reset.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/style.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/catalog.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/forms.css">

	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/common.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/menu.js"></script-->

	
	
</head>
<body>

<?php
	// Notifier module form
	Yii::import('application.modules.notifier.NotifierModule');
	NotifierModule::renderDialog();
?>


		<!-- Container -->
		<div class="container">
			
			<!-- Header -->
			<header class="row">
				
				<div class="col-lg-12 col-md-12 col-sm-12">
					
					<!-- Top Header -->
					<div id="top-header">
						
						<div class="row">
							
							<nav id="top-navigation" class="col-lg-7 col-md-7 col-sm-7">
								<?php
									$this->widget('zii.widgets.CMenu', array(
										'htmlOptions' => array( 'class' => 'pull-left'),
										'items'=>array(
											array('label'=>Yii::t('core', 'Помощь'), 'url'=>array('/pages/pages/view', 'url'=>'help')),
											array('label'=>Yii::t('core', 'Как сделать заказ'), 'url'=>array('/pages/pages/view', 'url'=>'how-to-create-order')),
											array('label'=>Yii::t('core', 'Гарантия'), 'url'=>array('/pages/pages/view', 'url'=>'garantiya')),
											array('label'=>Yii::t('core', 'Доставка и оплата'), 'url'=>array('/pages/pages/view', 'url'=>'dostavka-i-oplata')),
											array('label'=>Yii::t('core', 'Обратная связь'), 'url'=>array('/feedback/default/index')),
										),
									));
								?>
							</nav>
							
							<nav class="col-lg-5 col-md-5 col-sm-5">
								<ul class="pull-right">
									<?php if(Yii::app()->user->isGuest): ?>
										<li class="purple"><?php echo CHtml::link(Yii::t('core','<i class="icons icon-user-3"></i> Вход'), array('/users/login/login')) ?>	
											<!--ul id="login-dropdown" class="box-dropdown">
												<li>
													<?php echo CHtml::form($this->createUrl('/users/login'),'post', array('id'=>'user-login-form')); ?>
														<div class="box-wrapper">
															<h4>ВХОД</h4>
															<div class="iconic-input">
																<input type="text" placeholder="Имя пользователя">
																<i class="icons icon-user-3"></i>
															</div>
															<div class="iconic-input">
																<input type="text" placeholder="Пароль">
																<i class="icons icon-lock"></i>
															</div>
															<input type="checkbox" id="loginremember"> <label for="loginremember">Запомнить меня</label>
															<br>
															<br>
															<div class="pull-left">
																<input type="submit" class="orange" value="Войти">
															</div>
															<div class="pull-right">
																<?php echo CHtml::link(Yii::t('UsersModule', 'Забыли пароль?'), array('/users/remind')) ?>
																<!--a href="#">Забыли пароль?</a>
																<br>
																<a href="#">Забыли имя пользователя?</a>
																<br>
															</div>
															<br class="clearfix">
														</div>
														<div class="footer">
															<h4 class="pull-left">НОВЫЙ ПОКУПАТЕЛЬ?</h4>
															<?php echo CHtml::link(Yii::t('UsersModule', 'Регистрация'), array('/users/register'), array('class'=>'button pull-right')) ?>
															<!--a class="button pull-right" href="create_an_account.html">Регистрация</a>
														</div>
													<?php echo CHtml::endForm(); ?>
												</li>
											</ul-->
										</li>
										<li><?php echo CHtml::link(Yii::t('core','<i class="icons icon-lock"></i> Регистрация'), array('/users/register')) ?></li>
										
									<?php else: ?>
										<li><?php echo CHtml::link(Yii::t('core','<i class="icons icon-user-3"></i> Личный кабинет'), array('/users/profile/index')) ?></li>
										<li><?php echo CHtml::link(Yii::t('core','<i class="icons icon-bag"></i> Мои заказы'), array('/users/profile/orders')) ?></li>
										<li><?php echo CHtml::link(Yii::t('core','<i class="icons icon-logout-1"></i> Выход'), array('/users/login/logout')) ?></li>
									<?php endif; ?>
									
									
								</ul>
							</nav>
							
						</div>
						
					</div>
					<!-- /Top Header -->
					
					
					
					<!-- Main Header -->
					<div id="main-header">
						
						<div class="row">
							
							<div id="logo" class="col-lg-4 col-md-4 col-sm-4">
								<a href="/"><img id="logo" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/logo.png"></a>
							</div>
							
							<nav id="middle-navigation" class="col-lg-8 col-md-8 col-sm-8">
								<ul class="pull-right">
									<li class="blue">
										<a href="<?php echo Yii::app()->createUrl('/store/compare/index') ?>">
											<i class="icons icon-docs"></i><?php echo Yii::t('core', '{c} товаров', array('{c}'=>SCompareProducts::countSession())) ?>
										</a>
									</li>
									<li class="red">
										<a href="<?php echo Yii::app()->createUrl('/store/wishlist/index') ?>">
											<i class="icons icon-heart-empty"></i><?php echo Yii::t('core', '{c} товаров', array('{c}'=>StoreWishlist::countByUser())) ?>
										</a>
									</li>
									<li class="orange" id="cart">
										<?php $this->renderFile(Yii::getPathOfAlias('orders.views.cart._small_cart').'.php'); ?>
									</li>
									<!--li><a class="flag" href="#"><span class="english-flag"></span>English</a>
										<ul class="box-dropdown parent-arrow">
											<li>
												<div class="box-wrapper no-padding parent-border">
													<table class="language-table">
														<tr>
															<td class="flag"><span class="english-flag"></span></td>
															<td class="country"><a href="#">English</a></td>
														</tr>
														<tr>
															<td class="flag"><span class="german-flag"></span></td>
															<td class="country"><a href="#">German</a></td>
														</tr>
														<tr>
															<td class="flag"><span class="french-flag"></span></td>
															<td class="country"><a href="#">French</a></td>
														</tr>
														<tr>
															<td class="flag"><span class="italian-flag"></span></td>
															<td class="country"><a href="#">Italian</a></td>
														</tr>
														<tr>
															<td class="flag"><span class="spanish-flag"></span></td>
															<td class="country"><a href="#">Spanish</a></td>
														</tr>
													</table>
												</div>
											</li>
										</ul>
										
									</li-->
									<li><a href="#"><i class="icons icon-<?php echo Yii::app()->currency->active->iso; ?>"></i><?php echo Yii::app()->currency->active->name; ?></a>
										<ul class="box-dropdown parent-arrow">
											<li>
												<div class="box-wrapper no-padding parent-border">
													<table class="currency-table">
													
														<?php foreach(Yii::app()->currency->currencies as $currency): ?>
														
															<tr>
																<td>
																<?php
																	echo CHtml::ajaxLink(Yii::t('core', '<i class="icons icon-{s}"></i> {n}', array('{s}'=>$currency->iso, '{n}'=>$currency->name)), '/store/ajax/activateCurrency/'.$currency->id, 
																		array('success'=>'js:function(){window.location.reload(true)}',),
																		array('id'=>'sw'.$currency->id,'class'=>Yii::app()->currency->active->id===$currency->id?'active':''));
																?>
																</td>
															</tr>
																
														<?php endforeach; ?>
													</table>
												</div>
											</li>
										</ul>
									</li>
								</ul>
							</nav>
							
						</div>
						
					</div>
					<!-- /Main Header -->
					
					
					<!-- Main Navigation -->
					<nav id="main-navigation" class="style1">
					
						<ul>
						<?php
							//Yii::import('application.modules.store.models.StoreCategory');
							$items = StoreCategory::model()->findByPk(1)->asCMenuArray();
							if(isset($items['items'])):
								
								foreach ($items['items'] as $item): ?>
								
									<li class="<?php echo !empty($item['color_code']) ? $item['color_code'] : 'green'; ?>">
										<a href="/<?php echo $item['url']['url']; ?>">
											<i class="icons icon-<?php echo !empty($item['icon_code']) ? $item['icon_code'] : 'na'; ?>"></i>
											<span class="nav-caption"><?php echo $item['label']; ?></span>
											<span class="nav-description"><?php echo $item['desc']; ?></span>
										</a>
										
										<?php if (isset($item['items'])): ?>
											<ul class="wide-dropdown normalAnimation">
												<?php foreach ($item['items'] as $subitem): ?>
													<li>
														<ul>
															<li><span class="nav-caption"><a href="/<?php echo $subitem['url']['url']; ?>"><i class="icons icon-<?php echo !empty($subitem['icon_code']) ? $subitem['icon_code'] : 'na'; ?>"></i><?php echo $subitem['label']; ?></a></span></li>
																<?php if (isset($subitem['items'])):
																	foreach ($subitem['items'] as $subsubitem): ?>
																		<li><a href="/<?php echo $subsubitem['url']['url']; ?>"><i class="icons icon-right-dir"></i> <?php echo $subsubitem['label']; ?></a></li>
																<?php endforeach;
																	endif; ?>
														</ul>
													</li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</li>
								
								<?php
								endforeach;
							endif;							
						?>
							
							<li class="green">
								<a href="/pages/news">
									<i class="icons icon-pencil-7"></i>
									<span class="nav-caption">Блог</span>
									<span class="nav-description">Новости & Обзоры</span>
								</a>
							</li>
							
							<!--li class="purple">
								<a href="contact.html">
									<i class="icons icon-location-7"></i>
									<span class="nav-caption">Contact</span>
									<span class="nav-description">Store Locations</span>
								</a>
							</li-->
							
							<li class="nav-search">
									<i class="icons icon-search-1"></i>
							</li>
							
						</ul>
						
						<div id="search-bar">
							<?php echo CHtml::form($this->createUrl('/store/category/search')) ?>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<table id="search-bar-table">
										<tr>
											<td class="search-column-1">
												<p><span class="grey">Популярные запросы:</span> <a href="#">аксессуары</a>, <a href="#">акустика</a>, <a href="#">камера</a>, <a href="#">телефон</a>, <a href="#">ноутбук</a></p>
												<input type="text" name="q" id="searchQuery" placeholder="Введите ключевое слово">
											</td>
											<!--td class="search-column-2">
												<p class="align-right"><a href="#">Advanced Search</a></p>
												<select class="chosen-select-search">
													<option>All Categories</option>
													<option>All Categories</option>
													<option>All Categories</option>
													<option>All Categories</option>
													<option>All Categories</option>
												</select>
											</td-->
										</tr>
									</table>
								</div>
								<div id="search-button">
									<input type="submit" value="">
									<i class="icons icon-search-1"></i>
								</div>
							<?php echo CHtml::endForm() ?>
						</div>
						
					</nav>
					<!-- /Main Navigation -->
					
				</div>
				
			</header>
			<!-- /Header -->
			
			
			<!-- Content -->
			<div class="row content">
				<div class="col-lg-12 col-md-12 col-sm-12 flash_messages">
					<?php if(($messages = Yii::app()->user->getFlash('messages'))): ?>
						<div class="breadcrumbs">
							<p class="breadcrumbs">
								<a href="#" class="close">
									<i class="icons icon-cancel"></i>
									<?php
										if(is_array($messages))
											echo implode('</br>', $messages);
										else
											echo $messages;
									?>
								</a>
							</p>
						</div>
					<?php endif; ?>
				</div>
				

				<?php echo $content; ?>		
			</div>
			<!-- /Content -->
			
			
			<?php if (isset($this->clips['underFooter'])) echo $this->clips['underFooter']; ?>


			<!-- Banner -->
			<section class="banner">
				
				<div class="left-side-banner banner-item icon-on-right gray">
					<h4>+375 (29) 123-4567</h4>
					<p>Понедельник - Пятница: 8:00 - 17:00</p>
					<i class="icons icon-phone-outline"></i>
				</div>
				
				<!--a href="#"-->
				<div class="middle-banner banner-item icon-on-left light-blue">
					<h4>Бесплатная доставка</h4>
					<p>на все заказы свыше $99</p>
					<!--span class="button">Узнать больше</span-->
					<i class="icons icon-truck-1"></i>
				</div>
				<!--/a-->
				
				<!--a href="#"-->
				<div class="right-side-banner banner-item icon-on-right orange">
					<h4>УДОБНЫЙ САМОВЫВОЗ</h4>
					<p>склады в центре города</p>
					<i class="icons icon-warehouse"></i>
				</div>
				<!--/a-->
				
			</section>
			<!-- /Banner -->

			
			<!-- Footer -->
			<footer id="footer" class="row">
				
				<!-- Upper Footer --
				<div class="col-lg-12 col-md-12 col-sm-12">
					
					<div id="upper-footer">
					
						<div class="row">
							
							<!-- Newsletter --
							<div class="col-lg-7 col-md-7 col-sm-7">
								<form id="newsletter" action="php/newsletter.php">
									<h4>Newsletter Sign Up</h4>
									<input type="text" name="newsletter-email" placeholder="Enter your email address">
									<input type="submit" name="newsletter-submit" value="Submit">
								</form>
							</div>
							<!-- /Newsletter -->
							
							
							<!-- Social Media --
							<div class="col-lg-5 col-md-5 col-sm-5 social-media">
								<h4>Мы в соцсетях</h4>
								<ul>
									<li class="social-googleplus tooltip-hover" data-toggle="tooltip" data-placement="top" title="Google+"><a href="#"></a></li>
									<li class="social-facebook tooltip-hover" data-toggle="tooltip" data-placement="top" title="Facebook"><a href="#"></a></li>
									<li class="social-pinterest tooltip-hover" data-toggle="tooltip" data-placement="top" title="Pinterest"><a href="#"></a></li>
									<li class="social-twitter tooltip-hover" data-toggle="tooltip" data-placement="top" title="Twitter"><a href="#"></a></li>
									<li class="social-youtube tooltip-hover" data-toggle="tooltip" data-placement="top" title="Youtube"><a href="#"></a></li>
								</ul>
							</div>
							<!-- /Social Media --
							
						</div>
					
					</div>
					
				</div>
				<!-- /Upper Footer -->
				
				
				
				<!-- Main Footer -->
				<div class="col-lg-12 col-md-12 col-sm-12">
					
					<div id="main-footer">
					
						<div class="row">
							
							<!-- The Service -->
							<div class="col-lg-3 col-md-3 col-sm-6">
								<h4>Услуги</h4>
								
								<?php
									$this->widget('zii.widgets.CMenu', array(
										'itemTemplate' => '<i class="icons icon-right-dir"></i> {menu}',
										'items'=>array(
											array('label'=>Yii::t('core','Вход'), 'url'=>array('/users/login/login'), 'visible'=>Yii::app()->user->isGuest),
											array('label'=>Yii::t('UsersModule', 'Регистрация'), 'url'=>array('/users/register'), 'visible'=>Yii::app()->user->isGuest),
											array('label'=>Yii::t('UsersModule', 'Забыли пароль?'), 'url'=>array('/users/remind'), 'visible'=>Yii::app()->user->isGuest),
											array('label'=>Yii::t('core','Личный кабинет'), 'url'=>array('/users/profile/index'), 'visible'=>!Yii::app()->user->isGuest),
											array('label'=>Yii::t('core','Мои заказы'), 'url'=>array('/users/profile/orders'), 'visible'=>!Yii::app()->user->isGuest),
											array('label'=>Yii::t('core','Выход'), 'url'=>array('/users/login/logout'), 'visible'=>!Yii::app()->user->isGuest),
										),
									));
								?>
							</div>
							<!-- /The Service -->
							
							<!-- Information -->
							<div class="col-lg-3 col-md-3 col-sm-6">
								<h4>Информация</h4>
								<?php
									$this->widget('zii.widgets.CMenu', array(
										'itemTemplate' => '<i class="icons icon-right-dir"></i> {menu}',
										'items'=>array(
											array('label'=>Yii::t('core', 'Помощь'), 'url'=>array('/pages/pages/view', 'url'=>'help')),
											array('label'=>Yii::t('core', 'Как сделать заказ'), 'url'=>array('/pages/pages/view', 'url'=>'how-to-create-order')),
											array('label'=>Yii::t('core', 'Гарантия'), 'url'=>array('/pages/pages/view', 'url'=>'garantiya')),
											array('label'=>Yii::t('core', 'Доставка и оплата'), 'url'=>array('/pages/pages/view', 'url'=>'dostavka-i-oplata')),
											array('label'=>Yii::t('core', 'Обратная связь'), 'url'=>array('/feedback/default/index')),
										),
									));
								?>
							</div>
							<!-- /Information -->
							
							<!-- Free space -->
							<div class="col-lg-3 col-md-3 col-sm-6">
								
							</div>
							<!-- /Free space -->
							
							<!-- Contact Us -->
							<div class="col-lg-3 col-md-3 col-sm-6 contact-footer-info">
								<h4>Контакты</h4>
								<ul>
									<li><i class="icons icon-location"></i> пр. Независимости 52/1 о.312<br>Минск, Беларусь.</li>
									<li><i class="icons icon-phone"></i> +375 (29) 123 4567</li>
									<li><i class="icons icon-mail-alt"></i><a href="mailto:mail@company.com"> mail@homeshop.ru</a></li>
									<li><i class="icons icon-skype"></i> homeshop</li>
								</ul>
							</div>
							<!-- /Contact Us -->
							
						</div>
						
					</div>
					
				</div>
				<!-- /Main Footer -->
				
				
				
				<!-- Lower Footer -->
				<div class="col-lg-12 col-md-12 col-sm-12">
					
					<div id="lower-footer">
					
						<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-6">
								<p class="copyright">Copyright &copy; <?php echo date("Y"); ?> <a href="#">HomeShop</a>. All Rights Reserved.</p>
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6">
								<ul class="payment-list">
									<li class="payment1"></li>
									<li class="payment2"></li>
									<li class="payment3"></li>
									<li class="payment4"></li>
									<li class="payment5"></li>
								</ul>
							</div>
							
						</div>
						
					</div>
					
				</div>
				<!-- /Lower Footer -->
				
			</footer>
			<!-- Footer -->
			
			
			<div id="back-to-top">
				<i class="icon-up-dir"></i>
			</div>
			
		</div>
		<!-- Container -->
		
		
	</body>
</html>