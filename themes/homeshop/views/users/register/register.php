<?php

/**
 * @var UserProfile $profile
 * @var User $user
 * @var Controller $this
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('UsersModule.core','Регистрация');
?>

<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12 register-account">
		
		<div class="carousel-heading no-margin">
			<h4><?php echo Yii::t('UsersModule.core','Регистрация'); ?></h4>
		</div>
		
		<div class="page-content">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'user-register-form',
				'enableAjaxValidation'=>false,
			)); ?>

				<div class="row">
					
					<div class="col-lg-12 col-md-12 col-sm-12">
						<p><strong>Информация о покупателе</strong></p>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($user,'username'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->textField($user,'username'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($user,'password'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->passwordField($user,'password'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($user,'email'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->textField($user,'email'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($profile,'full_name'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->textField($profile,'full_name'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($profile,'phone'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->textField($profile,'phone'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo $form->labelEx($profile,'delivery_address'); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo $form->textField($profile,'delivery_address'); ?>
					</div>	
					
				</div>
				
				<?php echo $form->errorSummary(array($user, $profile)); ?>
				
				<?php if(CCaptcha::checkRequirements()): ?>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
							<?php echo $form->labelEx($user,'verifyCode'); ?>
						</div>
						
						<div class="col-lg-2 col-md-2 col-sm-2">
							<p>
							<?php $this->widget('CCaptcha', array(
								'clickableImage' => true,
								'showRefreshButton' => false
							)); ?>
							</p>
						</div>
						
						<div class="col-lg-2 col-md-2 col-sm-2">
							<?php echo $form->textField($user,'verifyCode'); ?>
						</div>	
						<div class="col-lg-4 col-md-4 col-sm-4">
							<?php echo $form->error($user,'verifyCode'); ?>
						</div>	
						
					</div>
				<?php endif; ?>
				
				<div class="row">
					
					<div class="col-lg-6 col-md-6 col-sm-6 align-left">
						<?php echo CHtml::submitButton(Yii::t('UsersModule.core', 'Отправить'), array('class'=>'big')); ?>
					</div>
					
					<div class="col-lg-6 col-md-6 col-sm-6 align-right">
						<small>
							<?php echo CHtml::link(Yii::t('UsersModule', 'Авторизация'), array('login/login'), array('class'=>'align-right')); ?>
							<br>
							<?php echo CHtml::link(Yii::t('UsersModule.core', 'Напомнить пароль'), array('/users/remind'), array('class'=>'align-right')); ?>
							<br>
						</small>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>
		
	</div>
	  
</div>
