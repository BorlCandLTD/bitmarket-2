<?php

/**
 * @var UserLoginForm $model
 * @var Controller $this
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('UsersModule.core','Авторизация');
?>

<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading no-margin">
			<h4><?php echo Yii::t('UsersModule.core','Авторизация'); ?></h4>
		</div>
		
		<div class="page-content">
			<?php echo CHtml::form($this->createUrl('/users/login'),'post', array('id'=>'user-login-form')); ?>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="iconic-input">
							<?php echo CHtml::activeTextField($model,'username', array('required'=>true, 'placeholder'=>'Имя пользователя')); ?>
							<i class="icons icon-user-3"></i>
						</div>
					</div>
					
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="iconic-input">
							<?php echo CHtml::activePasswordField($model,'password', array('required'=>true, 'placeholder'=>'Пароль')); ?>
							<i class="icons icon-lock"></i>
						</div>
					</div>
				</div>
				<?php echo CHtml::errorSummary($model); ?>
				<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
				<?php echo CHtml::activeLabel($model,'rememberMe'); ?>
				<br/><br/>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 align-left">
						<input type="submit" class="orange" value="<?php echo Yii::t('UsersModule.core','Войти'); ?>">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 align-right">
						<small>
							<?php echo CHtml::link(Yii::t('UsersModule', 'Регистрация'), array('/users/register'), array('class'=>'align-right')); ?>
							<br>
							<?php echo CHtml::link(Yii::t('UsersModule.core', 'Напомнить пароль'), array('/users/remind'), array('class'=>'align-right')); ?>
							<br>
						</small>
					</div>
				</div>
			
			<?php echo CHtml::endForm(); ?>
		</div>
		
	</div>
	  
</div>
