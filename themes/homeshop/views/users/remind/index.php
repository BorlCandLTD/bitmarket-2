<?php
/**
 * Remind user password view
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('UsersModule.core','Напомнить пароль');
?>

<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading no-margin">
			<h4><?php echo Yii::t('UsersModule.core','Напомнить пароль'); ?></h4>
		</div>
		
		<div class="page-content">
				<?php echo CHtml::form(); ?>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="iconic-input">
							<?php echo CHtml::activeTextField($model,'email', array('required'=>true, 'placeholder'=>'Email')); ?>
							<i class="icons icon-mail"></i>
						</div>
					</div>
					
				</div>
				
				<?php echo CHtml::errorSummary($model); ?>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 align-left">
						<input type="submit" class="orange" value="<?php echo Yii::t('UsersModule.core','Напомнить пароль'); ?>">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 align-right">
						<small>
							<?php echo CHtml::link(Yii::t('UsersModule', 'Регистрация'), array('register/register'), array('class'=>'align-right')); ?>
							<br>
							<?php echo CHtml::link(Yii::t('UsersModule', 'Авторизация'), array('login/login'), array('class'=>'align-right')); ?>
							<br>
						</small>
					</div>
				</div>
			
			<?php echo CHtml::endForm(); ?>
		</div>
		
	</div>
	  
</div>
