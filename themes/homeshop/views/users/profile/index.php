<?php

/**
 * @var $profile UserProfile
 * @var $user User
 * @var $form CActiveForm
 * @var $changePasswordForm ChangePasswordForm
 */

$this->layout = '//layouts/layout1';

$this->pageTitle=Yii::t('UsersModule.core', 'Личный кабинет');
?>
<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12 register-account">
		
		<div class="carousel-heading no-margin">
			<h4><?php echo Yii::t('UsersModule.core','Личный кабинет'); ?></h4>
		</div>
		
		<div class="page-content">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<?php $form=$this->beginWidget('CActiveForm'); ?>
					
						<?php echo $form->errorSummary(array($profile, $user)); ?>
				
							
						<div class="row">
							
							<div class="col-lg-12 col-md-12 col-sm-12">
								<p><strong>Информация о покупателе</strong></p>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->labelEx($profile,'full_name'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->textField($profile,'full_name'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->labelEx($user,'email'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->textField($user,'email'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->labelEx($profile,'phone'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->textField($profile,'phone'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->labelEx($profile,'delivery_address'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->textField($profile,'delivery_address'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-6 align-left">
								<?php echo CHtml::submitButton(Yii::t('UsersModule.admin', 'Сохранить'), array('class'=>'big')); ?>
							</div>
							
						</div>
				
					<?php $this->endWidget(); ?>
				
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-12">
				
					<?php $form=$this->beginWidget('CActiveForm'); ?>
					
						<?php echo $form->errorSummary($changePasswordForm); ?>
				
							
						<div class="row">
							
							<div class="col-lg-12 col-md-12 col-sm-12">
								<p><strong>Изменить пароль</strong></p>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->label($changePasswordForm,'current_password'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->passwordField($changePasswordForm,'current_password'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4">
								<p><?php echo $form->label($changePasswordForm,'new_password'); ?></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<?php echo $form->passwordField($changePasswordForm,'new_password'); ?>
							</div>	
							
						</div>
						
						<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-6 align-left">
								<?php echo CHtml::submitButton(Yii::t('UsersModule.admin', 'Изменить'), array('class'=>'big')); ?>
							</div>
							
						</div>
				
					<?php $this->endWidget(); ?>
					
				</div>
			</div>
		</div>
	</div>
	
</div>