<?php

/**
 * View user orders
 * @var $orders
 */
 
$this->layout = '//layouts/layout1';

$this->pageTitle=Yii::t('UsersModule.core', 'Мои заказы');

?>

<div class="row">
                    
	<!-- Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('UsersModule.core', 'Мои заказы'); ?></h4>
		</div>
		
	</div>
	<!-- /Heading -->
	
</div>	


<div class="row">
	
	
		<?php
			$this->widget('zii.widgets.grid.CGridView', array(
				'id'           => 'ordersListGrid',
				'itemsCssClass'=> 'order-table',
				'dataProvider' => $orders->search(),
				'template'     => '{items}',
				'htmlOptions' => array('class' => 'col-lg-12 col-md-12 col-sm-12'),
				'columns' => array(
					array(
						'name'=>'id',
						'type'=>'raw',
						'value'=>'CHtml::link(CHtml::encode($data->id), array("/orders/cart/view", "secret_key"=>$data->secret_key))',
						'htmlOptions' => array('class'=>'order-number'),
					),
					array(
						'name'=>'user_name',
						'type'=>'raw',
						'value'=>'CHtml::link(CHtml::encode($data->user_name), array("/orders/cart/view", "secret_key"=>$data->secret_key))',
						'htmlOptions' => array('class'=>'order-number'),
					),
					'user_email',
					'user_phone',
					array(
						'name'=>'created',
						'type'=>'raw',
						'value'=>'$data->created',
					),
					array(
						'name'=>'status_id',
						'filter'=>CHtml::listData(OrderStatus::model()->orderByPosition()->findAll(), 'id', 'name'),
						'value'=>'$data->status_name',
					),
					array(
						'name'=>'delivery_id',
						'filter'=>CHtml::listData(StoreDeliveryMethod::model()->orderByPosition()->findAll(), 'id', 'name'),
						'value'=>'$data->delivery_name'
					),
					array(
						'type'=>'raw',
						'name'=>'full_price',
						'value'=>function($data,$row){ // declare signature so that we can use $data, and $row within this function 
							return '<span class="price">'.StoreProduct::formatPrice($data->full_price).' '.Yii::app()->currency->main->symbol.'</span>';
						},
					),
				),
			));
		?>
	
</div>
