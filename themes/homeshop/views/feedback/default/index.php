<?php

/**
 * @var $this Controller
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('FeedbackModule.core', 'Обратная связь');

?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 register-account">
		
		<div class="carousel-heading no-margin">
			<h4><?php echo Yii::t('FeedbackModule.core', 'Обратная связь') ?></h4>
		</div>
		
		<div class="page-content">
			
			<?php $form=$this->beginWidget('CActiveForm'); ?>
			
				<?php echo $form->errorSummary($model); ?>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo CHtml::activeLabel($model,'name', array('required'=>true)); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo CHtml::activeTextField($model,'name'); ?>
					</div>	
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo CHtml::activeLabel($model,'email', array('required'=>true)); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo CHtml::activeTextField($model,'email'); ?>
					</div>	
					
				</div>
				
				<?php if(Yii::app()->settings->get('feedback', 'enable_captcha')): ?>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<?php echo CHtml::activeLabel($model,'code', array('required'=>true)); ?>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-2">
						<p>
						<?php $this->widget('CCaptcha', array(
							'clickableImage' => true,
							'showRefreshButton' => false
						)); ?>
						</p>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-2">
						<?php echo $form->textField($model,'code'); ?>
					</div>	
					<div class="col-lg-4 col-md-4 col-sm-4">
						<?php echo $form->error($model,'code'); ?>
					</div>	
					
				</div>
				<?php endif; ?>
				
				<div class="row">
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p><?php echo CHtml::activeLabel($model,'message', array('required'=>true)); ?></p>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<?php echo CHtml::activeTextArea($model,'message', array('rows'=>6)); ?>
					</div>	
					
				</div>
				
				
				
				<input class="big" type="submit" value="<?php echo Yii::t('FeedbackModule.core', 'Отправить') ?>">
				
			<?php $this->endWidget(); ?>
			
		</div>
		
	</div>
	
</div>