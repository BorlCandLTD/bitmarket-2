<?php

/**
 * View category pages
 * @var PageCategory $model
 */

$this->layout = '//layouts/layout1';

// Set meta tags
$this->pageTitle = ($model->meta_title) ? $model->meta_title : $model->name;
$this->pageKeywords = $model->meta_keywords;
$this->pageDescription = $model->meta_description;

?>

<div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo $model->name ?></h4>
		</div>
		
	</div>
	
	<?php 
		if (sizeof($pages) > 0 && sizeof($pages) % 2 == 1): 
			$page = $pages[0];
	?>
        <div class="col-lg-12 col-md-12 col-sm-12">
		
			<div class="blog-item">
				
				<div class="blog-info">
					<h3><?php echo CHtml::link($page->title, array('/pages/pages/view', 'url'=>$page->url)); ?></h3>
					<div class="blog-meta">
						<span class="date"><i class="icons icon-clock"></i> <?php echo $page->publish_date; ?></span>
					</div>
					<p><?php echo $page->short_description; ?></p>
				</div>
				
				<?php echo CHtml::link('<div class="product-actions blog-actions"><span class="product-action dark-blue"><span class="action-wrapper"><i class="icons icon-doc-text"></i><span class="action-name">Читать полностью</span></span></span></div>', array('/pages/pages/view', 'url'=>$page->url)); ?>
			</div>
		
		</div>
    <?php unset($pages[0]); endif; ?>
	
</div>

<?php if (sizeof($pages) > 0): ?>
	<div class="row">
        <?php foreach ($pages as $page): ?>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="blog-item">
					
					<div class="blog-info">
						<h3><?php echo CHtml::link($page->title, array('/pages/pages/view', 'url'=>$page->url)); ?></h3>
						<div class="blog-meta">
							<span class="date"><i class="icons icon-clock"></i> <?php echo $page->publish_date; ?></span>
						</div>	
						<p><?php echo $page->short_description; ?></p>
					</div>
					<?php echo CHtml::link('<div class="product-actions blog-actions"><span class="product-action dark-blue"><span class="action-wrapper"><i class="icons icon-doc-text"></i><span class="action-name">Читать полностью</span></span></span></div>', array('/pages/pages/view', 'url'=>$page->url)); ?>
				</div>
			</div>
        <?php endforeach ?>
	</div>
<?php else: ?>
	<?php echo Yii::t('PagesModule.core', 'В категории нет страниц.') ?>
<?php endif ?>

<!--div class="col-lg-6 col-md-6 col-sm-6">
	<div class="pagination">
		<a href="#"><div class="previous"><i class="icons icon-left-dir"></i></div></a>
		<a href="#"><div class="page-button">1</div></a>
		<a href="#"><div class="page-button">2</div></a>
		<a href="#"><div class="page-button">3</div></a>
		<a href="#"><div class="next"><i class="icons icon-right-dir"></i></div></a>
	</div>
</div-->
	<div class="col-lg-6 col-md-6 col-sm-6">
		<?php $this->widget('application.modules.store.widgets.SLinkPager', array(
			'pages' => $pagination,
			'htmlOptions'=> array('class' => 'pagination',),
		)) ?>
	</div>

	

	
