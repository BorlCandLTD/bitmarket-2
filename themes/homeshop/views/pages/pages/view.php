<?php

/**
 * View page
 * @var Page $model
 */

$this->layout = '//layouts/layout1';

// Set meta tags
$this->pageTitle       = ($model->meta_title) ? $model->meta_title : $model->title;
$this->pageKeywords    = $model->meta_keywords;
$this->pageDescription = $model->meta_description;
?>

<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo $model->title; ?></h4>
		</div>
		
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="blog-item">
			
			<div class="blog-info">
				
				<p>
					<?php echo $model->short_description; ?>
				</p>
				<p>
					<?php echo $model->full_description; ?>
				</p>
				
			
				<div class="blog-meta">
					<span class="date"><i class="icons icon-clock"></i> <?php echo $model->publish_date; ?></span>
				</div>
		</div>
		
	</div>
</div>