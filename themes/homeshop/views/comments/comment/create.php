<?php
/**
 * @var $this Controller
 * @var $form CActiveForm
 */

// Load module
$module = Yii::app()->getModule('comments');
// Validate and save comment on post request
$comment = $module->processRequest($model);
// Load model comments
$comments = Comment::getObjectComments($model);

$currentUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

// Display comments
if(!empty($comments)){ ?>
<ul class="comments">
	<?php foreach($comments as $row){ ?>
		<li id="comment_<?php echo $row->id; ?>">
			<p><strong><?php echo CHtml::encode($row->name); ?></strong> </p>
			<span class="date"><?php echo $row->created; ?></span>
			<?php echo CHtml::link('<i class="icons icon-hash"></i>', Yii::app()->request->getUrl().'#comment_'.$row->id) ?>
			<p><?php echo nl2br(CHtml::encode($row->text)); ?></p>
		</li>
	<?php } ?>
</ul>
<?php } ?>

<div class="row leave_comment" id="leave_comment">
								
	<div class="col-lg-6 col-md-6 col-sm-8">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'                     =>'comment-create-form',
			'action'                 =>$currentUrl.'#comment-create-form',
			'enableAjaxValidation'   =>false,
			'enableClientValidation' =>true,
		)); ?>
		
		<h3><?php echo Yii::t('CommentsModule.core', 'Оставить отзыв') ?></h3>
		
		<?php if(Yii::app()->user->isGuest): ?>
			<?php echo $form->labelEx($comment,'name'); ?>
			<?php echo $form->textField($comment,'name'); ?>
			<?php echo $form->error($comment,'name'); ?>
			<br><br>
			<?php echo $form->labelEx($comment,'email'); ?>
			<?php echo $form->textField($comment,'email'); ?>
			<?php echo $form->error($comment,'email'); ?>
			<br><br>
		<?php endif; ?>
		<?php echo $form->labelEx($comment,'text'); ?>
		<?php echo $form->textArea($comment,'text', array('rows'=>5)); ?>
		<?php echo $form->error($comment,'text'); ?>
		<br>
		<?php if(Yii::app()->user->isGuest): ?><br>
			<?php echo CHtml::activeLabelEx($comment, 'verifyCode')?>
			
				<div class="row">
					
					<div class="col-lg-3 col-md-3 col-sm-3">
						<?php $this->widget('CCaptcha', array(
							'clickableImage'=>true,
							'showRefreshButton'=>false,
						)) ?>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						
						<?php echo CHtml::activeTextField($comment, 'verifyCode')?>
					</div>
					
				</div>
				<?php echo $form->error($comment,'verifyCode'); ?>
				<br>
		<?php endif; ?>
		<?php echo CHtml::submitButton(Yii::t('CommentsModule.core', 'Отправить'), array('class'=>'dark-blue big')); ?>
		<?php $this->endWidget(); ?><!-- /form -->
	</div>
	
</div>