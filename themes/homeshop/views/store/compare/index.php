<?php

/**
 * Compare products view
 *
 * @var CompareController $this
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('StoreModule.core', 'Сравнение продуктов');

?>

<div class="row">
                    
	<!-- Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('StoreModule.core', 'Сравнение продуктов') ?></h4>
		</div>
		
	</div>
	<!-- /Heading -->
	
</div>
<?php if(!empty($this->model->products)): ?>
<div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<table class="compare-table">
			<thead>
				<tr>
					<th>Название</th>
					<?php foreach($this->model->products as $p): ?>
						<td>
							<h5><?php echo CHtml::link(CHtml::encode($p->name), array('frontProduct/view', 'url'=>$p->url)) ?></h5>
							<?php
								echo CHtml::form(array('/orders/cart/add'));
								echo CHtml::hiddenField('product_id', $p->id);
								echo CHtml::hiddenField('product_price', $p->price);
								echo CHtml::hiddenField('use_configurations', $p->use_configurations);
								echo CHtml::hiddenField('currency_rate', Yii::app()->currency->active->rate);
								echo CHtml::hiddenField('configurable_id', 0);
								echo CHtml::hiddenField('quantity', 1);

								
								if($p->getIsAvailable())
								{
									echo CHtml::ajaxSubmitButton(
										Yii::t('StoreModule.core',''), 
										array('/orders/cart/add'), 
										array(
											'id'=>'addProduct'.$p->id,
											'dataType'=>'json',
											'success'=>'js:function(data, textStatus, jqXHR){processCartResponseFromList(data, textStatus, jqXHR, "'.Yii::app()->createAbsoluteUrl('/store/frontProduct/view', array('url'=>$p->url)).'")}',
										), 
										array('style'=>'display:none;', 'id'=>'addProduct'.$p->id));
								}
							
								echo CHtml::endForm();
							?>
						</td>
					<?php endforeach; ?>
				</tr>
			</thead>
			<?php if(!empty($this->model->attributes)): ?>
			<tbody>
				<tr>
					<th>Изображение</th>
					<?php foreach($this->model->products as $p): ?>
						<td class="compare-image">
						<?php
							if($p->mainImage)
								$imgSource = $p->mainImage->getUrl('150x150');
							else
								$imgSource = 'http://placehold.it/150x150';
							echo CHtml::link(CHtml::image($imgSource, $p->mainImageTitle), array('frontProduct/view', 'url'=>$p->url));
						?>
						</td>
					<?php endforeach; ?>
				</tr>
				
				<tr>
					<th>Действия</th>
					
					<?php foreach($this->model->products as $p): ?>
					<td>
						<a href="#" onclick="$('#<?php echo 'addProduct'.$p->id; ?>').trigger('click'); return false;">
							<span class="add-to-cart">
								<span class="action-wrapper">
									<i class="icons icon-basket-2"></i>
								</span>
							</span>
						</a>
						<?php 
							echo CHtml::link('<span class="add-to-trash"><span class="action-wrapper"><i class="icons icon-trash-8"></i></span></span>', array('/store/compare/remove','id'=>$p->id),array());
						?>
					</td>
					
					<?php endforeach; ?>
				</tr>
				
				<tr>
					<th>Рейтинг</th>
					<?php foreach($this->model->products as $p): ?>
						<td><div class="rating readonly-rating" data-score="<?php echo ($p->votes != 0) ? round($p->rating / $p->votes) : 0 ?>"></div></td>
					<?php endforeach; ?>
				</tr>
				<tr>
					<th>Цена</th>
					<?php foreach($this->model->products as $p): ?>
						<td>
							<span class="price">
								<?php
								if($p->appliedDiscount)
									echo '<del>'.$p->toCurrentCurrency('originalPrice').' '.Yii::app()->currency->active->symbol.'</del><br>';
								?>
								<?php echo $p->priceRange() ?>
							</span>
						</td>
					<?php endforeach; ?>
				</tr>
			
				<?php foreach($this->model->attributes as $attribute): ?>
				<tr>
					<th><?php echo $attribute->title ?></th>
					<?php foreach($this->model->products as $product): ?>
					<td>
						<p>
							<?php
								$value=$product->{'eav_'.$attribute->name};
								echo $value===null ? Yii::t('StoreModule.core','Не указано') : $value;
							?>
						</p>
					</td>
					<?php endforeach; ?>
				</tr>
				<?php endforeach ?>
				
				<tr>
					<th>Описание</th>
					<?php foreach($this->model->products as $p): ?>
						<td><p><?php echo CHtml::encode($p->short_description); ?></p></td>
					<?php endforeach; ?>
				</tr>
			</tbody>
			<?php endif ?>
		</table>
		
	</div>
	
</div>
<?php else: ?>
	<?php echo Yii::t('StoreModule.core','Нет результатов.').CHtml::closeTag('br').CHtml::closeTag('br'); ?>
<?php endif ?>