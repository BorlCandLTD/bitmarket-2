<?php
/**
 * Product view
 * @var StoreProduct $model
 * @var $this Controller
 */

$this->layout = '//layouts/layout2';

// Set meta tags
$this->pageTitle = ($model->meta_title) ? $model->meta_title : $model->name;
$this->pageKeywords = $model->meta_keywords;
$this->pageDescription = $model->meta_description;

// Register main script
Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/product.view.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/product.view.configurations.js', CClientScript::POS_END);

// Create breadcrumbs
if($model->mainCategory)
{
	$ancestors = $model->mainCategory->excludeRoot()->ancestors()->findAll();

	foreach($ancestors as $c)
		$this->breadcrumbs[$c->name] = $c->getViewUrl();

	// Do not add root category to breadcrumbs
	if ($model->mainCategory->id != 1)
		$this->breadcrumbs[$model->mainCategory->name] = $model->mainCategory->getViewUrl();
}

// Fancybox ext
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'a.fancybox',
));

?>

<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="breadcrumbs">
		<?php
			$this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
				'tagName'=>'p',
				'separator'=>' <i class="icons icon-right-dir"></i> ',
			));
		?>
	</div>
</div>

<!-- Main Content -->
<section class="main-content col-lg-12 col-md-12 col-sm-12">
	
	<!-- Product -->
	<div class="product-single">
		
		<div class="row">
			
			<!-- Product Images Carousel -->
			<div class="col-lg-4 col-md-4 col-sm-4 product-single-image">
				
				<div id="product-slider">
					<ul class="slides">
						<li>
						
							<?php
								// Main product image
								if($model->mainImage){
									echo CHtml::image($model->mainImage->getUrl('370x370', 'resize'), $model->mainImage->title, array('class'=>'cloud-zoom', 'data-large'=>$model->mainImage->getUrl()));//, $model->mainImage->getUrl(), ); ?>
									<a class="fullscreen-button" num="pi1" >
										<div class="product-fullscreen">
											<i class="icons icon-resize-full-1"></i>
										</div>
									</a>
							<?php
								}
								else{
									echo CHtml::image('http://placehold.it/370x370', $model->mainImage->title, array('class'=>'cloud-zoom', 'data-large'=>$model->mainImage->getUrl(), 'num'=>'1'));
								}
							?>
						</li>
					</ul>
				</div>
				<div id="product-carousel">
					<ul class="slides">
					<?php
							
							$i = 1;
							if($model->mainImage)
							{
								echo CHtml::openTag('li');
								echo CHtml::link('', $model->mainImage->getUrl(), array('class'=>'fancybox', 'rel'=>'product-images', 'num'=>'pi'.$i));
								echo CHtml::image($model->mainImage->getUrl('80x80'), $model->mainImage->title, array('data-large'=>$model->mainImage->getUrl(), 'num'=>'pi'.$i));
								echo CHtml::closeTag('li');
								
								$i++;
							}
							else{
								echo CHtml::openTag('li');
								echo CHtml::link('', 'http://placehold.it/370x370', array('class'=>'fancybox', 'rel'=>'product-images', 'num'=>'pi'.$i));
								echo CHtml::image('http://placehold.it/80x80', '', array('data-large'=>'http://placehold.it/370x370', 'num'=>'pi'.$i));
								echo CHtml::closeTag('li');
								
								$i++;
							}
							
							// Display additional images
							foreach($model->imagesNoMain as $image)
							{
								echo CHtml::openTag('li');
								echo CHtml::link('', $image->getUrl(), array('class'=>'fancybox', 'rel'=>'product-images', 'num'=>'pi'.$i));
								echo CHtml::image($image->getUrl('80x80'), $model->mainImage->title, array('data-large'=>$image->getUrl(), 'num'=>'pi'.$i));
								echo CHtml::closeTag('li');
								
								$i++;
							}
						?>
					</ul>
					<div class="product-arrows">
						<div class="left-arrow">
							<i class="icons icon-left-dir"></i>
						</div>
						<div class="right-arrow">
							<i class="icons icon-right-dir"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /Product Images Carousel -->
			
			<div class="col-lg-8 col-md-8 col-sm-8 product-single-info full-size">
				<?php echo CHtml::form(array('/orders/cart/add')) ?>
				<?php 
					echo CHtml::hiddenField('product_id', $model->id);
					echo CHtml::hiddenField('product_price', $model->price);
					echo CHtml::hiddenField('use_configurations', $model->use_configurations);
					echo CHtml::hiddenField('currency_rate', Yii::app()->currency->active->rate);
					echo CHtml::hiddenField('configurable_id', 0);
					echo CHtml::hiddenField('quantity', 1);
				?>
				
				<h2><?php echo CHtml::encode($model->name); ?></h2>
				<div class="rating-box">
				<?php $this->widget('CStarRating',array(
					'name'=>'rating_'.$model->id,
					'id'=>'rating_'.$model->id,
					'allowEmpty'=>false,
					'readOnly'=>isset(Yii::app()->request->cookies['rating_'.$model->id]),
					'minRating'=>1,
					'maxRating'=>5,
					'value'=>($model->rating+$model->votes) ? round($model->rating / $model->votes) : 0,
					'callback'=>'js:function(){rateProduct('.$model->id.')}',
					'cssFile'=>Yii::app()->theme->baseUrl.'/assets/css/rating/jquery.rating.css',
			)); ?>
			&nbsp;
					<?php echo $model->commentsCount; ?> Отзыв(ов)
				</div>
				<table><tr><td><?php echo $model->short_description; ?></td></tr></table>
				
				<?php
					if($model->getEavAttributes())
					{						
						echo $this->renderPartial('_attributes', array('model'=>$model), true);
					}
				?>
				
				<span class="price">
					<?php
						if($model->appliedDiscount){
							echo '<del>'.$model->toCurrentCurrency('originalPrice').' '.Yii::app()->currency->active->symbol.'</del> ';
						}

						echo CHtml::openTag('span',array('id'=>'productPrice'));
						echo StoreProduct::formatPrice($model->toCurrentCurrency()); 
						echo CHtml::closeTag('span');
						echo ' '.Yii::app()->currency->active->symbol;
					?>
				</span>
				
				<?php $this->renderPartial('_configurations', array('model'=>$model)); ?>
				
				
				
				<?php
					
					
					if($model->getIsAvailable())
					{
						echo CHtml::ajaxSubmitButton(
							Yii::t('StoreModule.core',''), 
							array('/orders/cart/add'), 
							array(
								'id'=>'addProduct'.$model->id,
								'dataType'=>'json',
								'success'=>'js:function(data, textStatus, jqXHR){processCartResponseFromList(data, textStatus, jqXHR, "'.Yii::app()->createAbsoluteUrl('/store/frontProduct/view', array('url'=>$model->url)).'")}',
							), 
							array('style'=>'display:none;', 'id'=>'addProduct'.$model->id));
					}
				?>
				
				<?php echo CHtml::endForm(); ?>
				
				
				<div class="product-actions">
					<?php if($model->getIsAvailable()): ?>
						<span class="add-to-cart" onclick="return $('#<?php echo 'addProduct'.$model->id; ?>').trigger('click');">
							<span class="action-wrapper">
								<i class="icons icon-basket-2"></i>
								<span class="action-name">В корзину</span>
							</span >
						</span>
					<?php else: ?>
						<span class="add-to-cart" onclick="showNotifierPopup(<?php echo $model->id; ?>); return false;">
							<span class="action-wrapper">
								<i class="icons icon-na"></i>
								<span class="action-name">Нет в наличии</span>
							</span >
						</span>
					<?php endif; ?>
					<span class="add-to-favorites" onclick="addProductToWishList(<?php echo $model->id ?>); return false;">
						<span class="action-wrapper">
							<i class="icons icon-heart-empty"></i>
							<span class="action-name">В желаемые</span>
						</span>
					</span>
					<span class="add-to-compare" onclick="addProductToCompare(<?php echo $model->id ?>); return false;">
						<span class="action-wrapper">
							<i class="icons icon-docs"></i>
							<span class="action-name">К сравнению</span>
						</span>
					</span>
				
				</div>
				
				<div class="social-share">
					
						
				</div>
				
			</div>
			
		</div>
		
	</div>
	<!-- /Product -->
	
	
	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12">
			
			<!-- Accordion -->
			<div class="accordion">
				
				<ul>
					<?php if (!empty($model->full_description)): ?>
					<!-- Item -->
					<li>
					
						<div class="accordion-header">
							<h4>Описание</h4>
							<span class="accordion-button">
								<i class="icons icon-plus-1"></i>
							</span>
						</div>
						<div class="accordion-content page-content">
							<p><?php echo $model->full_description; ?></p>
						</div>
						
					</li>
					<!-- /Item -->
					<?php endif; ?>
					<!-- Item -->
					<li>
						<div class="accordion-header">
							<h4>Отзывы</h4>
							<span class="accordion-button">
								<i class="icons icon-plus-1"></i>
							</span>
						</div>
						<div class="accordion-content page-content">
							<?php
								// Comments tab
								echo $this->renderPartial('//comments/comment/create', array('model'=>$model,), true);
							?>
						</div>
					</li>
					<!-- /Item -->
					
				</ul>
				<!-- /Accordion -->
			</div>
			
			
		</div>
		
	</div>
	
	
	<?php if($model->relatedProductCount){
		echo $this->renderPartial('_related', array('model'=>$model,), true); 
	}
	?>
	
	<!-- Product Buttons -->
	<div class="row button-row">
		
		<div class="col-lg-5 col-md-5 col-sm-5">
			<a class="button grey regular" href="/<?php echo $model->mainCategory->full_path; ?>"><i class="icons icon-reply"></i> НАЗАД К: <?php echo $model->mainCategory->name; ?></a>
		</div>
		
	</div>
	<!-- /Product Buttons -->
	
</section>
<!-- /Main Content -->





<? /* ?>
<div class="product">

	<div class="images">
		<div class="image_row">
			<div class="main">
				<?php
					// Main product image
					if($model->mainImage)
						echo CHtml::link(CHtml::image($model->mainImage->getUrl('340x250', 'resize'), $model->mainImage->title), $model->mainImage->getUrl(), array('class'=>'thumbnail'));
					else
						echo CHtml::link(CHtml::image('http://placehold.it/340x250'), '#', array('class'=>'thumbnail'));
				?>
			</div>
			<div class="stars">
				<?php $this->widget('CStarRating',array(
					'name'=>'rating_'.$model->id,
					'id'=>'rating_'.$model->id,
					'allowEmpty'=>false,
					'readOnly'=>isset(Yii::app()->request->cookies['rating_'.$model->id]),
					'minRating'=>1,
					'maxRating'=>5,
					'value'=>($model->rating+$model->votes) ? round($model->rating / $model->votes) : 0,
					'callback'=>'js:function(){rateProduct('.$model->id.')}',
			)); ?>
			</div>
		</div>
		<div class="additional">
			<ul>
			<?php
			// Display additional images
			foreach($model->imagesNoMain as $image)
			{
				echo CHtml::openTag('li', array('class'=>'span2'));
				echo CHtml::link(CHtml::image($image->getUrl('160x120'), $image->title), $image->getUrl(), array('class'=>'thumbnail'));
				echo CHtml::closeTag('li');
			}
			?>
			</ul>
		</div>
	</div>

	<div class="info">
		<?php echo CHtml::form(array('/orders/cart/add')) ?>

		<h1><?php echo CHtml::encode($model->name); ?></h1>

		<?php $this->renderPartial('_configurations', array('model'=>$model)); ?>

		<div class="errors" id="productErrors"></div>

		<div style="clear: both;font-size: 16px">
			<?php
			if($model->appliedDiscount)
				echo '<span style="color:red; "><s>'.$model->toCurrentCurrency('originalPrice').' '.Yii::app()->currency->active->symbol.'</s></span>';
			?>
		</div>

		<div class="price">
			<span id="productPrice"><?php echo StoreProduct::formatPrice($model->toCurrentCurrency()); ?></span>
			<?php echo Yii::app()->currency->active->symbol; ?>
		</div>

		<div class="actions">
			<?php
				echo CHtml::hiddenField('product_id', $model->id);
				echo CHtml::hiddenField('product_price', $model->price);
				echo CHtml::hiddenField('use_configurations', $model->use_configurations);
				echo CHtml::hiddenField('currency_rate', Yii::app()->currency->active->rate);
				echo CHtml::hiddenField('configurable_id', 0);
				echo CHtml::hiddenField('quantity', 1);

				if($model->isAvailable)
				{
					echo CHtml::ajaxSubmitButton(Yii::t('StoreModule.core','Купить'), array('/orders/cart/add'), array(
						'dataType' => 'json',
						'success'  => 'js:function(data, textStatus, jqXHR){processCartResponse(data, textStatus, jqXHR)}',
					), array(
						'id'=>'buyButton',
						'class'=>'blue_button'
					));
				}
				else
				{
					echo CHtml::link('Сообщить о появлении', '#', array(
						'onclick' => 'showNotifierPopup('.$model->id.'); return false;',
					));
				}

				echo CHtml::endForm();
			?>

			<div class="silver_clean silver_button">
				<button title="<?=Yii::t('core','Сравнить')?>" onclick="return addProductToCompare(<?php echo $model->id ?>);"><span class="icon compare"></span>Сравнить</button>
			</div>

			<div class="silver_clean silver_button">
				<button title="<?=Yii::t('core','В список желаний')?>" onclick="return addProductToWishList(<?php echo $model->id ?>);"><span class="icon heart"></span>Список желаний</button>
			</div>
		</div>
		<div class="desc"><?php echo $model->short_description; ?></div>
		<div class="desc"><?php echo $model->full_description; ?></div>
	</div>

	<div style="clear:both;"></div>

	<?php
		$tabs = array();

		// EAV tab
		if($model->getEavAttributes())
		{
			$tabs[Yii::t('StoreModule.core', 'Характеристики')] = array(
				'content'=>$this->renderPartial('_attributes', array('model'=>$model), true
			));
		}

		// Comments tab
		$tabs[Yii::t('StoreModule.core', 'Отзывы').' ('.$model->commentsCount.')'] = array(
			'id'=>'comments_tab',
			'content'=>$this->renderPartial('comments.views.comment.create', array(
				'model'=>$model,
			), true));

		// Related products tab
		if($model->relatedProductCount)
		{
			$tabs[Yii::t('StoreModule.core', 'Сопутствующие продукты').' ('.$model->relatedProductCount.')'] = array(
				'id'=>'related_products_tab',
				'content'=>$this->renderPartial('_related', array(
					'model'=>$model,
				), true));
		}

		// Render tabs
		$this->widget('zii.widgets.jui.CJuiTabs', array(
			'id'=>'tabs',
			'tabs'=>$tabs
		));

		// Fix tabs opening by anchor
		Yii::app()->clientScript->registerScript('tabSelector', '
			$(function() {
				var anchor = $(document).attr("location").hash;
				var result = $("#tabs").find(anchor).parents(".ui-tabs-panel");
				if($(result).length)
				{
					$("#tabs").tabs("select", "#"+$(result).attr("id"));
				}
			});
		');
	?>
</div> <? */ ?>
