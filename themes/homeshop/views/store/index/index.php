<?php

/**
 * Setting layout
 */
$this->layout = '//layouts/layout1';

/**
 * Site start view
 */
?>


<section class="slider">
	<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>
				<!-- SLIDE  -->
				<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
					<!-- MAIN IMAGE -->
					<img src="/uploads/importImages/mainPageBanner.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->
					<!--div class="tp-caption skewfromrightshort fadeout"
						data-x="40"
						data-y="60"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"><h2><strong>Lorem Ipsum Dolor</strong></h2>
					</div>
					<div class="tp-caption skewfromrightshort fadeout"
						data-x="40"
						data-y="140"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"><h3>All the power in your hands!</h3>
					</div>
					<div class="tp-caption skewfromrightshort fadeout"
						data-x="40"
						data-y="250"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"><span>From <span class="price">$960.00</span></span>
					</div-->
					<div class="tp-caption skewfromrightshort fadeout"
						data-x="right"
						data-y="bottom"
						data-hoffset="-10"
						data-voffset="-10"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"><a class="button big red" href="/products/search/q/Apple">Посмотреть сейчас</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- Featured Products -->
<div class="products-row row">
	
	<!-- Carousel Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4>Популярные товары</h4>
			<div class="carousel-arrows">
				<i class="icons icon-left-dir"></i>
				<i class="icons icon-right-dir"></i>
			</div>
		</div>
		
	</div>
	<!-- /Carousel Heading -->
	
	<!-- Carousel -->
	<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
		
		<div class="owl-carousel" data-max-items="4">
		
			<?php
				foreach($popular as $p){
					$this->renderPartial('_product', array('data'=>$p));
				}
			?>
						
		</div>
	</div>
	<!-- /Carousel -->
	
</div>
<!-- /Featured Products -->

<?php $this->beginClip('underFooter'); ?>

<!-- Newest Products -->
<div class="products-row row">
	
	<!-- Carousel Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4>Новинки</h4>
			<div class="carousel-arrows">
				<i class="icons icon-left-dir"></i>
				<i class="icons icon-right-dir"></i>
			</div>
		</div>
		
	</div>
	<!-- /Carousel Heading -->
	
	<!-- Carousel -->
	<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
		
		<div class="owl-carousel" data-max-items="4">
		
			<?php
				foreach($newest as $p){
					$this->renderPartial('_product', array('data'=>$p));
				}
			?>
						
		</div>
	</div>
	<!-- /Carousel -->
	
</div>
<!-- /Newest Products -->

<!-- Added_to_cart Products -->
<div class="products-row row">
	
	<!-- Carousel Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4>Хиты продаж</h4>
			<div class="carousel-arrows">
				<i class="icons icon-left-dir"></i>
				<i class="icons icon-right-dir"></i>
			</div>
		</div>
		
	</div>
	<!-- /Carousel Heading -->
	
	<!-- Carousel -->
	<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
		
		<div class="owl-carousel" data-max-items="4">
		
			<?php
				foreach($added_to_cart as $p){
					$this->renderPartial('_product', array('data'=>$p));
				}
			?>
						
		</div>
	</div>
	<!-- /Carousel -->
	
</div>
<!-- /Added_to_cart Products -->

<!-- News -->
<div class="products-row row">
	
	<!-- Carousel Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4>Последние новости &amp; Обзоры</h4>
			<div class="carousel-arrows">
				<i class="icons icon-left-dir"></i>
				<i class="icons icon-right-dir"></i>
			</div>
		</div>
		
	</div>
	<!-- /Carousel Heading -->
	
	
	<!-- Carousel -->
	<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
		
		<div class="owl-carousel" data-max-items="2">
			
			<?php foreach($news as $n): ?>
				<!-- Slide -->
				<div>
					<!-- Carousel Item -->
					<article class="news">
						
						<div class="news-background">
						
							<div class="row">
								<!--div class="col-lg-6 col-md-6 col-sm-6 news-thumbnail">
									<a href="#"><img src="img/news/sample1.jpg" alt="News1"></a>
								</div-->
								<div class="col-lg-push-1 col-md-push-1 col-sm-push-1 col-lg-10 col-md-10 col-sm-10 news-content">
									<h5><a href="<?php echo $n->viewUrl ?>"><?php echo $n->title ?></a></h5>
									<span class="date"><i class="icons icon-clock-1"></i> <?php echo $n->created ?></span>
									<p><?php echo $n->short_description ?></p>
								</div>
							</div>
							
						</div>
						
					</article>
					<!-- /Carousel Item -->
				</div>
				<!-- /Slide -->
			<?php endforeach; ?>
			
		</div>
	
	</div>
	<!-- /Carousel -->
	
</div>
<!-- /News -->
<!--div class="centered">
	<div class="wide_line">
		<span>Новости</span>

	</div>

	<ul class="news">
		
	</ul>

	<div class="all_news">
		<a href="<?=$n->category->viewUrl?>">Все новости</a>
	</div>
</div-->
<?php $this->endClip(); ?>