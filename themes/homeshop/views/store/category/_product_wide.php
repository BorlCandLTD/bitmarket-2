<?php
/**
 * @var StoreProduct $data
 */
?>
<!-- Product Item -->
<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="grid-view product">
		<div class="product-image col-lg-4 col-md-4 col-sm-4">
			<?php if($data->appliedDiscount): ?>
				<span class="product-tag">Скидка <?php echo strpos($data->appliedDiscount->sum, '%') !== false ? $data->appliedDiscount->sum : $data->appliedDiscount->sum.'$' ?></span>
			<?php endif; ?>
			
			<?php
			if($data->mainImage)
				$imgSource = $data->mainImage->getUrl('270x270');
			else
				$imgSource = 'http://placehold.it/270x270';
			echo CHtml::link(CHtml::image($imgSource, $data->mainImageTitle), array('frontProduct/view', 'url'=>$data->url), array('class'=>'thumbnail'));
			?>
		</div>
		<?php
			echo CHtml::form(array('/orders/cart/add'));
			echo CHtml::hiddenField('product_id', $data->id);
			echo CHtml::hiddenField('product_price', $data->price);
			echo CHtml::hiddenField('use_configurations', $data->use_configurations);
			echo CHtml::hiddenField('currency_rate', Yii::app()->currency->active->rate);
			echo CHtml::hiddenField('configurable_id', 0);
			echo CHtml::hiddenField('quantity', 1);
			
			if($data->getIsAvailable())
			{
				echo CHtml::ajaxSubmitButton(
					Yii::t('StoreModule.core',''), 
					array('/orders/cart/add'), 
					array(
						'id'=>'addProduct'.$data->id,
						'dataType'=>'json',
						'success'=>'js:function(data, textStatus, jqXHR){processCartResponseFromList(data, textStatus, jqXHR, "'.Yii::app()->createAbsoluteUrl('/store/frontProduct/view', array('url'=>$data->url)).'")}',
					), 
					array('style'=>'display:none;', 'id'=>'addProduct'.$data->id));
			}
		?>
		
		
		<?php echo CHtml::endForm() ?>
		<div class="col-lg-8 col-md-8 col-sm-8 product-content no-padding">
			<div class="product-info">
				<h5><?php echo CHtml::link(CHtml::encode($data->name), array('frontProduct/view', 'url'=>$data->url)) ?></h5>
				<span class="price"><?php echo $data->priceRange() ?></span>
				<div class="rating-box">
					<div class="rating readonly-rating" data-score="<?php echo ($data->votes != 0) ? round($data->rating / $data->votes) : 0 ?>"></div>
					<span><?php echo $data->commentsCount; ?> Отзыв(ов)</span>
				</div>
				<p><?php echo $data->short_description ?></p>		
			</div>


			<div class="product-actions full-width">
				<?php if($data->getIsAvailable()): ?>
					<span class="add-to-cart" onclick="return $('#<?php echo 'addProduct'.$data->id; ?>').trigger('click');">
						<span class="action-wrapper">
							<i class="icons icon-basket-2"></i>
							<span class="action-name">В корзину</span>
						</span >
					</span>
				<?php else: ?>
					<span class="add-to-cart" onclick="showNotifierPopup(<?php echo $data->id; ?>); return false;">
						<span class="action-wrapper">
							<i class="icons icon-na"></i>
							<span class="action-name">Нет в наличии</span>
						</span >
					</span>
				<?php endif; ?>
				<span class="add-to-favorites" onclick="return addProductToWishList(<?php echo $data->id ?>);">
					<span class="action-wrapper">
						<i class="icons icon-heart-empty"></i>
						<span class="action-name">В желаемые</span>
					</span>
				</span>
				<span class="add-to-compare" onclick="return addProductToCompare(<?php echo $data->id ?>);">
					<span class="action-wrapper">
						<i class="icons icon-docs"></i>
						<span class="action-name">К сравнению</span>
					</span>
				</span>
			</div>
		</div>
	</div>
	
</div>
<!-- Product Item -->
