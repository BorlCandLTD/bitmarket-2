<?php

/**
 * Category view
 * @var $this CategoryController
 * @var $model StoreCategory
 * @var $provider CActiveDataProvider
 * @var $categoryAttributes
 */

/**
 * Setting layout
 */
$this->layout = '//layouts/layout2';

// Set meta tags
$this->pageTitle = ($this->model->meta_title) ? $this->model->meta_title : $this->model->name;
$this->pageKeywords = $this->model->meta_keywords;
$this->pageDescription = $this->model->meta_description;

// Create breadcrumbs
$ancestors = $this->model->excludeRoot()->ancestors()->findAll();

foreach($ancestors as $c)
	$this->breadcrumbs[$c->name] = $c->getViewUrl();

$this->breadcrumbs[] = $this->model->name;

// Count on page
$limits=array(Yii::app()->request->removeUrlParam('/store/category/view', 'per_page')  => $this->allowedPageLimit[0]);
array_shift($this->allowedPageLimit);
foreach($this->allowedPageLimit as $l){
	$limits[Yii::app()->request->addUrlParam('/store/category/view', array('per_page'=> $l))]=$l;
}

$cpager = Yii::t('StoreModule.core', 'Показывать').' '.CHtml::dropDownList('per_page', Yii::app()->request->url, $limits, array('onchange'=>'applyCategorySorter(this)', 'class'=>'chosen-select')).' '.Yii::t('StoreModule.core', ' на странице');

?>
<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="breadcrumbs">
		<?php
			$this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
				'tagName'=>'p',
				'separator'=>' <i class="icons icon-right-dir"></i> ',
			));
		?>
	</div>
</div>

<!-- Main Content -->
<section class="main-content col-lg-9 col-md-9 col-sm-9">
		
	<div class="row">
	
		<!-- Heading -->
		<div class="col-lg-12 col-md-12 col-sm-12">
			
			<div class="carousel-heading">
				<h4><?php echo CHtml::encode($this->model->name); ?></h4>
				
				
				<?php
					echo CHtml::dropDownList(
						'sorter', 
						Yii::app()->request->url, 
						array(
							Yii::app()->request->removeUrlParam('/store/category/view', 'sort')  => 'Сортировка',
							Yii::app()->request->addUrlParam('/store/category/view', array('sort'=>'price'))  => Yii::t('StoreModule.core', 'Сначала дешевые'),
							Yii::app()->request->addUrlParam('/store/category/view', array('sort'=>'price.desc')) => Yii::t('StoreModule.core', 'Сначала дорогие'),
						), 
						array(
							'onchange'=>'applyCategorySorter(this)',
							'class'=>'chosen-select',
						)
					);
				?>
				
				<div class="category-buttons">
					<a href="<?php echo Yii::app()->request->removeUrlParam('/store/category/view', 'view') ?>"><i class="icons icon-th-3 <?php if($itemView==='_product') echo 'active-button'; ?>"></i></a>
					<a href="<?php echo Yii::app()->request->addUrlParam('/store/category/view',  array('view'=>'wide')) ?>"><i class="icons icon-th-list-4 <?php if($itemView==='_product_wide') echo 'active-button'; ?>"></i></a>
				</div>
			</div>
			
		</div>
		<!-- /Heading -->
	
	</div>
	
	<?php
		$this->widget('application.modules.store.widgets.SListView', array(
			'dataProvider'=>$provider,
			'ajaxUpdate'=>false,
			'template'=>'<div class="row">{summary} <div class="col-lg-6 col-md-6 col-sm-6">{pager}</div></div> <div class="row">{items}</div> <div class="row">{summary} <div class="col-lg-6 col-md-6 col-sm-6">{pager}</div></div>',
			'itemView'=>$itemView,
			'summaryText'=>'<div class="col-lg-6 col-md-6 col-sm-6"><div class="category-results"><p>Элементы {start}&mdash;{end} из {count}</p><p>'.$cpager.'</p></div></div>',
			'summaryTagName'=>'p',
			'emptyTagName'=>'div',
			'emptyCssClass'=>'col-lg-6 col-md-6 col-sm-6',
			'emptyText'=>'<div class="category-results"><p>Нет результатов</p></div>',
			'pagerCssClass'=>'pagination',
			'sortableAttributes'=>array(
				'name', 'price'
			),
		));
	?>
</section>
<!-- /Main Content -->

<!-- Sidebar -->
<aside class="sidebar right-sidebar col-lg-3 col-md-3 col-sm-3">
	<!-- Filter -->
	<div class="row sidebar-box green">
	
		<div class="col-lg-12 col-md-12 col-sm-12">
			
			<div class="sidebar-box-heading">
				<i class="icons icon-filter"></i>
				<h4>Фильтры</h4>
			</div>
			<div class="sidebar-box-content sidebar-padding-box">
				<?php 
					$this->widget('application.modules.store.widgets.filter.SFilterRenderer', array(
						'model'=>$this->model,
						'attributes'=>$this->eavAttributes,
					));
				?>
			</div>
		</div>
		
	</div>
	<!-- Filter -->
	
	
</aside>
<!-- /Sidebar -->
