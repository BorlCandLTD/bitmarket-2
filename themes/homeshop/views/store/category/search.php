<?php

/**
 * Search view
 * @var $this CategoryController
 */

$this->layout = '//layouts/layout1';

// Set meta tags
$this->pageTitle = Yii::t('StoreModule.core', 'Поиск');
$this->breadcrumbs[] = Yii::t('StoreModule.core', 'Поиск');

$limits=array(Yii::app()->request->removeUrlParam('/store/category/search', 'per_page')  => $this->allowedPageLimit[0]);
array_shift($this->allowedPageLimit);
foreach($this->allowedPageLimit as $l){
	$limits[Yii::app()->request->addUrlParam('/store/category/search', array('per_page'=> $l))]=$l;
}

$cpager = Yii::t('StoreModule.core', 'Показывать').' '.CHtml::dropDownList('per_page', Yii::app()->request->url, $limits, array('onchange'=>'applyCategorySorter(this)', 'class'=>'chosen-select')).' '.Yii::t('StoreModule.core', ' на странице');

?>


<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="breadcrumbs">
			<?php
				$this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'tagName'=>'p',
					'separator'=>' <i class="icons icon-right-dir"></i> ',
				));
			?>
		</div>
	</div>

	<!-- Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4>
				<?php
					echo Yii::t('StoreModule.core', 'Результаты поиска');
					if(($q=Yii::app()->request->getParam('q')))
						echo ' "'.CHtml::encode($q).'"';
				?>
			</h4>
			
			<?php
				echo CHtml::dropDownList(
					'sorter', 
					Yii::app()->request->url, 
					array(
						Yii::app()->request->removeUrlParam('/store/category/search', 'sort')  => 'Сортировка',
						Yii::app()->request->addUrlParam('/store/category/search', array('sort'=>'price'))  => Yii::t('StoreModule.core', 'Сначала дешевые'),
						Yii::app()->request->addUrlParam('/store/category/search', array('sort'=>'price.desc')) => Yii::t('StoreModule.core', 'Сначала дорогие'),
					), 
					array(
						'onchange'=>'applyCategorySorter(this)',
						'class'=>'chosen-select',
					)
				);
			?>
			
		</div>
		
	</div>
	<!-- /Heading -->

</div>

<?php
	$this->widget('application.modules.store.widgets.SListView', array(
		'dataProvider'=>$provider,
		'ajaxUpdate'=>false,
		'template'=>'<div class="row">{summary} <div class="col-lg-6 col-md-6 col-sm-6">{pager}</div></div> <div class="row">{items}</div> <div class="row">{summary} <div class="col-lg-6 col-md-6 col-sm-6">{pager}</div></div>',
		'itemView'=>'_product_search',
		'summaryText'=>'<div class="col-lg-6 col-md-6 col-sm-6"><div class="category-results"><p>Элементы {start}&mdash;{end} из {count}</p><p>'.$cpager.'</p></div></div>',
		'summaryTagName'=>'p',
		'emptyTagName'=>'div',
		'emptyCssClass'=>'col-lg-6 col-md-6 col-sm-6',
		'emptyText'=>'<div class="category-results"><p>Нет результатов</p></div>',
		'pagerCssClass'=>'pagination',
		'sortableAttributes'=>array(
			'name', 'price'
		),
	));
?>