<tr>
	<td class="wishlist-image">
		<?php
			if($data->mainImage)
				$imgSource = $data->mainImage->getUrl('200x200');
			else
				$imgSource = 'http://placehold.it/200x200';
			echo CHtml::link(CHtml::image($imgSource, $data->mainImageTitle), array('frontProduct/view', 'url'=>$data->url));
		?>
	</td>
	<td class="wishlist-info">
		<h5><?php echo CHtml::link(CHtml::encode($data->name), array('frontProduct/view', 'url'=>$data->url)) ?></h5>
		<span class="product-category"><?php echo $data->short_description; ?></span><br>
		<div class="rating readonly-rating" data-score="<?php echo ($data->votes != 0) ? round($data->rating / $data->votes) : 0 ?>"></div>
	</td>
	<td class="wishlist-price">
		<?php
			if($data->appliedDiscount){
				echo '<del>'.$data->toCurrentCurrency('originalPrice').' '.Yii::app()->currency->active->symbol.'</del><br/>';
			}

			echo CHtml::openTag('span',array('class'=>'price'));
			echo StoreProduct::formatPrice($data->toCurrentCurrency()); 
			echo ' '.Yii::app()->currency->active->symbol;
			echo CHtml::closeTag('span');
		?>
	</td>
	<td class="wishlist-actions">
		<?php
			echo CHtml::form(array('/orders/cart/add'));
			echo CHtml::hiddenField('product_id', $data->id);
			echo CHtml::hiddenField('product_price', $data->price);
			echo CHtml::hiddenField('use_configurations', $data->use_configurations);
			echo CHtml::hiddenField('currency_rate', Yii::app()->currency->active->rate);
			echo CHtml::hiddenField('configurable_id', 0);
			echo CHtml::hiddenField('quantity', 1);
		
			if($data->getIsAvailable())
			{
				echo CHtml::ajaxSubmitButton(
					Yii::t('StoreModule.core',''), 
					array('/orders/cart/add'), 
					array(
						'id'=>'addProduct'.$data->id,
						'dataType'=>'json',
						'success'=>'js:function(data, textStatus, jqXHR){processCartResponseFromList(data, textStatus, jqXHR, "'.Yii::app()->createAbsoluteUrl('/store/frontProduct/view', array('url'=>$data->url)).'")}',
					), 
					array('style'=>'display:none;', 'id'=>'addProduct'.$data->id));
			}
			
			echo CHtml::endForm();
		?>
		
		
		<?php if($data->getIsAvailable()): ?>
			<a href='#' onclick="$('#<?php echo 'addProduct'.$data->id; ?>').trigger('click'); return false;">
				<span class="add-to-cart">
					<span class="action-wrapper">
						<i class="icons icon-basket-2"></i>
						<span class="action-name">В корзину</span>
					</span >
				</span>
			</a>
		<?php else: ?>
			<span class="add-to-cart" onclick="showNotifierPopup(<?php echo $data->id; ?>); return false;">
				<span class="action-wrapper">
					<i class="icons icon-na"></i>
					<span class="action-name">Нет в наличии</span>
				</span >
			</span>
		<?php endif; ?>
		
		<?php if($this->model->getUserId()===Yii::app()->user->id)
			{
				echo CHtml::link('<span class="add-to-trash"><span class="action-wrapper"><i class="icons icon-trash-8"></i><span class="action-name">'.Yii::t('StoreModule.core', 'Удалить').'</span></span></span>', 
					array(
						'/store/wishlist/remove',
						'id'=>$data->id
					),
					array(
						'class'=>'remove',
					)
				);
			}
		?>		
	</td>
</tr>