<?php

/**
 * Wish list view
 *
 * @var WishlistController $this
 */

$this->layout = '//layouts/layout1';

$this->pageTitle = Yii::t('StoreModule.core', 'Список желаний');
?>



<div class="row">

	<!-- Heading -->
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('StoreModule.core', 'Список желаний') ?></h4>
						
			<div class="category-buttons">
				<a href="mailto:?body=<?php echo $this->model->getPublicLink() ?>&subject=<?php echo Yii::t('StoreModule.core', 'Мой список желаний') ?>"><i class="icons icon-share"></i></a>
			</div>
		</div>
		
	</div>
	<!-- /Heading -->
</div>	


<div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<?php if(!empty($this->model->products)): ?>
			<table class="wishlist-table">
				<tr>
					<th class="wishlist-image">Изображение</th>
					<th>Название/Описание</th>
					<th>Цена</th>
					<th>Действия</th>
				</tr>
				<?php
					foreach($this->model->products as $p)
					{
						$this->renderPartial('_product', array(
							'data'=>$p,
						));
					}
				?>
			</table>
		<?php else: ?>
			<?php echo Yii::t('StoreModule.core', 'Список желаний пуст.').CHtml::openTag('br').CHtml::openTag('br'); ?>
		<?php endif ?>
		
	</div>
	
</div>



