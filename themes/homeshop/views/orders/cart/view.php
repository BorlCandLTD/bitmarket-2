<?php

/**
 * View order
 * @var Order $model
 */
 
$this->layout = '//layouts/layout1';

$title = Yii::t('OrdersModule.core', 'Просмотр заказа #{id}', array('{id}'=>$model->id));
$this->pageTitle = $title;

?>

<div class="row">
                    	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo $title; ?></h4>
			<div class="carousel-arrows">
				<?php echo CHtml::link('<i class="icons icon-share"></i>', array("/orders/cart/view", "secret_key"=>$model->secret_key)); ?>
			</div>
		</div>
		
		<table class="orderinfo-table">
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Номер заказа') ?></th>
				<td><?php echo $model->id; ?></td>
			</tr> 
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Дата заказа') ?></th>
				<td><?php echo $model->created; ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Статус') ?></th>
				<td><?php echo $model->status_name; ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Последнее обновление') ?></th>
				<td><?php echo $model->updated; ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Доставка') ?></th>
				<td><?php echo CHtml::encode($model->delivery_name); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Стоимость доставки') ?></th>
				<td>
					<span class="price">
					<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($model->delivery_price)) ?>
					<?php echo Yii::app()->currency->active->symbol ?>
					</span>
				</td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Комментарий') ?></th>
				<td><?php echo CHtml::encode($model->user_comment); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Всего к оплате') ?></th>
				<td>
					<span class="price">
						<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($model->full_price)) ?>
						<?php echo Yii::app()->currency->active->symbol ?>
					</span>
				</td>
			</tr>                    
			
		</table>
		
	</div>
	
</div>

<div class="row">
	
	<div class="col-lg-6 col-md-6 col-sm-6">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('OrdersModule.core', 'Оплата') ?></h4>
		</div>
		
		<table class="orderinfo-table">
		
			<?php foreach($model->deliveryMethod->paymentMethods as $payment): ?>
			<tr>
				<th><?php echo $payment->name ?><p><?php echo $payment->renderPaymentForm($model) ?></p></th>
				<td><?php echo $payment->description ?></td>
			</tr>
			<?php endforeach ?>
			
		</table>
		
	</div>
	
	<div class="col-lg-6 col-md-6 col-sm-6">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('OrdersModule.core', 'Данные получателя') ?></h4>
		</div>
		
		<table class="orderinfo-table">

			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Имя') ?></th>
				<td><?php echo CHtml::encode($model->user_name); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Email') ?></th>
				<td><?php echo CHtml::encode($model->user_email); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Телефон') ?></th>
				<td><?php echo CHtml::encode($model->user_phone); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Адрес') ?></th>
				<td><?php echo CHtml::encode($model->user_address); ?></td>
			</tr>
			
			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Комментарий') ?></th>
				<td><?php echo CHtml::encode($model->user_comment); ?></td>
			</tr>
			
		</table>
		
	</div>
	
	 <div class="col-lg-12 col-md-12 col-sm-12">
		
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('OrdersModule.core', 'Заказаные товары') ?></h4>
		</div>
		
		<table class="orderinfo-table no-padding">

			<tr>
				<th><?php echo Yii::t('OrdersModule.core', 'Название товара') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Цена') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Количество') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Всего') ?></th>
			</tr> 
			
			<?php foreach($model->getOrderedProducts()->getData() as $product): ?>
			<tr>
				<td>
					<?php echo $product->getRenderFullName(false); ?>
				</td>
				<td>
					<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($product->price)) ?>
					<?php echo Yii::app()->currency->active->symbol; ?>
				</td>
				<td>
					<?php echo $product->quantity ?>
				</td>
				<td>
					<?php echo CHtml::openTag('span', array('class'=>'price')) ?>
					<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($product->price * $product->quantity)); ?>
					<?php echo Yii::app()->currency->active->symbol; ?>
					<?php echo CHtml::closeTag('span') ?>
				</td>
			</tr>
			<?php endforeach ?>
			
			<tr>
				<td class="align-right" colspan="3"><span class="price"><?php echo Yii::t('OrdersModule.core', 'Стоимость доставки') ?></span></td>
				<td>
					<strong>
					<?php echo CHtml::openTag('span', array('class'=>'price')) ?>
						<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($model->delivery_price)) ?>
						<?php echo Yii::app()->currency->active->symbol ?>
					<?php echo CHtml::closeTag('span') ?>
					</strong>
				</td>
			</tr>
			
			<tr>
				<td class="align-right" colspan="3"><span class="price big"><?php echo Yii::t('OrdersModule.core', 'Всего к оплате') ?></span></td>
				<td><span class="price big">
						<?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($model->full_price)) ?>
						<?php echo Yii::app()->currency->active->symbol ?>
					</span>
				</td>
			</tr>
			
		</table>
			
	</div>
	
</div>