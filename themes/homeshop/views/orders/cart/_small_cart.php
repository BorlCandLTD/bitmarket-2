<?php

/**
 * Small cart.
 * This template is loaded thru ajax request after new product added to cart.
 */
?>

<a href="<?php echo Yii::app()->createUrl('/orders/cart/index') ?>"><i class="icons icon-basket-2"></i><?php echo Yii::app()->cart->countItems() ?> товаров <?php Yii::t('core', '{p} {s}', array('{s}'=>Yii::app()->currency->active->symbol, '{p}'=>StoreProduct::formatPrice(Yii::app()->currency->convert(Yii::app()->cart->getTotalPrice())))); ?></a>
<!--ul id="cart-dropdown" class="box-dropdown parent-arrow">
	<li>
		<div class="box-wrapper parent-border">
			<p>Recently added item(s)</p>
			
			<table class="cart-table">
				<tr>
					<td><img src="img/products/sample1.jpg" alt="product"></td>
					<td>
						<h6>Lorem ipsum dolor</h6>
						<p>Product code PSBJ3</p>
					</td>
					<td>
						<span class="quantity"><span class="light">1 x</span> $79.00</span>
						<a href="#" class="parent-color">Remove</a>
					</td>
				</tr>
				<tr>
					<td><img src="img/products/sample1.jpg" alt="product"></td>
					<td>
						<h6>Lorem ipsum dolor</h6>
						<p>Product code PSBJ3</p>
					</td>
					<td>
						<span class="quantity"><span class="light">1 x</span> $79.00</span>
						<a href="#" class="parent-color">Remove</a>
					</td>
				</tr>
				<tr>
					<td><img src="img/products/sample1.jpg" alt="product"></td>
					<td>
						<h6>Lorem ipsum dolor</h6>
						<p>Product code PSBJ3</p>
					</td>
					<td>
						<span class="quantity"><span class="light">1 x</span> $79.00</span>
						<a href="#" class="parent-color">Remove</a>
					</td>
				</tr>
			</table>
			
			<br class="clearfix">
		</div>
		
		<div class="footer">
			<table class="checkout-table pull-right">
				<tr>
					<td class="align-right">Tax:</td>
					<td>$0.00</td>
				</tr>
				<tr>
					<td class="align-right">Discount:</td>
					<td>$37.00</td>
				</tr>
				<tr>
					<td class="align-right"><strong>Total:</strong></td>
					<td><strong class="parent-color">$999.00</strong></td>
				</tr>
			</table>
		</div>
		
		<div class="box-wrapper no-border">
			<a class="button pull-right parent-background" href="#">Checkout</a>
			<a class="button pull-right" href="order_info.html">View Cart</a>
		</div>
	</li>
</ul-->