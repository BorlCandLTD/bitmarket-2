<?php

/**
 * Display cart
 * @var Controller $this
 * @var SCart $cart
 * @var $totalPrice integer
 */
 
$this->layout = '//layouts/layout1';

Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/cart.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('cartScript', "var orderTotalPrice = '$totalPrice';", CClientScript::POS_HEAD);

$this->pageTitle = Yii::t('OrdersModule.core', 'Оформление заказа');

?>

<?php echo CHtml::form() ?>
<div class="row">
	
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<div class="carousel-heading">
			<h4><?php echo Yii::t('OrdersModule.core', 'Корзина') ?></h4>
		</div>
		
		<?php
			if(empty($items))
			{
				echo Yii::t('OrdersModule.core', 'Корзина пуста').'<br><br>';
				return;
			}
		?>
		<table class="shopping-table">
		
			<tr>
				<th colspan="2"><?php echo Yii::t('OrdersModule.core', 'Товар') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Модификации') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Цена') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Количество') ?></th>
				<th><?php echo Yii::t('OrdersModule.core', 'Всего') ?></th>
			</tr>
			
			<?php foreach($items as $index=>$product): ?>
			<?php
				$price = StoreProduct::calculatePrices($product['model'], $product['variant_models'], $product['configurable_id']);
			?>
			<tr>
				<td class="image-column">
				<?php 
				
				// Display image
					if($product['model']->mainImage)
						$imgSource = $product['model']->mainImage->getUrl('80x80');
					else
						$imgSource = 'http://placehold.it/80x80';
					echo CHtml::link(CHtml::image($imgSource, ''), array('/store/frontProduct/view', 'url'=>$product['model']->url));
				
				?>
				</td>
				<td><p><?php echo CHtml::link(CHtml::encode($product['model']->name), array('/store/frontProduct/view', 'url'=>$product['model']->url)); ?></p></td>
				<td>
				
				<?php
					// Display variant options
					if(!empty($product['variant_models']))
					{
						echo CHtml::openTag('p');
						foreach($product['variant_models'] as $variant){
							echo ' &mdash; '.$variant->attribute->title.': '.$variant->option->value;
						}
						echo CHtml::closeTag('p');
					}

					// Display configurable options
					if(isset($product['configurable_model']))
					{
						$attributeModels = StoreAttribute::model()->findAllByPk($product['model']->configurable_attributes);
						echo CHtml::openTag('p');
						foreach($attributeModels as $attribute)
						{
							$method = 'eav_'.$attribute->name;
							echo ' &mdash; '.$attribute->title.': '.$product['configurable_model']->$method;
						}
						echo CHtml::closeTag('p');
					}
					
					if (empty($product['variant_models']) && !isset($product['configurable_model'])){
						echo CHtml::openTag('p');
						echo 'Без модификаций';
						echo CHtml::closeTag('p');
					}
				?>
				</td>
				<td><p><?php echo StoreProduct::formatPrice(Yii::app()->currency->convert($price)).' '.Yii::app()->currency->active->symbol;?></p></td>
				<td class="quantity">
					<?php echo CHtml::textField("quantities[$index]", $product['quantity']) ?>
					
					<a href="#" onclick="$('#recount').trigger('click'); return false;"><i class="icons icon-arrows-cw"></i></a>
					<?php echo CHtml::link('<i class="icons icon-cancel-3"></i>', array('/orders/cart/remove', 'index'=>$index), array('class'=>'red-hover')) ?>
					
				</td>
				<td>
					<p>
						<?php
						echo CHtml::openTag('span', array('class'=>'price'));
						echo StoreProduct::formatPrice(Yii::app()->currency->convert($price * $product['quantity']));
						echo ' '.Yii::app()->currency->active->symbol;
						echo CHtml::closeTag('span');
						?>
					</p>
				</td>
			</tr> 
			<?php endforeach; ?>
			
			<tr>
				<td class="align-right" colspan="5"><span class="price big"><?php echo Yii::t('OrdersModule.core', 'Всего к оплате') ?></span></td>
				<td>
					<span class="price big">
					<?php echo StoreProduct::formatPrice($totalPrice) ?>
					<?php echo Yii::app()->currency->active->symbol ?>
					</span>
				</td>
			</tr>
		</table>
		
	</div>
	
</div>



<div class="row">
	
	<div class="col-lg-6 col-md-6 col-sm-6">
		
		<div class="carousel-heading no-margin">
			<h4>Способ доставки</h4>
		</div>
		
		<div class="page-content">
			
			<div class="row">
			
				<div class="col-lg-12 col-md-12 col-sm-12">
					<p></p>
					<?php foreach($deliveryMethods as $delivery): ?>
						<?php
							echo CHtml::activeRadioButton($this->form, 'delivery_id', array(
								'id'			 => 'delivery'.$delivery->id,
								'checked'        => ($this->form->delivery_id == $delivery->id),
								'uncheckValue'   => null,
								'value'          => $delivery->id,
								'data-price'     => Yii::app()->currency->convert($delivery->price),
								'data-free-from' => Yii::app()->currency->convert($delivery->free_from),
								'onClick'        => 'recountOrderTotalPrice(this);',
							));
						?>
						<label class="radio-label" for="delivery<?php echo $delivery->id; ?>"><?php echo CHtml::encode($delivery->name) ?></label>
						<p><?php echo $delivery->description?></p>
					<?php endforeach; ?>
					
				</div>
			</div>
			
		</div>
		
	</div>
	
	<div class="col-lg-6 col-md-6 col-sm-6">
		
		<div class="carousel-heading no-margin">
			<h4>Информация о получателе</h4>
		</div>
		
		<div class="page-content">
			
			<div class="row">
			
				<div class="col-lg-12 col-md-12 col-sm-12">
					<p></p>
			
					<div class="iconic-input">
						<?php echo CHtml::activeTextField($this->form,'name', array('required'=>true, 'placeholder'=>'Имя получателя')); ?>
						<i class="icons icon-user-3"></i>
					</div>
					
					<div class="iconic-input">
						<?php echo CHtml::activeTextField($this->form,'email', array('required'=>true, 'placeholder'=>'Email')); ?>
						<i class="icons icon-email"></i>
					</div>
					
					<div class="iconic-input">
						<?php echo CHtml::activeTextField($this->form,'phone', array('required'=>true, 'placeholder'=>'Телефон с кодом')); ?>
						<i class="icons icon-phone"></i>
					</div>
					
					<div class="iconic-input">
						<?php echo CHtml::activeTextField($this->form,'address', array('required'=>true, 'placeholder'=>'Адрес получателя')); ?>
						<i class="icons icon-address"></i>
					</div>
					
				</div>
			</div>
			
		</div>
		
	</div>
	  
</div>


<div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12">
	
		<div class="checkout-form">
			
			<?php echo CHtml::errorSummary($this->form); ?>
			<p>Комментарий к заказу</p>
			<div class="iconic-input">
				<?php echo CHtml::activeTextArea($this->form,'comment'); ?>
				<i class="icons icon-comment"></i>
			</div>
			<button id="recount" class="recount" name="recount" type="submit" value="1" style="display: none;">Пересчитать</button>
			<span class="price big">
				<?php echo Yii::t('OrdersModule.core', 'Всего') ?>
				<span id="orderTotalPrice">
					<?php echo StoreProduct::formatPrice($totalPrice); ?>
				</span>
				<?php echo Yii::app()->currency->active->symbol ?>
			</span>
			<br><br>
			<input type="submit" class="red huge" name="create" value="Оформить заказ">
						
		</div>
		
	</div>
		
</div>
<?php echo CHtml::endForm() ?>