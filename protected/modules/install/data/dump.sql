SET SQL_MODE = '';

CREATE TABLE IF NOT EXISTS `accounting1c` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_type` int(11) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ActionLog` (
`id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `event` tinyint(255) DEFAULT NULL,
  `model_name` varchar(50) DEFAULT '',
  `model_title` text,
  `datetime` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

INSERT INTO `ActionLog` (`id`, `username`, `event`, `model_name`, `model_title`, `datetime`) VALUES
(1, 'root', 1, 'Order', NULL, '2015-05-19 11:57:50'),
(2, 'root', 2, 'Order', '1', '2015-05-19 11:58:23'),
(3, 'root', 2, 'Order', '1', '2015-05-19 11:58:32'),
(4, 'root', 2, 'Order', '1', '2015-05-19 11:58:32'),
(5, 'root', 2, 'Order', '1', '2015-05-19 11:58:50'),
(6, 'root', 2, 'Order', '1', '2015-05-19 11:58:50'),
(7, 'root', 2, 'Order', '1', '2015-05-19 12:17:03'),
(8, 'root', 2, 'Order', '1', '2015-05-19 12:17:03'),
(9, 'root', 2, 'StoreCurrency', 'Рубли', '2015-05-19 22:45:32'),
(10, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-20 02:58:44'),
(11, 'root', 2, 'Discount', 'Скидка на всю технику Applek', '2015-05-20 02:59:09'),
(12, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 03:57:17'),
(13, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 03:58:15'),
(14, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 03:58:32'),
(15, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:00:29'),
(16, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:00:57'),
(17, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:01:16'),
(18, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:04:55'),
(19, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:06:48'),
(20, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:07:22'),
(21, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:07:56'),
(22, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:09:26'),
(23, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:09:49'),
(24, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:13:21'),
(25, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:13:41'),
(26, 'root', 2, 'StoreCategory', 'Игровой', '2015-05-20 04:13:55'),
(27, 'root', 2, 'StoreCurrency', 'USD', '2015-05-20 23:32:17'),
(28, 'root', 2, 'StoreCurrency', 'RUB', '2015-05-20 23:34:15'),
(29, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-21 15:49:17'),
(30, 'root', 1, 'Discount', 'HTC', '2015-05-21 15:58:44'),
(31, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:00:08'),
(32, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:01:09'),
(33, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:01:32'),
(34, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:02:10'),
(35, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-21 16:02:44'),
(36, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-21 16:03:43'),
(37, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-21 16:04:11'),
(38, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:04:45'),
(39, 'root', 2, 'Discount', 'Скидка на всю технику Apple', '2015-05-21 16:13:36'),
(40, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:13:54'),
(41, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:14:15'),
(42, 'root', 2, 'Discount', 'HTC', '2015-05-21 16:17:19'),
(43, 'root', 2, 'StoreProduct', 'BlackBerry Bold 9900', '2015-05-22 23:37:16'),
(44, 'root', 3, 'Order', '2', '2015-05-22 23:50:31'),
(45, 'root', 1, 'StoreCategory', 'Компьютерная техника', '2015-05-24 20:02:04'),
(46, 'root', 2, 'StoreCategory', 'Компьютеры', '2015-05-24 20:02:10'),
(47, 'root', 2, 'StoreCategory', 'Ноутбуки', '2015-05-24 20:27:04'),
(48, 'root', 2, 'StoreCategory', 'Ноутбуки', '2015-05-24 21:36:32'),
(49, 'root', 2, 'StoreCategory', 'Ноутбуки', '2015-05-24 21:36:44'),
(50, 'root', 2, 'StoreProduct', 'BlackBerry Bold 9900', '2015-05-27 14:42:55'),
(51, 'root', 2, 'StoreProduct', 'Apple iPad 2 16Gb Wi-Fi + 3G', '2015-05-27 16:12:32'),
(52, 'root', 2, 'StoreAttribute', 'memmory_size', '2015-05-27 16:13:42'),
(53, 'root', 2, 'StoreProduct', 'Apple iPad 2 16Gb Wi-Fi + 3G', '2015-05-27 16:14:46'),
(54, 'root', 2, 'StoreProduct', 'Apple iPad 2 Wi-Fi + 3G', '2015-05-27 17:06:25'),
(55, 'root', 2, 'StoreAttribute', 'memmory_size', '2015-05-27 17:06:47'),
(56, 'root', 2, 'StoreProduct', 'Apple iPad 2 Wi-Fi + 3G', '2015-05-27 17:07:52'),
(57, 'root', 2, 'StoreProduct', 'BlackBerry Bold 9900', '2015-05-27 20:28:50'),
(58, 'root', 2, 'StoreProduct', 'Apple iPad 2 Wi-Fi + 3G', '2015-05-27 20:31:52'),
(59, 'root', 2, 'Comment', 'Я первый!', '2015-05-27 20:34:01'),
(60, 'root', 2, 'StoreProduct', 'BlackBerry Bold 9900', '2015-05-27 21:30:50'),
(61, 'root', 3, 'Order', '4', '2015-05-27 23:30:51'),
(62, 'root', 3, 'Order', '3', '2015-05-27 23:30:51'),
(63, 'root', 3, 'Order', '5', '2015-05-27 23:30:51'),
(64, 'root', 2, 'Page', 'Доставка и оплата', '2015-05-28 03:37:14'),
(65, 'root', 2, 'Page', 'Гарантия', '2015-05-28 03:37:28'),
(66, 'root', 2, 'Page', 'Как сделать заказ', '2015-05-28 03:37:45'),
(67, 'root', 2, 'Page', 'Помощь', '2015-05-28 03:38:00'),
(68, 'root', 2, 'Page', 'Помощь', '2015-05-28 03:45:35'),
(69, 'root', 2, 'Page', 'Как сделать заказ', '2015-05-28 03:46:09'),
(70, 'root', 2, 'Page', 'Гарантия', '2015-05-28 03:46:13'),
(71, 'root', 2, 'Page', 'Доставка и оплата', '2015-05-28 03:46:16'),
(72, 'root', 2, 'StoreCategory', 'Телефоны', '2015-05-29 20:13:38'),
(73, 'root', 2, 'StoreCategory', 'Бюджетный', '2015-05-29 20:14:34'),
(74, 'root', 2, 'Comment', 'А я второй! :(', '2015-05-29 20:16:00'),
(75, 'root', 2, 'Order', '6', '2015-05-30 22:12:37'),
(76, 'root', 2, 'Order', '6', '2015-05-30 22:12:37'),
(77, 'root', 2, 'Order', '6', '2015-05-30 22:12:37'),
(78, 'root', 2, 'Order', '6', '2015-05-30 22:12:37'),
(79, 'root', 2, 'Order', '6', '2015-05-30 22:13:51'),
(80, 'root', 2, 'Order', '6', '2015-05-30 22:13:51'),
(81, 'root', 2, 'Order', '6', '2015-05-30 22:13:51'),
(82, 'root', 2, 'Order', '6', '2015-05-30 22:13:51'),
(83, 'root', 3, 'SystemModules', 'accounting1c', '2015-05-31 02:46:52'),
(84, 'root', 3, 'SystemModules', 'yandexMarket', '2015-05-31 02:47:36'),
(85, 'root', 2, 'Page', 'Google презентовал свои очки дополненной реальности‎', '2015-06-02 17:31:33'),
(86, 'root', 2, 'Page', 'Гарантия', '2015-06-02 17:32:14'),
(87, 'root', 2, 'Page', 'Как сделать заказ', '2015-06-02 17:32:19'),
(88, 'root', 2, 'Page', 'Доставка и оплата', '2015-06-02 17:32:24'),
(89, 'root', 2, 'Page', 'Samsung пытается избежать запрета на Galaxy Nexus', '2015-06-02 17:32:30'),
(90, 'root', 2, 'Page', 'За 8,5 месяцев Android 4.0 попал на 11% устройств', '2015-06-02 17:32:37'),
(91, 'root', 2, 'Page', 'Помощь', '2015-06-02 17:32:44'),
(92, 'root', 2, 'StoreCategory', 'Игровой', '2015-06-02 17:35:28'),
(93, 'root', 1, 'StoreCategory', 'Мобильные устройства', '2015-06-02 17:36:21'),
(94, 'root', 2, 'StoreCategory', 'Телефоны', '2015-06-02 17:36:28'),
(95, 'root', 2, 'StoreCategory', 'Планшеты', '2015-06-02 17:36:35'),
(96, 'root', 2, 'StoreCategory', 'Бюджетные', '2015-06-02 17:37:34'),
(97, 'root', 2, 'StoreCategory', 'Планшеты', '2015-06-02 17:38:26'),
(98, 'root', 2, 'StoreCategory', 'Ноутбуки', '2015-06-02 17:42:22'),
(99, 'root', 2, 'StoreCategory', 'Ноутбуки', '2015-06-02 17:42:27'),
(100, 'root', 2, 'StoreCategory', 'Компьютерная техника', '2015-06-02 17:44:24'),
(101, 'root', 1, 'StoreCategory', 'Переферия', '2015-06-02 18:05:40'),
(102, 'root', 2, 'StoreCategory', 'Мониторы', '2015-06-02 18:05:49'),
(103, 'root', 2, 'StoreCategory', 'Компьютерная акустика', '2015-06-02 18:05:54'),
(104, 'root', 2, 'StoreCategory', 'Игровые', '2015-06-02 18:07:02'),
(105, 'root', 2, 'StoreCategory', 'Периферия ', '2015-06-02 18:09:12'),
(106, 'root', 2, 'StoreCategory', 'Компьютерная техника', '2015-06-02 18:12:31'),
(107, 'root', 2, 'StoreCategory', 'Компьютерная техника', '2015-06-02 18:12:52'),
(108, 'root', 2, 'StoreCategory', 'Мобильные устройства', '2015-06-02 18:14:17'),
(109, 'root', 2, 'StoreCategory', 'Телефоны', '2015-06-02 18:15:06'),
(110, 'root', 2, 'StoreCategory', 'Мониторы', '2015-06-02 18:15:59'),
(111, 'root', 2, 'StoreCategory', 'Акустика', '2015-06-02 18:17:15'),
(112, 'root', 2, 'StoreCategory', 'Акустика', '2015-06-02 18:21:15'),
(113, 'root', 2, 'StoreCategory', 'Акустика', '2015-06-02 18:21:31'),
(114, 'root', 2, 'StoreCategory', 'Периферия ', '2015-06-02 18:22:11'),
(115, 'root', 2, 'StoreCategory', 'Периферия ', '2015-06-02 18:42:54'),
(116, 'root', 2, 'StoreCategory', 'Компьютеры', '2015-06-02 18:43:35'),
(117, 'root', 1, 'StoreCategory', 'В сборе', '2015-06-02 18:47:20'),
(118, 'root', 2, 'StoreCategory', 'В сборе', '2015-06-02 18:47:30'),
(119, 'root', 2, 'StoreCategory', 'В сборе', '2015-06-02 18:47:37'),
(120, 'root', 1, 'StoreCategory', 'Комплектующие', '2015-06-02 18:48:01'),
(121, 'root', 2, 'StoreCategory', 'Комплектующие', '2015-06-02 18:48:19'),
(122, 'root', 2, 'StoreCategory', 'Комплектующие', '2015-06-02 18:48:23'),
(123, 'root', 2, 'StoreCategory', 'Игровые', '2015-06-02 18:48:29'),
(124, 'root', 2, 'StoreCategory', 'Компьютерная техника', '2015-06-02 18:48:51'),
(125, 'root', 2, 'StoreCategory', 'Компьютерная техника', '2015-06-02 18:49:10'),
(126, 'root', 2, 'StoreCategory', 'Мобильные устройства', '2015-06-02 18:49:30'),
(127, 'root', 2, 'StoreCategory', 'Периферия ', '2015-06-02 18:50:10'),
(128, 'root', 2, 'StoreCategory', 'Планшеты', '2015-06-02 18:51:01'),
(129, 'root', 1, 'StoreCategory', 'Аксессуары', '2015-06-02 18:51:13'),
(130, 'root', 2, 'StoreCategory', 'Аксессуары', '2015-06-02 18:51:56'),
(131, 'root', 2, 'StoreCategory', 'Аксессуары', '2015-06-02 18:55:51'),
(132, 'root', 2, 'StoreCategory', 'Аксессуары', '2015-06-02 18:56:17'),
(133, 'root', 1, 'StoreCategory', 'Аксессуары для телефонов', '2015-06-02 18:56:47'),
(134, 'root', 2, 'StoreCategory', 'Аксессуары для телефонов', '2015-06-02 18:56:54'),
(135, 'root', 2, 'StoreCategory', 'Аксессуары для планшетов', '2015-06-02 18:57:15'),
(136, 'root', 2, 'StoreProduct', 'BlackBerry Bold 9900', '2015-06-02 19:05:24'),
(137, 'root', 3, 'Order', '8', '2015-06-16 20:29:01'),
(138, 'root', 3, 'Order', '7', '2015-06-16 20:29:02'),
(139, 'root', 3, 'Order', '6', '2015-06-16 20:29:02'),
(140, 'root', 3, 'Order', '9', '2015-06-16 20:29:02'),
(141, 'root', 1, 'StoreCurrency', 'Фунты стерлинги', '2015-06-16 20:39:14'),
(142, 'root', 3, 'StoreCurrency', 'Фунты стерлинги', '2015-06-16 20:39:38'),
(143, 'root', 1, 'StoreCurrency', 'GBP', '2015-06-16 20:48:08'),
(144, 'root', 1, 'StoreProduct', 'пнгор', '2015-06-17 01:38:09'),
(145, 'root', 2, 'StoreProduct', 'пнгор', '2015-06-17 01:39:03'),
(146, 'root', 3, 'StoreProduct', 'пнгор', '2015-06-17 03:47:21');

CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `AuthAssignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, NULL),
('Authenticated', '57', NULL, 'N;'),
('Authenticated', '58', NULL, 'N;'),
('Orders.Orders.*', '56', NULL, 'N;'),
('Orders.Statuses.*', '56', NULL, 'N;'),
('Authenticated', '56', NULL, 'N;'),
('Authenticated', '62', NULL, 'N;'),
('Authenticated', '63', NULL, 'N;'),
('Authenticated', '2', NULL, 'N;');

CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `AuthItem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Admin', 2, NULL, NULL, 'N;'),
('Authenticated', 2, NULL, NULL, 'N;'),
('Guest', 2, NULL, NULL, 'N;'),
('Orders.Orders.*', 1, NULL, NULL, 'N;'),
('Orders.Statuses.*', 1, NULL, NULL, 'N;'),
('Orders.Orders.Index', 0, NULL, NULL, 'N;'),
('Orders.Orders.Create', 0, NULL, NULL, 'N;'),
('Orders.Orders.Update', 0, NULL, NULL, 'N;'),
('Orders.Orders.AddProductList', 0, NULL, NULL, 'N;'),
('Orders.Orders.AddProduct', 0, NULL, NULL, 'N;'),
('Orders.Orders.RenderOrderedProducts', 0, NULL, NULL, 'N;'),
('Orders.Orders.JsonOrderedProducts', 0, NULL, NULL, 'N;'),
('Orders.Orders.Delete', 0, NULL, NULL, 'N;'),
('Orders.Orders.DeleteProduct', 0, NULL, NULL, 'N;'),
('Orders.Statuses.Index', 0, NULL, NULL, 'N;'),
('Orders.Statuses.Create', 0, NULL, NULL, 'N;'),
('Orders.Statuses.Update', 0, NULL, NULL, 'N;'),
('Orders.Statuses.Delete', 0, NULL, NULL, 'N;');

CREATE TABLE IF NOT EXISTS `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Comments` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `class_name` varchar(100) DEFAULT '',
  `object_pk` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `email` varchar(255) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  `text` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `Comments` (`id`, `user_id`, `class_name`, `object_pk`, `status`, `email`, `name`, `text`, `created`, `updated`, `ip_address`) VALUES
(1, 1, 'application.modules.store.models.StoreProduct', 39, 1, 'borlcand@gmail.com', 'root', 'Я первый!', '2015-05-27 20:32:24', '2015-05-27 20:34:01', '127.0.0.1'),
(2, 1, 'application.modules.store.models.StoreProduct', 31, 1, 'borlcand@gmail.com', 'root', 'А я второй! :(', '2015-05-29 20:15:37', '2015-05-29 20:16:00', '127.0.0.1');

CREATE TABLE IF NOT EXISTS `Discount` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `active` tinyint(1) DEFAULT NULL,
  `sum` varchar(10) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `Discount` (`id`, `name`, `active`, `sum`, `start_date`, `end_date`, `roles`) VALUES
(1, 'Скидка на всю технику Apple', 1, '5%', '2015-05-19 02:37:04', '2016-01-01 12:00:00', '[]'),
(2, 'HTC', 1, '5', '2014-09-03 16:00:30', '2016-04-13 16:00:30', '[]');

CREATE TABLE IF NOT EXISTS `DiscountCategory` (
`id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

INSERT INTO `DiscountCategory` (`id`, `discount_id`, `category_id`) VALUES
(379, 1, 236),
(378, 1, 235),
(377, 1, 237),
(376, 1, 234),
(375, 1, 233),
(374, 1, 232),
(373, 1, 231),
(372, 1, 230),
(371, 1, 1),
(382, 2, 1),
(383, 2, 230),
(384, 2, 231),
(385, 2, 232),
(386, 2, 233),
(387, 2, 234),
(388, 2, 237),
(389, 2, 235),
(390, 2, 236);

CREATE TABLE IF NOT EXISTS `DiscountManufacturer` (
`id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

INSERT INTO `DiscountManufacturer` (`id`, `discount_id`, `manufacturer_id`) VALUES
(46, 1, 6),
(50, 2, 17),
(47, 1, 16);

CREATE TABLE IF NOT EXISTS `grid_view_filter` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `grid_id` varchar(100) DEFAULT '',
  `name` varchar(100) DEFAULT '',
  `data` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `notifications` (
`id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Order` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `secret_key` varchar(10) DEFAULT '',
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` float(10,2) DEFAULT NULL,
  `total_price` float(10,2) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL COMMENT 'delivery address',
  `user_phone` varchar(30) DEFAULT NULL,
  `user_comment` varchar(500) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `admin_comment` text
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `Order` (`id`, `user_id`, `secret_key`, `delivery_id`, `delivery_price`, `total_price`, `status_id`, `paid`, `user_name`, `user_email`, `user_address`, `user_phone`, `user_comment`, `ip_address`, `created`, `updated`, `discount`, `admin_comment`) VALUES
(1, 1, 'nvib8fuhsg', 15, 0.00, 2850.00, 5, 1, 'Андрей Пуховский', 'borlcand@gmail.com', '', '', '', '127.0.0.1', '2015-05-19 11:57:50', '2015-05-19 12:17:03', '10', '');

CREATE TABLE IF NOT EXISTS `OrderHistory` (
`id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `data_before` text,
  `data_after` text,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `OrderHistory` (`id`, `order_id`, `user_id`, `username`, `handler`, `data_before`, `data_after`, `created`) VALUES
(1, 1, 1, 'root', 'product', 'a:4:{s:7:"deleted";b:0;s:4:"name";s:19:"DELL ALIENWARE M17x";s:5:"price";s:7:"2850.00";s:8:"quantity";s:1:"1";}', '', '2015-05-19 11:58:23'),
(2, 1, 1, 'root', 'attributes', 'a:1:{s:9:"status_id";s:10:"Новый";}', 'a:1:{s:9:"status_id";s:18:"Доставлен";}', '2015-05-19 12:17:03');

CREATE TABLE IF NOT EXISTS `OrderProduct` (
`id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `configurable_id` int(11) DEFAULT NULL,
  `name` text,
  `configurable_name` text COMMENT 'Store name of configurable product',
  `configurable_data` text,
  `variants` text,
  `quantity` smallint(6) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

INSERT INTO `OrderProduct` (`id`, `order_id`, `product_id`, `configurable_id`, `name`, `configurable_name`, `configurable_data`, `variants`, `quantity`, `sku`, `price`) VALUES
(88, 1, 8, NULL, 'DELL ALIENWARE M17x', NULL, NULL, NULL, 1, NULL, 2850.00);

CREATE TABLE IF NOT EXISTS `OrderStatus` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `position` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `OrderStatus` (`id`, `name`, `position`) VALUES
(1, 'Новый', 0),
(5, 'Доставлен', 1);

CREATE TABLE IF NOT EXISTS `Page` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT '',
  `layout` varchar(2555) DEFAULT '',
  `view` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO `Page` (`id`, `user_id`, `category_id`, `url`, `created`, `updated`, `publish_date`, `status`, `layout`, `view`) VALUES
(8, 1, NULL, 'help', '2012-06-10 22:35:51', '2015-06-02 17:32:44', '2015-06-01 17:31:54', 'published', '', ''),
(9, 1, NULL, 'how-to-create-order', '2012-06-10 22:36:50', '2015-06-02 17:32:19', '2015-06-01 17:31:53', 'published', '', ''),
(10, 1, NULL, 'garantiya', '2012-06-10 22:37:06', '2015-06-02 17:32:14', '2015-06-01 17:31:52', 'published', '', ''),
(11, 1, NULL, 'dostavka-i-oplata', '2012-06-10 22:37:18', '2015-06-02 17:32:24', '2015-06-01 17:31:51', 'published', '', ''),
(12, 1, 7, 'samsung-pitaetsya-izbezhat-zapreta-na-galaxy-nexus-v-shtatah-pri-pomoshi-patcha', '2012-07-07 12:08:50', '2015-06-02 17:32:30', '2015-06-01 17:31:50', 'published', '', ''),
(13, 1, 7, 'za-85-mesyacev-android-40-popal-na-11-ustroistv', '2012-07-07 12:19:44', '2015-06-02 17:32:37', '2015-06-01 17:31:49', 'published', '', ''),
(14, 1, 7, 'google-prezentoval-svoi-ochki-dopolnennoi-realnosti', '2012-07-07 12:26:11', '2015-06-02 17:31:33', '2015-06-01 17:30:34', 'published', '', '');

CREATE TABLE IF NOT EXISTS `PageCategory` (
`id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `full_url` text,
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `page_size` smallint(6) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `PageCategory` (`id`, `parent_id`, `url`, `full_url`, `layout`, `view`, `created`, `updated`, `page_size`) VALUES
(7, NULL, 'news', 'news', '', '', '2012-07-07 12:06:03', '2013-04-29 23:24:05', NULL),
(10, NULL, 'tesstovya2', 'tesstovya2', '', '', '2013-05-21 23:59:34', '2013-05-21 23:59:34', NULL);

CREATE TABLE IF NOT EXISTS `PageCategoryTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `PageCategoryTranslate` (`id`, `object_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(13, 7, 1, 'Новости', '', '', '', ''),
(14, 7, 9, 'Новости', '', '', '', ''),
(15, 11, 1, 'sdfsdf', '', '', '', ''),
(16, 11, 9, 'sdfsdf', '', '', '', ''),
(17, 12, 1, 'Тесстовя2', '', '', '', ''),
(18, 12, 9, 'Тесстовя2', '', '', '', '');

CREATE TABLE IF NOT EXISTS `PageTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `short_description` text,
  `full_description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

INSERT INTO `PageTranslate` (`id`, `object_id`, `language_id`, `title`, `short_description`, `full_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(22, 11, 9, 'Доставка и оплата', '', '', '', '', ''),
(15, 8, 1, 'Помощь', 'Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).', '', '', '', ''),
(16, 8, 9, 'Помощь', '', '', '', '', ''),
(17, 9, 1, 'Как сделать заказ', '<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений. В результате сгенерированный Lorem Ipsum выглядит правдоподобно, не имеет повторяющихся абзацей или "невозможных" слов.</p>', '', '', '', ''),
(18, 9, 9, 'Как сделать заказ', '', '', '', '', ''),
(19, 10, 1, 'Гарантия', '<p>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе.</p>\r\n<p>В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла"), написанной Цицероном в 45 году н.э. Этот трактат по теории этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, "Lorem ipsum dolor sit amet..", происходит от одной из строк в разделе 1.10.32 Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33 "de Finibus Bonorum et Malorum" Цицерона и их английский перевод, сделанный H. Rackham, 1914 год.</p>', '', '', '', ''),
(20, 10, 9, 'Гарантия', '', '', '', '', ''),
(21, 11, 1, 'Доставка и оплата', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>  ', '', '', '', ''),
(23, 12, 1, 'Samsung пытается избежать запрета на Galaxy Nexus', 'Текущую ситуацию с судебным противостоянием Apple и Samsung в Штатах (ссылка по теме) можно описать строчкой их двух слов: всё плохо. ', 'В смысле всё плохо для Samsung — южнокорейская корпорация так и не сумела отбиться от назначенного судом предварительного запрета на американские продажи планшетников Galaxy Tab 10.1 и, главное, новейших смартфонов Galaxy Nexus. Расклад намечается хуже некуда: по некоторым выкладкам, потенциальный ущерб от подобного запрета может достигнуть 180 млн. долларов, две трети из которых придутся на непроданные Galaxy Nexus.', '', '', ''),
(25, 13, 1, 'За 8,5 месяцев Android 4.0 попал на 11% устройств', 'В свое время платформа Android 2.x распространялась активнее, чем Android 4.0 Ice Cream Sandwich, а ведь уже появилась новая мобильная ОС от Google — Android 4.1 Jelly Bean. За 8,5 месяцев своего существования Android ICS попал на 10,9% устройств, а лидировать на рынке продолжает Android 2,3 Gingerbread.', '', '', '', ''),
(24, 12, 9, 'Samsung пытается избежать запрета на Galaxy Nexus в Штатах при помощи патча', 'Текущую ситуацию с судебным противостоянием Apple и Samsung в Штатах (ссылка по теме) можно описать строчкой их двух слов: всё плохо. В смысле всё плохо для Samsung — южнокорейская корпорация так и не сумела отбиться от назначенного судом предварительного запрета на американские продажи планшетников Galaxy Tab 10.1 и, главное, новейших смартфонов Galaxy Nexus. Расклад намечается хуже некуда: по некоторым выкладкам, потенциальный ущерб от подобного запрета может достигнуть 180 млн. долларов, две трети из которых придутся на непроданные Galaxy Nexus.', '', '', '', ''),
(26, 13, 9, 'За 8,5 месяцев Android 4.0 попал на 11% устройств', 'В свое время платформа Android 2.x распространялась активнее, чем Android 4.0 Ice Cream Sandwich, а ведь уже появилась новая мобильная ОС от Google — Android 4.1 Jelly Bean. За 8,5 месяцев своего существования Android ICS попал на 10,9% устройств, а лидировать на рынке продолжает Android 2,3 Gingerbread.', '', '', '', ''),
(27, 14, 1, 'Google презентовал свои очки дополненной реальности‎', 'Компания Google приступит к продаже очков дополненной реальности, который разрабатываются в рамках проекта Google Project Glass, пишет блог All Things Digital. ', 'Очки будут стоить 1,5 тысячи долларов, и поставки стартуют в начале 2013 года. Устройство, однако, будет предназначено только для разработчиков. Оформить предварительный заказ на него смогут исключительно посетители конференции I/O, открывшейся 27 июня в Сан-Франциско. ', '', '', ''),
(28, 14, 9, 'Google презентовал свои очки дополненной реальности‎', 'Компания Google приступит к продаже очков дополненной реальности, который разрабатываются в рамках проекта Google Project Glass, пишет блог All Things Digital. ', 'Очки будут стоить 1,5 тысячи долларов, и поставки стартуют в начале 2013 года. Устройство, однако, будет предназначено только для разработчиков. Оформить предварительный заказ на него смогут исключительно посетители конференции I/O, открывшейся 27 июня в Сан-Франциско. ', '', '', '');

CREATE TABLE IF NOT EXISTS `Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `StoreAttribute` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `type` tinyint(4) DEFAULT NULL,
  `display_on_front` tinyint(1) DEFAULT '1',
  `use_in_filter` tinyint(1) DEFAULT NULL,
  `use_in_variants` tinyint(1) DEFAULT NULL,
  `use_in_compare` tinyint(1) DEFAULT '0',
  `select_many` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

INSERT INTO `StoreAttribute` (`id`, `name`, `type`, `display_on_front`, `use_in_filter`, `use_in_variants`, `use_in_compare`, `select_many`, `position`, `required`) VALUES
(1, 'processor_manufacturer', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(2, 'freq', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(3, 'memmory', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(4, 'memmory_type', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(5, 'screen', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(6, 'screen_dimension', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(7, 'rms_power', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(8, 'corpus_material', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(9, 'sound_type', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(10, 'monitor_diagonal', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(11, 'monitor_dimension', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(12, 'view_angle', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(13, 'phone_platform', 3, 1, 1, NULL, 1, NULL, 0, NULL),
(14, 'phone_weight', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(15, 'phone_display', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(16, 'phone_camera', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(17, 'tablet_screen_size', 3, 1, NULL, NULL, 1, NULL, 0, NULL),
(18, 'memmory_size', 3, 1, 1, 1, 1, 0, 0, 0),
(19, 'weight', 3, 1, NULL, NULL, 1, NULL, 0, NULL);

CREATE TABLE IF NOT EXISTS `StoreAttributeOption` (
`id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

INSERT INTO `StoreAttributeOption` (`id`, `attribute_id`, `position`) VALUES
(86, 1, NULL),
(87, 2, NULL),
(88, 3, NULL),
(89, 4, NULL),
(90, 5, NULL),
(91, 6, NULL),
(92, 1, NULL),
(93, 2, NULL),
(94, 2, NULL),
(95, 2, NULL),
(96, 1, NULL),
(97, 2, NULL),
(98, 3, NULL),
(99, 6, NULL),
(100, 2, NULL),
(101, 6, NULL),
(102, 3, NULL),
(103, 5, NULL),
(104, 1, NULL),
(105, 2, NULL),
(106, 5, NULL),
(107, 2, NULL),
(108, 7, NULL),
(109, 8, NULL),
(110, 9, NULL),
(111, 7, NULL),
(112, 7, NULL),
(113, 8, NULL),
(114, 7, NULL),
(115, 9, NULL),
(116, 7, NULL),
(117, 7, NULL),
(118, 10, NULL),
(119, 11, NULL),
(120, 12, NULL),
(121, 10, NULL),
(122, 10, NULL),
(123, 12, NULL),
(124, 10, NULL),
(125, 11, NULL),
(126, 13, NULL),
(127, 14, NULL),
(128, 15, NULL),
(129, 16, NULL),
(130, 14, NULL),
(131, 15, NULL),
(132, 16, NULL),
(133, 14, NULL),
(134, 13, NULL),
(135, 14, NULL),
(136, 15, NULL),
(137, 14, NULL),
(138, 16, NULL),
(139, 14, NULL),
(140, 13, NULL),
(141, 15, NULL),
(142, 13, NULL),
(143, 14, NULL),
(144, 15, NULL),
(145, 14, NULL),
(146, 15, NULL),
(147, 17, NULL),
(148, 6, NULL),
(149, 18, 0),
(150, 19, NULL),
(151, 18, 1),
(152, 17, NULL),
(153, 6, NULL),
(154, 19, NULL),
(155, 18, 2),
(156, 19, NULL),
(157, 17, NULL),
(158, 6, NULL),
(159, 19, NULL),
(160, 19, NULL);

CREATE TABLE IF NOT EXISTS `StoreAttributeOptionTranslate` (
`id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=325 DEFAULT CHARSET=utf8;

INSERT INTO `StoreAttributeOptionTranslate` (`id`, `language_id`, `object_id`, `value`) VALUES
(171, 1, 86, 'Celeron'),
(172, 9, 86, 'Celeron'),
(173, 1, 87, '2200 МГц'),
(174, 9, 87, '2200 МГц'),
(175, 1, 88, '2048 Мб'),
(176, 9, 88, '2048 Мб'),
(177, 1, 89, 'DDR3'),
(178, 9, 89, 'DDR3'),
(179, 1, 90, '15.6 дюйм'),
(180, 9, 90, '15.6 дюйм'),
(181, 1, 91, '1366x768'),
(182, 9, 91, '1366x768'),
(183, 1, 92, 'E-240'),
(184, 9, 92, 'E-240'),
(185, 1, 93, '1650 МГц'),
(186, 9, 93, '1650 МГц'),
(187, 1, 94, '1500 МГц'),
(188, 9, 94, '1500 МГц'),
(189, 1, 95, '1700 МГц'),
(190, 9, 95, '1700 МГц'),
(191, 1, 96, 'Core i7'),
(192, 9, 96, 'Core i7'),
(193, 1, 97, '2500 МГц'),
(194, 9, 97, '2500 МГц'),
(195, 1, 98, '8192 Мб'),
(196, 9, 98, '8192 Мб'),
(197, 1, 99, '1920x1080'),
(198, 9, 99, '1920x1080'),
(199, 1, 100, '2700 МГц'),
(200, 9, 100, '2700 МГц'),
(201, 1, 101, '1920x1200'),
(202, 9, 101, '1920x1200'),
(203, 1, 102, '4096 Мб'),
(204, 9, 102, '4096 Мб'),
(205, 1, 103, '15.4 дюйм'),
(206, 9, 103, '15.4 дюйм'),
(207, 1, 104, 'Core i5'),
(208, 9, 104, 'Core i5'),
(209, 1, 105, '2660 МГц'),
(210, 9, 105, '2660 МГц'),
(211, 1, 106, '16.4 дюйм'),
(212, 9, 106, '16.4 дюйм'),
(213, 1, 107, '1730 МГц'),
(214, 9, 107, '1730 МГц'),
(215, 1, 108, '71 Вт'),
(216, 9, 108, '71 Вт'),
(217, 1, 109, 'пластик'),
(218, 9, 109, 'пластик'),
(219, 1, 110, '5.1'),
(220, 9, 110, '5.1'),
(221, 1, 111, '62 Вт'),
(222, 9, 111, '62 Вт'),
(223, 1, 112, '80 Вт'),
(224, 9, 112, '80 Вт'),
(225, 1, 113, 'MDF'),
(226, 9, 113, 'MDF'),
(227, 1, 114, '25 Вт'),
(228, 9, 114, '25 Вт'),
(229, 1, 115, '2.1'),
(230, 9, 115, '2.1'),
(231, 1, 116, '30 Вт'),
(232, 9, 116, '30 Вт'),
(233, 1, 117, '28 Вт'),
(234, 9, 117, '28 Вт'),
(235, 1, 118, '24"'),
(236, 9, 118, '24"'),
(237, 1, 119, '1920x1200'),
(238, 9, 119, '1920x1200'),
(239, 1, 120, '178°/178°'),
(240, 9, 120, '178°/178°'),
(241, 1, 121, '23"'),
(242, 9, 121, '23"'),
(243, 1, 122, '21.5"'),
(244, 9, 122, '21.5"'),
(245, 1, 123, '170°/160°'),
(246, 9, 123, '170°/160°'),
(247, 1, 124, '27"'),
(248, 9, 124, '27"'),
(249, 1, 125, '2560x1440'),
(250, 9, 125, '2560x1440'),
(251, 1, 126, 'Android'),
(252, 9, 126, 'Android'),
(253, 1, 127, '118 г'),
(254, 9, 127, '118 г'),
(255, 1, 128, '3.8"'),
(256, 9, 128, '3.8"'),
(257, 1, 129, '5 МП'),
(258, 9, 129, '5 МП'),
(259, 1, 130, '129 г'),
(260, 9, 130, '129 г'),
(261, 1, 131, '4.7"'),
(262, 9, 131, '4.7"'),
(263, 1, 132, '8 МП'),
(264, 9, 132, '8 МП'),
(265, 1, 133, '162 г'),
(266, 9, 133, '162 г'),
(267, 1, 134, 'iOS'),
(268, 9, 134, 'iOS'),
(269, 1, 135, '140 г'),
(270, 9, 135, '140 г'),
(271, 1, 136, '3.5"'),
(272, 9, 136, '3.5"'),
(273, 1, 137, '135 г'),
(274, 9, 137, '135 г'),
(275, 1, 138, '3 МП'),
(276, 9, 138, '3 МП'),
(277, 1, 139, '137 г'),
(278, 9, 139, '137 г'),
(279, 1, 140, 'MeeGo'),
(280, 9, 140, 'MeeGo'),
(281, 1, 141, '3.9"'),
(282, 9, 141, '3.9"'),
(283, 1, 142, 'BlackBerry OS'),
(284, 9, 142, 'BlackBerry OS'),
(285, 1, 143, '130 г'),
(286, 9, 143, '130 г'),
(287, 1, 144, '2.8"'),
(288, 9, 144, '2.8"'),
(289, 1, 145, '122 г'),
(290, 9, 145, '122 г'),
(291, 1, 146, '2.4"'),
(292, 9, 146, '2.4"'),
(293, 1, 147, '9.7"'),
(294, 9, 147, '9.7"'),
(295, 1, 148, '1024x768'),
(296, 9, 148, '1024x768'),
(297, 1, 149, '16 Гб'),
(298, 9, 149, '16 Гб'),
(299, 1, 150, '613 г'),
(300, 9, 150, '613 г'),
(301, 1, 151, '64 Гб'),
(302, 9, 151, '64 Гб'),
(303, 1, 152, '7"'),
(304, 9, 152, '7"'),
(305, 1, 153, '1024x600'),
(306, 9, 153, '1024x600'),
(307, 1, 154, '343 г'),
(308, 9, 154, '343 г'),
(309, 1, 155, '8 Гб'),
(310, 9, 155, '8 Гб'),
(311, 1, 156, '410 г'),
(312, 9, 156, '410 г'),
(313, 1, 157, '10.1"'),
(314, 9, 157, '10.1"'),
(315, 1, 158, '1280x800'),
(316, 9, 158, '1280x800'),
(317, 1, 159, '586 г'),
(318, 9, 159, '586 г'),
(319, 1, 160, '565 г'),
(320, 9, 160, '565 г');

CREATE TABLE IF NOT EXISTS `StoreAttributeTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

INSERT INTO `StoreAttributeTranslate` (`id`, `object_id`, `language_id`, `title`) VALUES
(41, 1, 1, 'Тип процессора'),
(42, 1, 9, 'Тип процессора'),
(43, 2, 1, 'Частота процессора'),
(44, 2, 9, 'Частота процессора'),
(45, 3, 1, 'Объем памяти'),
(46, 3, 9, 'Объем памяти'),
(47, 4, 1, 'Тип памяти'),
(48, 4, 9, 'Тип памяти'),
(49, 5, 1, 'Диагональ'),
(50, 5, 9, 'Диагональ'),
(51, 6, 1, 'Разрешение'),
(52, 6, 9, 'Разрешение'),
(53, 7, 1, 'Суммарная мощность'),
(54, 7, 9, 'Суммарная мощность'),
(55, 8, 1, 'Материал'),
(56, 8, 9, 'Материал'),
(57, 9, 1, 'Тип'),
(58, 9, 9, 'Тип'),
(59, 10, 1, 'Диагональ'),
(60, 10, 9, 'Диагональ'),
(61, 11, 1, 'Разрешение'),
(62, 11, 9, 'Разрешение'),
(63, 12, 1, 'Угол обзора'),
(64, 12, 9, 'Угол обзора'),
(65, 13, 1, 'Платформа'),
(66, 13, 9, 'Платформа'),
(67, 14, 1, 'Вес'),
(68, 14, 9, 'Вес'),
(69, 15, 1, 'Диагональ'),
(70, 15, 9, 'Диагональ'),
(71, 16, 1, 'Камера'),
(72, 16, 9, 'Камера'),
(73, 17, 1, 'Диагональ'),
(74, 17, 9, 'Диагональ'),
(75, 18, 1, 'Объем памяти'),
(76, 18, 9, 'Объем памяти'),
(77, 19, 1, 'Вес'),
(78, 19, 9, 'Вес');

CREATE TABLE IF NOT EXISTS `StoreCategory` (
`id` int(10) unsigned NOT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rgt` int(10) unsigned DEFAULT NULL,
  `level` smallint(5) unsigned DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `full_path` varchar(255) DEFAULT '',
  `color` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `description` text
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

INSERT INTO `StoreCategory` (`id`, `lft`, `rgt`, `level`, `url`, `full_path`, `color`, `code`, `layout`, `view`, `description`) VALUES
(1, 1, 40, 1, 'root', '', NULL, NULL, '', '', NULL),
(230, 11, 16, 3, 'noutbuki', 'kompyuternaya-tehnika/noutbuki', 'red', 'laptop', '', '', 'Бюджетные & Игровые'),
(231, 14, 15, 4, 'byudzhetnie', 'noutbuki/byudzhetnie', 'orange', '', '', '', ''),
(232, 12, 13, 4, 'igrovie', 'kompyuternaya-tehnika/noutbuki/igrovie', '', '', '', '', ''),
(233, 17, 22, 3, 'kompyuteri', 'kompyuternaya-tehnika/kompyuteri', '', 'desktop-2', '', '', ''),
(234, 37, 38, 3, 'akustika', 'periferiya/akustika', '', 'headphones', '', '', ''),
(235, 35, 36, 3, 'monitori', 'periferiya/monitori', '', 'monitor', '', '', ''),
(236, 29, 32, 3, 'telefoni', 'mobilnie-ustroistva/telefoni', 'purple', 'mobile-3', '', '', ''),
(237, 25, 28, 3, 'plansheti', 'mobilnie-ustroistva/plansheti', '', 'tablet', '', '', ''),
(238, 10, 23, 2, 'kompyuternaya-tehnika', 'kompyuternaya-tehnika', 'blue', 'desktop', '', '', 'Компьютеры &amp; Ноутбуки'),
(239, 24, 33, 2, 'mobilnie-ustroistva', 'mobilnie-ustroistva', 'red', 'mobile', '', '', 'Телефоны &amp; Планшеты'),
(240, 34, 39, 2, 'periferiya', 'periferiya', 'orange', 'floppy', '', '', 'Различная компьютерная периферия'),
(241, 18, 19, 4, 'v-sbore', 'kompyuternaya-tehnika/kompyuteri/v-sbore', '', 'desktop-circled', '', '', ''),
(242, 20, 21, 4, 'komplektuyushie', 'kompyuternaya-tehnika/kompyuteri/komplektuyushie', '', '', '', '', ''),
(243, 26, 27, 4, 'aksessuari-dlya-planshetov', 'mobilnie-ustroistva/plansheti/aksessuari-dlya-planshetov', '', 'gift', '', '', ''),
(244, 30, 31, 4, 'aksessuari-dlya-telefonov', 'mobilnie-ustroistva/telefoni/aksessuari-dlya-telefonov', '', '', '', '', '');

CREATE TABLE IF NOT EXISTS `StoreCategoryTranslate` (
`id` int(10) unsigned NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` varchar(255) DEFAULT '',
  `meta_description` varchar(255) DEFAULT '',
  `description` text
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

INSERT INTO `StoreCategoryTranslate` (`id`, `object_id`, `language_id`, `name`, `meta_title`, `meta_keywords`, `meta_description`, `description`) VALUES
(1, 1, 1, 'root', '', '', '', NULL),
(22, 219, 1, 'Планшеты', '', '', '', NULL),
(23, 219, 9, 'Планшеты', '', '', '', NULL),
(24, 220, 1, 'Ассортимент', '', '', '', NULL),
(25, 220, 9, 'Ассортимент', '', '', '', NULL),
(28, 1, 9, 'root', '', '', '', NULL),
(45, 230, 1, 'Ноутбуки', '', '', '', 'Бюджетные & Игровые'),
(46, 230, 9, 'Ноутбуки', NULL, NULL, NULL, NULL),
(47, 231, 1, 'Бюджетные', '', '', '', ''),
(48, 231, 9, 'Бюджетный', NULL, NULL, NULL, NULL),
(49, 232, 1, 'Игровые', '', '', '', ''),
(50, 232, 9, 'Игровой', NULL, NULL, NULL, NULL),
(51, 233, 1, 'Компьютеры', '', '', '', ''),
(52, 233, 9, 'Компьютеры', NULL, NULL, NULL, NULL),
(53, 234, 1, 'Акустика', '', '', '', ''),
(54, 234, 9, 'Компьютерная акустика', NULL, NULL, NULL, NULL),
(55, 235, 1, 'Мониторы', '', '', '', ''),
(56, 235, 9, 'Мониторы', NULL, NULL, NULL, NULL),
(57, 236, 1, 'Телефоны', '', '', '', ''),
(58, 236, 9, 'Телефоны', NULL, NULL, NULL, NULL),
(59, 237, 1, 'Планшеты', '', '', '', ''),
(60, 237, 9, 'Планшеты', NULL, NULL, NULL, NULL),
(61, 238, 1, 'Компьютерная техника', '', '', '', 'Компьютеры &amp; Ноутбуки'),
(62, 238, 9, 'Компьютерная техника', '', '', '', ''),
(63, 239, 1, 'Мобильные устройства', '', '', '', 'Телефоны &amp; Планшеты'),
(64, 239, 9, 'Мобильные устройства', '', '', '', ''),
(65, 240, 1, 'Периферия ', '', '', '', 'Различная компьютерная периферия'),
(66, 240, 9, 'Переферия', '', '', '', ''),
(67, 241, 1, 'В сборе', '', '', '', ''),
(68, 241, 9, 'В сборе', '', '', '', ''),
(69, 242, 1, 'Комплектующие', '', '', '', ''),
(70, 242, 9, 'Комплектующие', '', '', '', ''),
(71, 243, 1, 'Аксессуары для планшетов', '', '', '', ''),
(72, 243, 9, 'Аксессуары', '', '', '', ''),
(73, 244, 1, 'Аксессуары для телефонов', '', '', '', ''),
(74, 244, 9, 'Аксессуары для телефонов', '', '', '', '');

CREATE TABLE IF NOT EXISTS `StoreCurrency` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `iso` varchar(10) DEFAULT '',
  `symbol` varchar(10) DEFAULT '',
  `rate` float(10,3) DEFAULT NULL,
  `main` tinyint(1) DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `StoreCurrency` (`id`, `name`, `iso`, `symbol`, `rate`, `main`, `default`) VALUES
(1, 'USD', 'dollar', '$', 1.000, 1, 1),
(2, 'RUB', 'rouble', 'руб.', 49.178, 0, 0),
(4, 'GBP', 'pound', '£', 1.550, 0, 0);

CREATE TABLE IF NOT EXISTS `StoreDeliveryMethod` (
`id` int(11) NOT NULL,
  `price` float(10,2) DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT '0.00',
  `position` smallint(6) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO `StoreDeliveryMethod` (`id`, `price`, `free_from`, `position`, `active`) VALUES
(14, 10.00, 1000.00, 0, 1),
(15, 0.00, 0.00, 1, 1),
(16, 30.00, 0.00, 2, 1);

CREATE TABLE IF NOT EXISTS `StoreDeliveryMethodTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `StoreDeliveryMethodTranslate` (`id`, `object_id`, `language_id`, `name`, `description`) VALUES
(1, 14, 1, 'Курьером', ''),
(2, 14, 9, 'English', 'english description'),
(3, 15, 1, 'Самовывоз', ''),
(4, 15, 9, 'Самовывоз', ''),
(5, 16, 1, 'Адресная доставка по стране', ''),
(6, 16, 9, 'Адресная доставка по стране', '');

CREATE TABLE IF NOT EXISTS `StoreDeliveryPayment` (
`id` int(11) NOT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='Saves relations between delivery and payment methods ';

INSERT INTO `StoreDeliveryPayment` (`id`, `delivery_id`, `payment_id`) VALUES
(24, 12, 14),
(23, 10, 16),
(22, 10, 13),
(21, 10, 14),
(34, 11, 16),
(33, 11, 13),
(25, 12, 15),
(26, 12, 16),
(78, 14, 21),
(77, 14, 19),
(76, 14, 20),
(75, 14, 18),
(82, 15, 20),
(81, 15, 18),
(55, 16, 17),
(56, 16, 18),
(57, 16, 20),
(58, 16, 19),
(74, 14, 17);

CREATE TABLE IF NOT EXISTS `StoreManufacturer` (
`id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT '',
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `StoreManufacturer` (`id`, `url`, `layout`, `view`) VALUES
(1, 'lenovo', '', ''),
(2, 'asus', '', ''),
(3, 'dell', '', ''),
(4, 'fujitsu', '', ''),
(5, 'hp', '', ''),
(6, 'apple', '', ''),
(7, 'sony', '', ''),
(8, 'acer', '', ''),
(9, 'logitech', '', ''),
(10, 'microlab', '', ''),
(11, 'edifier', '', ''),
(12, 'sven', '', ''),
(13, 'lg', '', ''),
(14, 'samsung', '', ''),
(15, 'philips', '', ''),
(16, 'htc', '', ''),
(17, 'nokia', '', ''),
(18, 'blackberry', '', '');

CREATE TABLE IF NOT EXISTS `StoreManufacturerTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` varchar(255) DEFAULT '',
  `meta_description` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

INSERT INTO `StoreManufacturerTranslate` (`id`, `object_id`, `language_id`, `name`, `description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(23, 1, 1, 'Lenovo', NULL, NULL, NULL, NULL),
(24, 1, 9, 'Lenovo', NULL, NULL, NULL, NULL),
(25, 2, 1, 'Asus', NULL, NULL, NULL, NULL),
(26, 2, 9, 'Asus', NULL, NULL, NULL, NULL),
(27, 3, 1, 'Dell', NULL, NULL, NULL, NULL),
(28, 3, 9, 'Dell', NULL, NULL, NULL, NULL),
(29, 4, 1, 'Fujitsu', NULL, NULL, NULL, NULL),
(30, 4, 9, 'Fujitsu', NULL, NULL, NULL, NULL),
(31, 5, 1, 'HP', NULL, NULL, NULL, NULL),
(32, 5, 9, 'HP', NULL, NULL, NULL, NULL),
(33, 6, 1, 'Apple', NULL, NULL, NULL, NULL),
(34, 6, 9, 'Apple', NULL, NULL, NULL, NULL),
(35, 7, 1, 'Sony', NULL, NULL, NULL, NULL),
(36, 7, 9, 'Sony', NULL, NULL, NULL, NULL),
(37, 8, 1, 'Acer', NULL, NULL, NULL, NULL),
(38, 8, 9, 'Acer', NULL, NULL, NULL, NULL),
(39, 9, 1, 'Logitech', NULL, NULL, NULL, NULL),
(40, 9, 9, 'Logitech', NULL, NULL, NULL, NULL),
(41, 10, 1, 'Microlab', NULL, NULL, NULL, NULL),
(42, 10, 9, 'Microlab', NULL, NULL, NULL, NULL),
(43, 11, 1, 'Edifier', NULL, NULL, NULL, NULL),
(44, 11, 9, 'Edifier', NULL, NULL, NULL, NULL),
(45, 12, 1, 'Sven', NULL, NULL, NULL, NULL),
(46, 12, 9, 'Sven', NULL, NULL, NULL, NULL),
(47, 13, 1, 'LG', NULL, NULL, NULL, NULL),
(48, 13, 9, 'LG', NULL, NULL, NULL, NULL),
(49, 14, 1, 'Samsung', NULL, NULL, NULL, NULL),
(50, 14, 9, 'Samsung', NULL, NULL, NULL, NULL),
(51, 15, 1, 'Philips', NULL, NULL, NULL, NULL),
(52, 15, 9, 'Philips', NULL, NULL, NULL, NULL),
(53, 16, 1, 'HTC', NULL, NULL, NULL, NULL),
(54, 16, 9, 'HTC', NULL, NULL, NULL, NULL),
(55, 17, 1, 'Nokia', NULL, NULL, NULL, NULL),
(56, 17, 9, 'Nokia', NULL, NULL, NULL, NULL),
(57, 18, 1, 'BlackBerry', NULL, NULL, NULL, NULL),
(58, 18, 9, 'BlackBerry', NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `StorePaymentMethod` (
`id` int(11) NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `payment_system` varchar(100) DEFAULT '',
  `position` smallint(6) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO `StorePaymentMethod` (`id`, `currency_id`, `active`, `payment_system`, `position`) VALUES
(17, 1, 1, 'webmoney', 0),
(18, 2, 1, '', 2),
(19, 1, 1, 'robokassa', 1),
(20, 2, 1, '', 3),
(21, 2, 1, 'qiwi', 4);

CREATE TABLE IF NOT EXISTS `StorePaymentMethodTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO `StorePaymentMethodTranslate` (`id`, `object_id`, `language_id`, `name`, `description`) VALUES
(1, 17, 1, 'WebMoney', 'WebMoney — это универсальное средство для расчетов в Сети, целая среда финансовых взаимоотношений, которой сегодня пользуются миллионы людей по всему миру.'),
(2, 17, 9, 'English payment1', 'russian description'),
(3, 18, 1, 'Наличная', 'Наличная оплата осуществляется только в рублях при доставке товара курьером покупателю.'),
(7, 20, 1, 'Безналичная', ' Стоимость товара при безналичной оплате без ПДВ равна розничной цене товара + 2% '),
(8, 20, 9, 'Безналичная', ''),
(4, 18, 9, 'Наличка', '<p>ыла оылдао ылдао ылдоа ылдва<br />ыаол ывла оывалд ыова</p>'),
(5, 19, 1, 'Robokassa', 'Многими пользователями электронные платежные системы расцениваются в качестве наиболее удобного средства оплаты товаров и услуг в Интернете.'),
(6, 19, 9, 'Robokassa', '<p>Description goes here</p>'),
(9, 21, 1, 'Qiwi', 'Оплата с помощью Qiwi'),
(10, 21, 9, 'Qiwi', 'Оплата с помощью Qiwi');

CREATE TABLE IF NOT EXISTS `StoreProduct` (
`id` int(11) NOT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `use_configurations` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `max_price` float(10,2) NOT NULL DEFAULT '0.00',
  `is_active` tinyint(1) DEFAULT NULL,
  `layout` varchar(255) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `availability` tinyint(2) DEFAULT '1',
  `auto_decrease_quantity` tinyint(2) DEFAULT '1',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `added_to_cart_count` int(11) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `discount` varchar(255) DEFAULT NULL,
  `video` text
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProduct` (`id`, `manufacturer_id`, `type_id`, `use_configurations`, `url`, `price`, `max_price`, `is_active`, `layout`, `view`, `sku`, `quantity`, `availability`, `auto_decrease_quantity`, `views_count`, `created`, `updated`, `added_to_cart_count`, `votes`, `rating`, `discount`, `video`) VALUES
(1, 1, 2, 0, 'lenovo-b570', 345.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:32', '2015-05-19 02:36:32', 0, 0, 0, NULL, 'Intel GMA HD'),
(2, 1, 2, 0, 'lenovo-g570', 360.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 2, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 2, 0, 0, NULL, 'Intel GMA HD'),
(3, 2, 2, 0, 'asus-k53u', 375.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 3, 0, 0, NULL, 'Intel GMA HD'),
(4, 2, 2, 0, 'asus-x54c', 370.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 0, 0, 0, NULL, 'Intel HD Graphics 3000'),
(5, 3, 2, 0, 'dell-inspiron-n5050', 380.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 1, 0, 0, NULL, 'Intel HD Graphics 3000'),
(6, 4, 2, 0, 'fujitsu-lifebook-ah531', 395.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 0, 0, 0, NULL, 'Intel HD Graphics 3000'),
(7, 5, 2, 0, 'hp-elitebook-8560w', 3150.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 0, 0, 0, NULL, 'NVIDIA Quadro 2000M'),
(8, 3, 2, 0, 'dell-alienware-m17x', 2850.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:33', '2015-05-19 02:36:33', 0, 0, 0, NULL, 'AMD Radeon HD 6990M'),
(9, 6, 2, 0, 'apple-macbook-pro-15-late-2011', 2600.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 13, '2015-05-19 02:36:34', '2015-05-19 02:36:33', 5, 0, 0, NULL, 'ATI Radeon HD 6770М'),
(10, 1, 2, 0, 'lenovo-thinkpad-w520', 2450.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 4, 0, 0, NULL, 'NVIDIA Quadro 2000M'),
(11, 7, 2, 0, 'sony-vaio-vpc-f13s8r', 1950.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 3, 0, 0, NULL, 'NVIDIA GeForce GT 425M'),
(12, 8, 2, 0, 'acer-aspire-5943g-7748g75twiss', 2350.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 6, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 11, 1, 5, NULL, 'ATI Mobility Radeon HD 5850'),
(13, 9, 3, 0, 'logitech-x-530', 99.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 0, 0, 0, NULL, NULL),
(14, 10, 3, 0, 'microlab-m-860', 89.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 0, 0, 0, NULL, NULL),
(15, 11, 3, 0, 'edifier-m3700', 115.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 0, 0, 0, NULL, NULL),
(16, 9, 3, 0, 'logitech-z-313', 84.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 3, '2015-05-19 02:36:34', '2015-05-19 02:36:34', 0, 1, 5, NULL, NULL),
(17, 12, 3, 0, 'sven-sps-820', 55.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:34', 0, 0, 0, NULL, NULL),
(18, 11, 3, 0, 'edifier-m1385', 67.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(19, 11, 3, 0, 'edifier-x600', 45.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 12, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, '0', NULL),
(20, 10, 3, 0, 'microlab-fc-362', 85.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(21, 3, 4, 0, 'dell-u2412m', 240.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(22, 3, 4, 0, 'dell-u2312hm', 180.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(23, 13, 4, 0, 'lg-flatron-m2250d', 210.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(24, 13, 4, 0, 'lg-flatron-ips226v', 175.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(25, 14, 4, 0, 'samsung-syncmaster-s22a350n', 150.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(26, 15, 4, 0, 'philips-237e3qphsu', 160.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(27, 15, 4, 0, 'philips-227e3lsu', 190.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:35', '2015-05-19 02:36:35', 0, 0, 0, NULL, NULL),
(28, 5, 4, 0, 'hp-zr2740w', 460.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(29, 5, 4, 0, 'hp-zr2440w', 380.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(30, 14, 5, 0, 'samsung-galaxy-ace-ii', 390.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(31, 16, 5, 0, 'htc-one-xl', 425.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 35, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 2, 1, 3, NULL, NULL),
(32, 16, 5, 0, 'htc-sensation-xl', 480.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 1, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(33, 6, 5, 0, 'apple-iphone-4s-16gb', 675.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 1, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 1, 0, 0, NULL, NULL),
(34, 6, 5, 0, 'apple-iphone-3gs-8gb', 425.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(35, 6, 5, 0, 'apple-iphone-4-16gb', 550.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(36, 17, 5, 0, 'nokia-n9', 425.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 12, '2015-05-19 02:36:36', '2015-05-19 02:36:36', 0, 0, 0, NULL, NULL),
(37, 18, 5, 0, 'blackberry-bold-9900', 445.00, 0.00, 1, '', '', '', 0, 2, 1, 117, '2015-05-19 02:36:37', '2015-06-02 19:05:24', 0, 1, 5, '', NULL),
(38, 18, 5, 0, 'blackberry-bold-9780', 379.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 2, 0, 0, NULL, NULL),
(39, 6, 6, 0, 'apple-ipad-2-wi-fi--3g', 430.00, 0.00, 1, '', '', '', 0, 1, 1, 136, '2015-05-19 02:36:37', '2015-05-27 20:31:52', 20, 0, 0, '', NULL),
(40, 6, 6, 0, 'apple-ipad-2-64gb-wi-fi--3g', 560.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 3, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 0, 0, 0, NULL, NULL),
(41, 14, 6, 0, 'samsung-galaxy-tab-70-plus-p6200-16gb', 350.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 14, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 2, 1, 5, NULL, NULL),
(42, 8, 6, 0, 'acer-iconia-tab-a100-8gb', 365.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 0, 0, 0, NULL, NULL),
(43, 2, 6, 0, 'asus-transformer-pad-prime-201-64gb', 495.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 0, 0, 0, NULL, NULL),
(44, 14, 6, 0, 'samsung-galaxy-tab-101-p7500-16gb', 475.00, 0.00, 1, NULL, NULL, NULL, 0, 1, 1, 0, '2015-05-19 02:36:37', '2015-05-19 02:36:37', 0, 0, 0, NULL, NULL);

CREATE TABLE IF NOT EXISTS `StoreProductAttributeEAV` (
  `entity` int(11) unsigned NOT NULL,
  `attribute` varchar(250) DEFAULT '',
  `value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductAttributeEAV` (`entity`, `attribute`, `value`) VALUES
(1, 'processor_manufacturer', '86'),
(1, 'freq', '87'),
(1, 'memmory', '88'),
(1, 'memmory_type', '89'),
(1, 'screen', '90'),
(1, 'screen_dimension', '91'),
(2, 'processor_manufacturer', '86'),
(2, 'freq', '87'),
(2, 'memmory', '88'),
(2, 'memmory_type', '89'),
(2, 'screen', '90'),
(2, 'screen_dimension', '91'),
(3, 'processor_manufacturer', '92'),
(3, 'freq', '93'),
(3, 'memmory', '88'),
(3, 'memmory_type', '89'),
(3, 'screen', '90'),
(3, 'screen_dimension', '91'),
(4, 'processor_manufacturer', '86'),
(4, 'freq', '94'),
(4, 'memmory', '88'),
(4, 'memmory_type', '89'),
(4, 'screen', '90'),
(4, 'screen_dimension', '91'),
(5, 'processor_manufacturer', '86'),
(5, 'freq', '95'),
(5, 'memmory', '88'),
(5, 'memmory_type', '89'),
(5, 'screen', '90'),
(5, 'screen_dimension', '91'),
(6, 'processor_manufacturer', '86'),
(6, 'freq', '95'),
(6, 'memmory', '88'),
(6, 'memmory_type', '89'),
(6, 'screen', '90'),
(6, 'screen_dimension', '91'),
(7, 'processor_manufacturer', '96'),
(7, 'freq', '97'),
(7, 'memmory', '98'),
(7, 'memmory_type', '89'),
(7, 'screen', '90'),
(7, 'screen_dimension', '99'),
(8, 'processor_manufacturer', '96'),
(8, 'freq', '100'),
(8, 'memmory', '98'),
(8, 'memmory_type', '89'),
(8, 'screen', '90'),
(8, 'screen_dimension', '101'),
(9, 'processor_manufacturer', '96'),
(9, 'freq', '97'),
(9, 'memmory', '102'),
(9, 'memmory_type', '89'),
(9, 'screen', '103'),
(9, 'screen_dimension', '99'),
(10, 'processor_manufacturer', '96'),
(10, 'freq', '100'),
(10, 'memmory', '102'),
(10, 'memmory_type', '89'),
(10, 'screen', '90'),
(10, 'screen_dimension', '99'),
(11, 'processor_manufacturer', '104'),
(11, 'freq', '105'),
(11, 'memmory', '102'),
(11, 'memmory_type', '89'),
(11, 'screen', '106'),
(11, 'screen_dimension', '99'),
(12, 'processor_manufacturer', '96'),
(12, 'freq', '107'),
(12, 'memmory', '98'),
(12, 'memmory_type', '89'),
(12, 'screen', '90'),
(12, 'screen_dimension', '99'),
(13, 'rms_power', '108'),
(13, 'corpus_material', '109'),
(13, 'sound_type', '110'),
(14, 'rms_power', '111'),
(14, 'corpus_material', '109'),
(14, 'sound_type', '110'),
(15, 'rms_power', '112'),
(15, 'corpus_material', '113'),
(15, 'sound_type', '110'),
(16, 'rms_power', '114'),
(16, 'corpus_material', '109'),
(16, 'sound_type', '115'),
(17, 'rms_power', '116'),
(17, 'corpus_material', '113'),
(17, 'sound_type', '115'),
(18, 'rms_power', '117'),
(18, 'corpus_material', '113'),
(18, 'sound_type', '115'),
(19, 'rms_power', '116'),
(19, 'corpus_material', '113'),
(19, 'sound_type', '115'),
(20, 'rms_power', '111'),
(20, 'corpus_material', '113'),
(20, 'sound_type', '115'),
(21, 'monitor_diagonal', '118'),
(21, 'monitor_dimension', '119'),
(21, 'view_angle', '120'),
(22, 'monitor_diagonal', '121'),
(22, 'monitor_dimension', '119'),
(22, 'view_angle', '120'),
(23, 'monitor_diagonal', '122'),
(23, 'monitor_dimension', '119'),
(23, 'view_angle', '123'),
(24, 'monitor_diagonal', '122'),
(24, 'monitor_dimension', '119'),
(24, 'view_angle', '120'),
(25, 'monitor_diagonal', '122'),
(25, 'monitor_dimension', '119'),
(25, 'view_angle', '123'),
(26, 'monitor_diagonal', '121'),
(26, 'monitor_dimension', '119'),
(26, 'view_angle', '120'),
(27, 'monitor_diagonal', '122'),
(27, 'monitor_dimension', '119'),
(27, 'view_angle', '120'),
(28, 'monitor_diagonal', '124'),
(28, 'monitor_dimension', '125'),
(28, 'view_angle', '120'),
(29, 'monitor_diagonal', '118'),
(29, 'monitor_dimension', '119'),
(29, 'view_angle', '120'),
(30, 'phone_platform', '126'),
(30, 'phone_weight', '127'),
(30, 'phone_display', '128'),
(30, 'phone_camera', '129'),
(31, 'phone_platform', '126'),
(31, 'phone_weight', '130'),
(31, 'phone_display', '131'),
(31, 'phone_camera', '132'),
(32, 'phone_platform', '126'),
(32, 'phone_weight', '133'),
(32, 'phone_display', '131'),
(32, 'phone_camera', '132'),
(33, 'phone_platform', '134'),
(33, 'phone_weight', '135'),
(33, 'phone_display', '136'),
(33, 'phone_camera', '132'),
(34, 'phone_platform', '134'),
(34, 'phone_weight', '137'),
(34, 'phone_display', '136'),
(34, 'phone_camera', '138'),
(35, 'phone_platform', '134'),
(35, 'phone_weight', '139'),
(35, 'phone_display', '136'),
(35, 'phone_camera', '129'),
(36, 'phone_platform', '140'),
(36, 'phone_weight', '137'),
(36, 'phone_display', '141'),
(36, 'phone_camera', '132'),
(37, 'phone_camera', '129'),
(37, 'phone_display', '144'),
(37, 'phone_weight', '143'),
(38, 'phone_platform', '142'),
(38, 'phone_weight', '145'),
(38, 'phone_display', '146'),
(38, 'phone_camera', '129'),
(39, 'weight', '150'),
(39, 'tablet_screen_size', '147'),
(40, 'tablet_screen_size', '147'),
(40, 'screen_dimension', '148'),
(40, 'memmory_size', '151'),
(40, 'weight', '150'),
(41, 'tablet_screen_size', '152'),
(41, 'screen_dimension', '153'),
(41, 'memmory_size', '149'),
(41, 'weight', '154'),
(42, 'tablet_screen_size', '152'),
(42, 'screen_dimension', '153'),
(42, 'memmory_size', '155'),
(42, 'weight', '156'),
(43, 'tablet_screen_size', '157'),
(43, 'screen_dimension', '158'),
(43, 'memmory_size', '151'),
(43, 'weight', '159'),
(44, 'tablet_screen_size', '157'),
(44, 'screen_dimension', '158'),
(44, 'memmory_size', '149'),
(44, 'weight', '160'),
(37, 'phone_platform', '142');

CREATE TABLE IF NOT EXISTS `StoreProductCategoryRef` (
`id` int(11) NOT NULL,
  `product` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `is_main` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductCategoryRef` (`id`, `product`, `category`, `is_main`) VALUES
(162, 1, 231, 1),
(163, 1, 230, 0),
(164, 2, 231, 1),
(165, 2, 230, 0),
(166, 3, 231, 1),
(167, 3, 230, 0),
(168, 4, 231, 1),
(169, 4, 230, 0),
(170, 5, 231, 1),
(171, 5, 230, 0),
(172, 6, 231, 1),
(173, 6, 230, 0),
(174, 7, 232, 1),
(175, 7, 230, 0),
(176, 8, 232, 1),
(177, 8, 230, 0),
(178, 9, 232, 1),
(179, 9, 230, 0),
(180, 10, 232, 1),
(181, 10, 230, 0),
(182, 11, 232, 1),
(183, 11, 230, 0),
(184, 12, 232, 1),
(185, 12, 230, 0),
(186, 13, 234, 1),
(283, 14, 1, 0),
(188, 14, 234, 1),
(285, 15, 1, 0),
(190, 15, 234, 1),
(287, 16, 1, 0),
(192, 16, 234, 1),
(289, 17, 1, 0),
(194, 17, 234, 1),
(291, 18, 1, 0),
(196, 18, 234, 1),
(293, 19, 1, 0),
(198, 19, 234, 1),
(295, 20, 1, 0),
(200, 20, 234, 1),
(297, 7, 1, 0),
(202, 21, 235, 1),
(203, 22, 235, 1),
(204, 23, 235, 1),
(205, 24, 235, 1),
(206, 25, 235, 1),
(207, 26, 235, 1),
(208, 27, 235, 1),
(209, 28, 235, 1),
(210, 29, 235, 1),
(211, 30, 236, 1),
(212, 31, 236, 1),
(213, 32, 236, 1),
(214, 33, 236, 1),
(215, 34, 236, 1),
(216, 35, 236, 1),
(217, 36, 236, 1),
(218, 37, 236, 1),
(219, 38, 236, 1),
(220, 39, 237, 1),
(238, 40, 239, 0),
(222, 40, 237, 1),
(239, 41, 1, 0),
(224, 41, 237, 1),
(241, 42, 1, 0),
(226, 42, 237, 1),
(243, 43, 1, 0),
(228, 43, 237, 1),
(245, 44, 1, 0),
(230, 44, 237, 1),
(247, 30, 1, 0),
(237, 40, 1, 0),
(233, 37, 1, 0),
(234, 37, 239, 0),
(235, 39, 1, 0),
(236, 39, 239, 0),
(240, 41, 239, 0),
(242, 42, 239, 0),
(244, 43, 239, 0),
(246, 44, 239, 0),
(248, 30, 239, 0),
(249, 31, 1, 0),
(250, 31, 239, 0),
(251, 32, 1, 0),
(252, 32, 239, 0),
(253, 33, 1, 0),
(254, 33, 239, 0),
(255, 34, 1, 0),
(256, 34, 239, 0),
(257, 35, 1, 0),
(258, 35, 239, 0),
(259, 36, 1, 0),
(260, 36, 239, 0),
(261, 38, 1, 0),
(262, 38, 239, 0),
(263, 21, 1, 0),
(264, 21, 240, 0),
(265, 22, 1, 0),
(266, 22, 240, 0),
(267, 23, 1, 0),
(268, 23, 240, 0),
(269, 24, 1, 0),
(270, 24, 240, 0),
(271, 25, 1, 0),
(272, 25, 240, 0),
(273, 26, 1, 0),
(274, 26, 240, 0),
(275, 27, 1, 0),
(276, 27, 240, 0),
(277, 28, 1, 0),
(278, 28, 240, 0),
(279, 29, 1, 0),
(280, 29, 240, 0),
(281, 13, 1, 0),
(282, 13, 240, 0),
(284, 14, 240, 0),
(286, 15, 240, 0),
(288, 16, 240, 0),
(290, 17, 240, 0),
(292, 18, 240, 0),
(294, 19, 240, 0),
(296, 20, 240, 0),
(298, 7, 238, 0),
(299, 8, 1, 0),
(300, 8, 238, 0),
(301, 9, 1, 0),
(302, 9, 238, 0),
(303, 10, 1, 0),
(304, 10, 238, 0),
(305, 11, 1, 0),
(306, 11, 238, 0),
(307, 12, 1, 0),
(308, 12, 238, 0),
(309, 1, 1, 0),
(310, 1, 238, 0),
(311, 2, 1, 0),
(312, 2, 238, 0),
(313, 3, 1, 0),
(314, 3, 238, 0),
(315, 4, 1, 0),
(316, 4, 238, 0),
(317, 5, 1, 0),
(318, 5, 238, 0),
(319, 6, 1, 0),
(320, 6, 238, 0);

CREATE TABLE IF NOT EXISTS `StoreProductConfigurableAttributes` (
  `product_id` int(11) NOT NULL COMMENT 'Attributes available to configure product',
  `attribute_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `StoreProductConfigurations` (
  `product_id` int(11) NOT NULL COMMENT 'Saves relations beetwen product and configurations',
  `configurable_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `StoreProductImage` (
`id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `is_main` tinyint(1) DEFAULT '0',
  `uploaded_by` int(11) DEFAULT NULL,
  `date_uploaded` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductImage` (`id`, `product_id`, `name`, `is_main`, `uploaded_by`, `date_uploaded`, `title`) VALUES
(140, 1, '1_-1599364830.jpg', 1, NULL, '2015-05-19 02:36:32', NULL),
(141, 2, '2_491031549.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(142, 3, '3_-1704293594.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(143, 4, '4_1566631388.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(144, 5, '5_1956307479.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(145, 6, '6_243190146.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(146, 7, '7_1558627083.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(147, 8, '8_895913424.jpg', 1, NULL, '2015-05-19 02:36:33', NULL),
(148, 9, '9_647330701.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(149, 10, '10_648192338.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(150, 11, '11_1838252176.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(151, 12, '12_710276274.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(152, 13, '13_-322286165.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(153, 14, '14_-926930243.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(154, 15, '15_550458964.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(155, 16, '16_1048722848.jpg', 1, NULL, '2015-05-19 02:36:34', NULL),
(156, 17, '17_-1006495850.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(157, 18, '18_1639066443.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(158, 19, '19_900318220.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(159, 20, '20_814461983.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(160, 21, '21_-1433524052.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(161, 22, '22_-135696105.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(162, 23, '23_728198372.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(163, 24, '24_-236665647.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(164, 25, '25_2097015627.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(165, 26, '26_-1147865519.jpg', 1, NULL, '2015-05-19 02:36:35', NULL),
(166, 27, '27_930808167.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(167, 28, '28_1358316813.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(168, 29, '29_-875208690.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(169, 30, '30_2083058471.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(170, 31, '31_-1755442950.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(171, 32, '32_1717193479.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(172, 33, '33_1511975719.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(173, 34, '34_-1222566579.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(174, 35, '35_1594267275.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(175, 36, '36_175366695.jpg', 1, NULL, '2015-05-19 02:36:36', NULL),
(176, 37, '37_-754305780.jpg', 1, NULL, '2015-05-19 02:36:37', ''),
(177, 38, '38_-199901566.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(178, 39, '39_-1661391222.jpg', 1, NULL, '2015-05-19 02:36:37', ''),
(179, 40, '40_1527867224.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(180, 41, '41_-2067520978.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(181, 42, '42_-1217614896.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(182, 43, '43_-70724283.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(183, 44, '44_100505213.jpg', 1, NULL, '2015-05-19 02:36:37', NULL),
(184, 37, '37_-1936324188.jpg', 0, 1, '2015-05-27 14:42:56', '');

CREATE TABLE IF NOT EXISTS `StoreProductTranslate` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `short_description` text,
  `full_description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductTranslate` (`id`, `object_id`, `language_id`, `name`, `short_description`, `full_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(265, 1, 1, 'Lenovo B570', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, Intel GMA HD, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.35 кг', NULL, NULL, NULL, NULL),
(266, 1, 9, 'Lenovo B570', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, Intel GMA HD, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.35 кг', NULL, NULL, NULL, NULL),
(267, 2, 1, 'Lenovo G570', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, ATI Radeon HD 6370M, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.43 кг', NULL, NULL, NULL, NULL),
(268, 2, 9, 'Lenovo G570', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, ATI Radeon HD 6370M, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.43 кг', NULL, NULL, NULL, NULL),
(269, 3, 1, 'ASUS K53U', 'C-60 / E-240 / E-450, 1000-1650 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.6 кг', NULL, NULL, NULL, NULL),
(270, 3, 9, 'ASUS K53U', 'C-60 / E-240 / E-450, 1000-1650 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.6 кг', NULL, NULL, NULL, NULL),
(271, 4, 1, 'ASUS X54C', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth, 2.6 кг', NULL, NULL, NULL, NULL),
(272, 4, 9, 'ASUS X54C', 'Celeron / Pentium, 1500-2200 МГц, 2048-4096 Мб, 320-500 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth, 2.6 кг', NULL, NULL, NULL, NULL),
(273, 5, 1, 'DELL INSPIRON N5050', 'Celeron, 1500-1700 МГц, 2048 Мб, 320-500 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.37 кг', NULL, NULL, NULL, NULL),
(274, 5, 9, 'DELL INSPIRON N5050', 'Celeron, 1500-1700 МГц, 2048 Мб, 320-500 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth (опционально), 2.37 кг', NULL, NULL, NULL, NULL),
(275, 6, 1, 'Fujitsu LIFEBOOK AH531', 'Celeron / Pentium, 1500-2200 МГц, 2048 Мб, 320 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth, 2.5 кг', NULL, NULL, NULL, NULL),
(276, 6, 9, 'Fujitsu LIFEBOOK AH531', 'Celeron / Pentium, 1500-2200 МГц, 2048 Мб, 320 Гб, 15.6 дюйм, Intel HD Graphics 3000, DVD-RW, Wi-Fi, Bluetooth, 2.5 кг', NULL, NULL, NULL, NULL),
(277, 7, 1, 'HP EliteBook 8560w', 'Core i7, 2500 МГц, 8192 Мб, 750 Гб, 15.6 дюйм, NVIDIA Quadro 2000M, BD-RE, Wi-Fi, Bluetooth, 3 кг', NULL, NULL, NULL, NULL),
(278, 7, 9, 'HP EliteBook 8560w', 'Core i7, 2500 МГц, 8192 Мб, 750 Гб, 15.6 дюйм, NVIDIA Quadro 2000M, BD-RE, Wi-Fi, Bluetooth, 3 кг', NULL, NULL, NULL, NULL),
(279, 8, 1, 'DELL ALIENWARE M17x', 'Core i7, 2200-2400 МГц, 8192-16384 Мб, 750-1500 Гб, 17 дюйм, AMD Radeon HD 6990M, BD-RE / Blu-Ray / DVD-RW, Wi-Fi, Bluetooth, 5.3 кг', NULL, NULL, NULL, NULL),
(280, 8, 9, 'DELL ALIENWARE M17x', 'Core i7, 2200-2400 МГц, 8192-16384 Мб, 750-1500 Гб, 17 дюйм, AMD Radeon HD 6990M, BD-RE / Blu-Ray / DVD-RW, Wi-Fi, Bluetooth, 5.3 кг', NULL, NULL, NULL, NULL),
(281, 9, 1, 'Apple MacBook Pro 15 Late 2011', 'Core i7, 2200-2400 МГц, 4096 Мб, 500-750 Гб, 15.4 дюйм, ATI Radeon HD 6750M / ATI Radeon HD 6770М, DVD-RW, Wi-Fi, Bluetooth, 2.54 кг', NULL, NULL, NULL, NULL),
(282, 9, 9, 'Apple MacBook Pro 15 Late 2011', 'Core i7, 2200-2400 МГц, 4096 Мб, 500-750 Гб, 15.4 дюйм, ATI Radeon HD 6750M / ATI Radeon HD 6770М, DVD-RW, Wi-Fi, Bluetooth, 2.54 кг', NULL, NULL, NULL, NULL),
(283, 10, 1, 'Lenovo THINKPAD W520', 'Core i7 / Core i7 Extreme, 2000-2700 МГц, 4096-8192 Мб, 160-580 Гб, 15.6 дюйм, NVIDIA Quadro 2000M, DVD-RW, Wi-Fi, Bluetooth, 2.45 кг', NULL, NULL, NULL, NULL),
(284, 10, 9, 'Lenovo THINKPAD W520', 'Core i7 / Core i7 Extreme, 2000-2700 МГц, 4096-8192 Мб, 160-580 Гб, 15.6 дюйм, NVIDIA Quadro 2000M, DVD-RW, Wi-Fi, Bluetooth, 2.45 кг', NULL, NULL, NULL, NULL),
(285, 11, 1, 'Sony VAIO VPC-F13S8R', 'Core i5, 2660 МГц, 4096 Мб, 640 Гб, 16.4 дюйм, NVIDIA GeForce GT 425M, Blu-Ray, Wi-Fi, Bluetooth, 3.1 кг', NULL, NULL, NULL, NULL),
(286, 11, 9, 'Sony VAIO VPC-F13S8R', 'Core i5, 2660 МГц, 4096 Мб, 640 Гб, 16.4 дюйм, NVIDIA GeForce GT 425M, Blu-Ray, Wi-Fi, Bluetooth, 3.1 кг', NULL, NULL, NULL, NULL),
(287, 12, 1, 'Acer ASPIRE 5943G-7748G75TWiss', 'Core i7, 1730 МГц, 8192 Мб, 750 Гб, 15.6 дюйм, ATI Mobility Radeon HD 5850, BD-RE, Wi-Fi, Bluetooth, 3.3 кг', NULL, NULL, NULL, NULL),
(288, 12, 9, 'Acer ASPIRE 5943G-7748G75TWiss', 'Core i7, 1730 МГц, 8192 Мб, 750 Гб, 15.6 дюйм, ATI Mobility Radeon HD 5850, BD-RE, Wi-Fi, Bluetooth, 3.3 кг', NULL, NULL, NULL, NULL),
(289, 13, 1, 'Logitech X-530', 'число каналов: 5.1, мощность 71.20 Вт, 40-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF, разъем для наушников, магнитное экранирование', NULL, NULL, NULL, NULL),
(290, 13, 9, 'Logitech X-530', 'число каналов: 5.1, мощность 71.20 Вт, 40-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF, разъем для наушников, магнитное экранирование', NULL, NULL, NULL, NULL),
(291, 14, 1, 'Microlab M-860', 'число каналов: 5.1, мощность 62 Вт, 30-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF, магнитное экранирование, пульт ДУ', NULL, NULL, NULL, NULL),
(292, 14, 9, 'Microlab M-860', 'число каналов: 5.1, мощность 62 Вт, 30-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF, магнитное экранирование, пульт ДУ', NULL, NULL, NULL, NULL),
(293, 15, 1, 'Edifier M3700', 'число каналов: 5.1, мощность 80 Вт, 45-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование, пульт ДУ', NULL, NULL, NULL, NULL),
(294, 15, 9, 'Edifier M3700', 'число каналов: 5.1, мощность 80 Вт, 45-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование, пульт ДУ', NULL, NULL, NULL, NULL),
(295, 16, 1, 'Logitech Z-313', 'число каналов: 2.1, мощность 25 Вт, 48-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF', NULL, NULL, NULL, NULL),
(296, 16, 9, 'Logitech Z-313', 'число каналов: 2.1, мощность 25 Вт, 48-20000 Гц, материал колонок: пластик, материал сабвуфера: MDF', NULL, NULL, NULL, NULL),
(297, 17, 1, 'Sven SPS-820', 'число каналов: 2.1, мощность 38 Вт, 50-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование', NULL, NULL, NULL, NULL),
(298, 17, 9, 'Sven SPS-820', 'число каналов: 2.1, мощность 38 Вт, 50-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование', NULL, NULL, NULL, NULL),
(299, 18, 1, 'Edifier M1385', 'число каналов: 2.1, мощность 28 Вт, материал сабвуфера: MDF, разъем для наушников, магнитное экранирование, радио', NULL, NULL, NULL, NULL),
(300, 18, 9, 'Edifier M1385', 'число каналов: 2.1, мощность 28 Вт, материал сабвуфера: MDF, разъем для наушников, магнитное экранирование, радио', NULL, NULL, NULL, NULL),
(301, 19, 1, 'Edifier X600', 'число каналов: 2.1, мощность 30 Вт, 48-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование', NULL, NULL, NULL, NULL),
(302, 19, 9, 'Edifier X600', 'число каналов: 2.1, мощность 30 Вт, 48-20000 Гц, материал колонок: MDF, материал сабвуфера: MDF, магнитное экранирование', NULL, NULL, NULL, NULL),
(303, 20, 1, 'Microlab FC 362', 'число каналов: 2.1, мощность 54 Вт, 35-20000 Гц, материал сабвуфера: MDF, разъем для наушников', NULL, NULL, NULL, NULL),
(304, 20, 9, 'Microlab FC 362', 'число каналов: 2.1, мощность 54 Вт, 35-20000 Гц, материал сабвуфера: MDF, разъем для наушников', NULL, NULL, NULL, NULL),
(305, 21, 1, 'DELL U2412M', 'ЖК (TFT E-IPS) 24", широкоформатный, 1920x1200, LED-подсветка, 300 кд/м2, 1000:1, 8 мс, 178°/178°, USB-концентратор, DVI, DisplayPort, VGA', NULL, NULL, NULL, NULL),
(306, 21, 9, 'DELL U2412M', 'ЖК (TFT E-IPS) 24", широкоформатный, 1920x1200, LED-подсветка, 300 кд/м2, 1000:1, 8 мс, 178°/178°, USB-концентратор, DVI, DisplayPort, VGA', NULL, NULL, NULL, NULL),
(307, 22, 1, 'DELL U2312HM', 'ЖК (TFT IPS) 23", широкоформатный, 1920x1080, LED-подсветка, 300 кд/м2, 1000:1, 8 мс, 178°/178°, USB-концентратор, DVI, DisplayPort, VGA', NULL, NULL, NULL, NULL),
(308, 22, 9, 'DELL U2312HM', 'ЖК (TFT IPS) 23", широкоформатный, 1920x1080, LED-подсветка, 300 кд/м2, 1000:1, 8 мс, 178°/178°, USB-концентратор, DVI, DisplayPort, VGA', NULL, NULL, NULL, NULL),
(309, 23, 1, 'LG Flatron M2250D', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 5 мс, 170°/160°, стереоколонки, ТВ-тюнер, HDMI, VGA, композитный вход, компонентный вход, SCART', NULL, NULL, NULL, NULL),
(310, 23, 9, 'LG Flatron M2250D', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 5 мс, 170°/160°, стереоколонки, ТВ-тюнер, HDMI, VGA, композитный вход, компонентный вход, SCART', NULL, NULL, NULL, NULL),
(311, 24, 1, 'LG Flatron IPS226V', 'ЖК (TFT E-IPS) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 178°/178°, DVI, HDMI, VGA', NULL, NULL, NULL, NULL),
(312, 24, 9, 'LG Flatron IPS226V', 'ЖК (TFT E-IPS) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 178°/178°, DVI, HDMI, VGA', NULL, NULL, NULL, NULL),
(313, 25, 1, 'Samsung SyncMaster S22A350N', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 170°/160°, VGA', NULL, NULL, NULL, NULL),
(314, 25, 9, 'Samsung SyncMaster S22A350N', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 170°/160°, VGA', NULL, NULL, NULL, NULL),
(315, 26, 1, 'Philips 237E3QPHSU', 'ЖК (TFT IPS) 23", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 178°/178°, HDMI x2, VGA', NULL, NULL, NULL, NULL),
(316, 26, 9, 'Philips 237E3QPHSU', 'ЖК (TFT IPS) 23", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, 178°/178°, HDMI x2, VGA', NULL, NULL, NULL, NULL),
(317, 27, 1, 'Philips 227E3LSU', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, DVI, VGA', NULL, NULL, NULL, NULL),
(318, 27, 9, 'Philips 227E3LSU', 'ЖК (TFT TN) 21.5", широкоформатный, 1920x1080, LED-подсветка, 250 кд/м2, 1000:1, 5 мс, DVI, VGA', NULL, NULL, NULL, NULL),
(319, 28, 1, 'HP ZR2740w', 'ЖК (TFT IPS) 27", широкоформатный, 2560x1440, LED-подсветка, 380 кд/м2, 1000:1, 12 мс, 178°/178°, USB-концентратор, DVI, DisplayPort', NULL, NULL, NULL, NULL),
(320, 28, 9, 'HP ZR2740w', 'ЖК (TFT IPS) 27", широкоформатный, 2560x1440, LED-подсветка, 380 кд/м2, 1000:1, 12 мс, 178°/178°, USB-концентратор, DVI, DisplayPort', NULL, NULL, NULL, NULL),
(321, 29, 1, 'HP ZR2440w', 'ЖК (TFT IPS) 24", широкоформатный, 1920x1200, LED-подсветка, 350 кд/м2, 1000:1, 6 мс, 178°/178°, USB-концентратор, DVI, HDMI, DisplayPort', NULL, NULL, NULL, NULL),
(322, 29, 9, 'HP ZR2440w', 'ЖК (TFT IPS) 24", широкоформатный, 1920x1200, LED-подсветка, 350 кд/м2, 1000:1, 6 мс, 178°/178°, USB-концентратор, DVI, HDMI, DisplayPort', NULL, NULL, NULL, NULL),
(323, 30, 1, 'Samsung Galaxy Ace II', 'GSM, 3G, смартфон, Android 2.3, вес 118 г, ШхВхТ: 62.3x118.3x10.5 мм, экран 3.8", 480x800, FM-радио, Bluetooth, Wi-Fi, GPS, ГЛОНАСС, фотокамера 5 МП, память 4 Гб, слот microSD (TransFlash), аккумулятор 1500 мАч', NULL, NULL, NULL, NULL),
(324, 30, 9, 'Samsung Galaxy Ace II', 'GSM, 3G, смартфон, Android 2.3, вес 118 г, ШхВхТ: 62.3x118.3x10.5 мм, экран 3.8", 480x800, FM-радио, Bluetooth, Wi-Fi, GPS, ГЛОНАСС, фотокамера 5 МП, память 4 Гб, слот microSD (TransFlash), аккумулятор 1500 мАч', NULL, NULL, NULL, NULL),
(325, 31, 1, 'HTC One XL', 'GSM, 3G, смартфон, Android 4.0, вес 129 г, ШхВхТ: 69.9x134.8x8.9 мм, экран 4.7", 720x1280, FM-радио, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, память 32 Гб, аккумулятор 1800 мАч', NULL, NULL, NULL, NULL),
(326, 31, 9, 'HTC One XL', 'GSM, 3G, смартфон, Android 4.0, вес 129 г, ШхВхТ: 69.9x134.8x8.9 мм, экран 4.7", 720x1280, FM-радио, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, память 32 Гб, аккумулятор 1800 мАч', NULL, NULL, NULL, NULL),
(327, 32, 1, 'HTC Sensation XL', 'GSM, 3G, смартфон, Android 2.3, вес 162 г, ШхВхТ: 70.7x132.5x9.9 мм, экран 4.7", 480x800, FM-радио, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, память 16 Гб, аккумулятор 1600 мАч', NULL, NULL, NULL, NULL),
(328, 32, 9, 'HTC Sensation XL', 'GSM, 3G, смартфон, Android 2.3, вес 162 г, ШхВхТ: 70.7x132.5x9.9 мм, экран 4.7", 480x800, FM-радио, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, память 16 Гб, аккумулятор 1600 мАч', NULL, NULL, NULL, NULL),
(329, 33, 1, 'Apple iPhone 4S 16Gb', 'GSM, CDMA800, CDMA1900, 3G, смартфон, iOS 5, вес 140 г, ШхВхТ: 58.6x115.2x9.3 мм, экран 3.5", 640x960, Bluetooth, Wi-Fi, GPS, ГЛОНАСС, фотокамера 8 МП, память 16 Гб', NULL, NULL, NULL, NULL),
(330, 33, 9, 'Apple iPhone 4S 16Gb', 'GSM, CDMA800, CDMA1900, 3G, смартфон, iOS 5, вес 140 г, ШхВхТ: 58.6x115.2x9.3 мм, экран 3.5", 640x960, Bluetooth, Wi-Fi, GPS, ГЛОНАСС, фотокамера 8 МП, память 16 Гб', NULL, NULL, NULL, NULL),
(331, 34, 1, 'Apple iPhone 3GS 8Gb', 'GSM, 3G, смартфон, iOS, вес 135 г, ШхВхТ: 62x116x12 мм, экран 3.5", 320x480, Bluetooth, Wi-Fi, GPS, фотокамера 3 МП, память 8 Гб', NULL, NULL, NULL, NULL),
(332, 34, 9, 'Apple iPhone 3GS 8Gb', 'GSM, 3G, смартфон, iOS, вес 135 г, ШхВхТ: 62x116x12 мм, экран 3.5", 320x480, Bluetooth, Wi-Fi, GPS, фотокамера 3 МП, память 8 Гб', NULL, NULL, NULL, NULL),
(333, 35, 1, 'Apple iPhone 4 16Gb', 'GSM, 3G, смартфон, iOS 4, вес 137 г, ШхВхТ: 59x115x9 мм, экран 3.5", 640x960, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 16 Гб', NULL, NULL, NULL, NULL),
(334, 35, 9, 'Apple iPhone 4 16Gb', 'GSM, 3G, смартфон, iOS 4, вес 137 г, ШхВхТ: 59x115x9 мм, экран 3.5", 640x960, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 16 Гб', NULL, NULL, NULL, NULL),
(335, 36, 1, 'Nokia N9', 'GSM, 3G, смартфон, MeeGo 1.2, вес 135 г, ШхВхТ: 61x116x12 мм, экран 3.9", 480x854, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, аккумулятор 1450 мАч', NULL, NULL, NULL, NULL),
(336, 36, 9, 'Nokia N9', 'GSM, 3G, смартфон, MeeGo 1.2, вес 135 г, ШхВхТ: 61x116x12 мм, экран 3.9", 480x854, Bluetooth, Wi-Fi, GPS, фотокамера 8 МП, аккумулятор 1450 мАч', NULL, NULL, NULL, NULL),
(337, 37, 1, 'BlackBerry Bold 9900', 'GSM, 3G, смартфон, BlackBerry OS, вес 130 г, ШхВхТ: 66x115x11 мм, экран 2.8", 640x480, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 8 Гб, слот microSD (TransFlash), аккумулятор 1230 мАч', '', '', '', ''),
(338, 37, 9, 'BlackBerry Bold 9900', 'GSM, 3G, смартфон, BlackBerry OS, вес 130 г, ШхВхТ: 66x115x11 мм, экран 2.8", 640x480, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 8 Гб, слот microSD (TransFlash), аккумулятор 1230 мАч', NULL, NULL, NULL, NULL),
(339, 38, 1, 'BlackBerry Bold 9780', 'GSM, 3G, смартфон, BlackBerry OS, вес 122 г, ШхВхТ: 60x109x14 мм, экран 2.4", 480x360, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 256 Мб, слот microSD (TransFlash), аккумулятор 1500 мАч', NULL, NULL, NULL, NULL),
(340, 38, 9, 'BlackBerry Bold 9780', 'GSM, 3G, смартфон, BlackBerry OS, вес 122 г, ШхВхТ: 60x109x14 мм, экран 2.4", 480x360, Bluetooth, Wi-Fi, GPS, фотокамера 5 МП, память 256 Мб, слот microSD (TransFlash), аккумулятор 1500 мАч', NULL, NULL, NULL, NULL),
(341, 39, 1, 'Apple iPad 2 Wi-Fi + 3G', 'экран 9.7", 1024x768, емкостный, мультитач, iOS, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, вес 613 г', 'Один из легендарных планшетов Apple', '', '', ''),
(342, 39, 9, 'Apple iPad 2 16Gb Wi-Fi + 3G', 'экран 9.7", 1024x768, емкостный, мультитач, iOS, встроенная память 16 Гб, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, вес 613 г', NULL, NULL, NULL, NULL),
(343, 40, 1, 'Apple iPad 2 64Gb Wi-Fi + 3G', 'экран 9.7", 1024x768, емкостный, мультитач, iOS, встроенная память 64 Гб, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, вес 613 г', NULL, NULL, NULL, NULL),
(344, 40, 9, 'Apple iPad 2 64Gb Wi-Fi + 3G', 'экран 9.7", 1024x768, емкостный, мультитач, iOS, встроенная память 64 Гб, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, вес 613 г', NULL, NULL, NULL, NULL),
(345, 41, 1, 'Samsung Galaxy Tab 7.0 Plus P6200 16GB', 'экран 7", 1024x600, емкостный, мультитач, Android 3.2, встроенная память 16 Гб, microSDHC, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, подключение к компьютеру по USB, вес 343 г', NULL, NULL, NULL, NULL),
(346, 41, 9, 'Samsung Galaxy Tab 7.0 Plus P6200 16GB', 'экран 7", 1024x600, емкостный, мультитач, Android 3.2, встроенная память 16 Гб, microSDHC, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, подключение к компьютеру по USB, вес 343 г', NULL, NULL, NULL, NULL),
(347, 42, 1, 'Acer Iconia Tab A100 8Gb', 'экран 7", 1024x600, емкостный, мультитач, Android 3.2, встроенная память 8 Гб, microSDHC, Wi-Fi, Bluetooth, GPS, гироскоп, две фотокамеры, HDMI, подключение к компьютеру по USB, вес 410 г', NULL, NULL, NULL, NULL),
(348, 42, 9, 'Acer Iconia Tab A100 8Gb', 'экран 7", 1024x600, емкостный, мультитач, Android 3.2, встроенная память 8 Гб, microSDHC, Wi-Fi, Bluetooth, GPS, гироскоп, две фотокамеры, HDMI, подключение к компьютеру по USB, вес 410 г', NULL, NULL, NULL, NULL),
(349, 43, 1, 'Asus Transformer Pad Prime 201 64Gb', 'экран 10.1", 1280x800, емкостный, мультитач, Android 4.0, встроенная память 64 Гб, microSD, Wi-Fi, Bluetooth, гироскоп, две фотокамеры, micro HDMI, подключение к компьютеру по USB, вес 586 г', NULL, NULL, NULL, NULL),
(350, 43, 9, 'Asus Transformer Pad Prime 201 64Gb', 'экран 10.1", 1280x800, емкостный, мультитач, Android 4.0, встроенная память 64 Гб, microSD, Wi-Fi, Bluetooth, гироскоп, две фотокамеры, micro HDMI, подключение к компьютеру по USB, вес 586 г', NULL, NULL, NULL, NULL),
(351, 44, 1, 'Samsung Galaxy Tab 10.1 P7500 16Gb', 'экран 10.1", 1280x800, емкостный, мультитач, Android 3.1, встроенная память 16 Гб, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, подключение к компьютеру по USB, вес 565 г', NULL, NULL, NULL, NULL),
(352, 44, 9, 'Samsung Galaxy Tab 10.1 P7500 16Gb', 'экран 10.1", 1280x800, емкостный, мультитач, Android 3.1, встроенная память 16 Гб, Wi-Fi, Bluetooth, 3G, GPS, гироскоп, две фотокамеры, подключение к компьютеру по USB, вес 565 г', NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `StoreProductType` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `categories_preset` text,
  `main_category` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductType` (`id`, `name`, `categories_preset`, `main_category`) VALUES
(1, 'Простой продукт', NULL, 0),
(2, 'Ноутбук', NULL, 0),
(3, 'Акустика', NULL, 0),
(4, 'Монитор', NULL, 0),
(5, 'Телефон', NULL, 0),
(6, 'Планшет', NULL, 0);

CREATE TABLE IF NOT EXISTS `StoreProductVariant` (
`id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `price` float(10,2) DEFAULT '0.00',
  `price_type` tinyint(1) DEFAULT NULL,
  `sku` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=300 DEFAULT CHARSET=utf8;

INSERT INTO `StoreProductVariant` (`id`, `attribute_id`, `option_id`, `product_id`, `price`, `price_type`, `sku`) VALUES
(298, 18, 149, 39, 0.00, 0, ''),
(299, 18, 151, 39, 130.00, 0, '');

CREATE TABLE IF NOT EXISTS `StoreRelatedProduct` (
`id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='Handle related products';

INSERT INTO `StoreRelatedProduct` (`id`, `product_id`, `related_id`) VALUES
(71, 37, 38),
(70, 37, 41),
(69, 37, 43);

CREATE TABLE IF NOT EXISTS `StoreTypeAttribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `StoreTypeAttribute` (`type_id`, `attribute_id`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 7),
(3, 8),
(3, 9),
(4, 10),
(4, 11),
(4, 12),
(5, 13),
(5, 14),
(5, 15),
(5, 16),
(6, 17),
(6, 18),
(6, 19);

CREATE TABLE IF NOT EXISTS `StoreWishlist` (
`id` int(11) NOT NULL,
  `key` varchar(10) DEFAULT '',
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `StoreWishlist` (`id`, `key`, `user_id`) VALUES
(1, 'kau13rorru', 1);

CREATE TABLE IF NOT EXISTS `StoreWishlistProducts` (
`id` int(11) NOT NULL,
  `wishlist_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

INSERT INTO `StoreWishlistProducts` (`id`, `wishlist_id`, `product_id`, `user_id`) VALUES
(187, 1, 10, 1),
(186, 1, 39, 1);

CREATE TABLE IF NOT EXISTS `SystemLanguage` (
`id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `code` varchar(25) DEFAULT '',
  `locale` varchar(100) DEFAULT '',
  `default` tinyint(1) DEFAULT NULL,
  `flag_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `SystemLanguage` (`id`, `name`, `code`, `locale`, `default`, `flag_name`) VALUES
(1, 'Русский', 'ru', 'ru', 1, 'ru.png'),
(9, 'English', 'en', 'en', 0, 'us.png');

CREATE TABLE IF NOT EXISTS `SystemModules` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

INSERT INTO `SystemModules` (`id`, `name`, `enabled`) VALUES
(7, 'users', 1),
(9, 'pages', 1),
(11, 'core', 1),
(12, 'rights', 1),
(16, 'orders', 1),
(15, 'store', 1),
(17, 'comments', 1),
(37, 'feedback', 1),
(38, 'discounts', 1),
(39, 'newsletter', 1),
(40, 'csv', 1),
(41, 'logger', 1),
(54, 'notifier', 1),
(55, 'statistics', 1),
(56, 'sitemap', 1);

CREATE TABLE IF NOT EXISTS `SystemSettings` (
`id` int(11) NOT NULL,
  `category` varchar(255) DEFAULT '',
  `key` varchar(255) DEFAULT '',
  `value` text
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

INSERT INTO `SystemSettings` (`id`, `category`, `key`, `value`) VALUES
(9, 'feedback', 'max_message_length', '1000'),
(8, 'feedback', 'enable_captcha', '1'),
(7, 'feedback', 'admin_email', 'borlcand@gmail.com'),
(10, '17_WebMoneyPaymentSystem', 'LMI_PAYEE_PURSE', 'Z123456578811'),
(11, '17_WebMoneyPaymentSystem', 'LMI_SECRET_KEY', 'theSercretPassword'),
(12, '18_WebMoneyPaymentSystem', 'LMI_PAYEE_PURSE', '1'),
(13, '18_WebMoneyPaymentSystem', 'LMI_SECRET_KEY', '2'),
(14, '19_RobokassaPaymentSystem', 'login', 'login'),
(15, '19_RobokassaPaymentSystem', 'password1', 'password1value'),
(16, '19_RobokassaPaymentSystem', 'password2', 'password2value'),
(53, 'accounting1c', 'password', '825ccb588f72a136dc4ba6356c482f4e733b0efc'),
(52, 'accounting1c', 'ip', '127.0.0.1'),
(24, 'yandexMarket', 'name', 'Демо магазин'),
(25, 'yandexMarket', 'company', 'Демо кампания'),
(26, 'yandexMarket', 'url', 'http://demo-company.loc/'),
(27, 'yandexMarket', 'currency_id', '2'),
(28, 'core', 'siteName', 'Home Shop'),
(29, 'core', 'productsPerPage', '9,12,18,24'),
(30, 'core', 'productsPerPageAdmin', '30'),
(31, 'core', 'theme', 'default'),
(32, '20_QiwiPaymentSystem', 'shop_id', ''),
(33, '20_QiwiPaymentSystem', 'password', ''),
(34, '21_QiwiPaymentSystem', 'shop_id', '211182'),
(35, '21_QiwiPaymentSystem', 'password', 'xsi100500'),
(36, 'core', 'editorTheme', 'complete'),
(37, 'core', 'editorHeight', '150'),
(38, 'core', 'editorAutoload', '1'),
(39, 'images', 'path', 'webroot.uploads.product'),
(40, 'images', 'thumbPath', 'webroot.assets.productThumbs'),
(41, 'images', 'url', '/uploads/product/'),
(42, 'images', 'thumbUrl', '/assets/productThumbs/'),
(43, 'images', 'maxFileSize', '10485760'),
(44, 'images', 'max_sizes', '1800х1600'),
(45, 'images', 'maximum_image_size', '1935x947'),
(46, 'images', 'watermark_image', ''),
(47, 'images', 'watermark_active', '1'),
(48, 'images', 'watermark_position', 'bottomRight'),
(49, 'images', 'watermark_position_vertical', 'bottom'),
(50, 'images', 'watermark_position_horizontal', 'right'),
(51, 'images', 'watermark_opacity', '100'),
(54, 'accounting1c', 'tempdir', 'application.runtime');

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1361214193),
('m130218_190341_add_description_to_store_category', 1361214373),
('m130218_190818_add_description_to_store_category_translate', 1361214547),
('m130420_194012_add_roles_to_discounts', 1366487103),
('m130420_204956_add_personal_discount_to_user', 1366491054),
('m130421_095545_add_personal_discount_to_product', 1366538394),
('m130504_170119_add_discout_to_order', 1367686940),
('m130504_183903_add_title_to_store_product_image', 1367692811),
('m130507_103455_add_banned_to_user', 1367923070),
('m130507_104810_unban_all_users', 1367923771),
('m130624_155800_add_new_fields_to_product', 1372089566),
('m130714_114907_add_admin_comment_to_orders', 1373802668),
('m130726_042212_create_order_history_table', 1374814430);

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `recovery_key` varchar(20) DEFAULT NULL,
  `recovery_password` varchar(100) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Saves user accounts';

INSERT INTO `user` (`id`, `username`, `password`, `email`, `created_at`, `last_login`, `login_ip`, `recovery_key`, `recovery_password`, `discount`, `banned`) VALUES
(1, 'root', 'a482f2167dfb78e1c2a3b11f7e27ad97caf365af', 'borlcand@gmail.com', '2015-05-19 02:37:04', '2015-06-17 00:47:42', '127.0.0.1', NULL, NULL, NULL, 0),
(2, 'customer1', '8cb2237d0679ca88db6464eac60da96345513964', 'customer1@bitmarket.me', '2015-06-04 18:05:46', '2015-06-04 18:07:08', '127.0.0.1', NULL, NULL, NULL, 0);

CREATE TABLE IF NOT EXISTS `user_profile` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT '',
  `phone` varchar(20) DEFAULT NULL,
  `delivery_address` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `user_profile` (`id`, `user_id`, `full_name`, `phone`, `delivery_address`) VALUES
(1, 1, 'root', '', ''),
(2, 2, 'Customer 1', '+375-29-123-4567', 'Минск, ул. Орловская д.21 кв.25');


ALTER TABLE `accounting1c`
 ADD PRIMARY KEY (`id`), ADD KEY `object_type` (`object_type`), ADD KEY `external_id` (`external_id`);

ALTER TABLE `ActionLog`
 ADD PRIMARY KEY (`id`), ADD KEY `username` (`username`), ADD KEY `event` (`event`), ADD KEY `datetime` (`datetime`), ADD KEY `model_name` (`model_name`);

ALTER TABLE `AuthAssignment`
 ADD PRIMARY KEY (`itemname`,`userid`);

ALTER TABLE `AuthItem`
 ADD PRIMARY KEY (`name`);

ALTER TABLE `AuthItemChild`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

ALTER TABLE `Comments`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `class_name_index` (`class_name`);

ALTER TABLE `Discount`
 ADD PRIMARY KEY (`id`), ADD KEY `active` (`active`), ADD KEY `start_date` (`start_date`), ADD KEY `end_date` (`end_date`);

ALTER TABLE `DiscountCategory`
 ADD PRIMARY KEY (`id`), ADD KEY `discount_id` (`discount_id`), ADD KEY `category_id` (`category_id`);

ALTER TABLE `DiscountManufacturer`
 ADD PRIMARY KEY (`id`), ADD KEY `discount_id` (`discount_id`), ADD KEY `manufacturer_id` (`manufacturer_id`);

ALTER TABLE `grid_view_filter`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`);

ALTER TABLE `Order`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `secret_key` (`secret_key`), ADD KEY `delivery_id` (`delivery_id`), ADD KEY `status_id` (`status_id`);

ALTER TABLE `OrderHistory`
 ADD PRIMARY KEY (`id`), ADD KEY `order_index` (`order_id`), ADD KEY `created_index` (`created`);

ALTER TABLE `OrderProduct`
 ADD PRIMARY KEY (`id`), ADD KEY `order_id` (`order_id`), ADD KEY `product_id` (`product_id`), ADD KEY `configurable_id` (`configurable_id`);

ALTER TABLE `OrderStatus`
 ADD PRIMARY KEY (`id`), ADD KEY `position` (`position`);

ALTER TABLE `Page`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `category_id` (`category_id`), ADD KEY `url` (`url`), ADD KEY `created` (`created`), ADD KEY `updated` (`updated`), ADD KEY `publish_date` (`publish_date`), ADD KEY `status` (`status`);

ALTER TABLE `PageCategory`
 ADD PRIMARY KEY (`id`), ADD KEY `parent_id` (`parent_id`), ADD KEY `url` (`url`), ADD KEY `created` (`created`), ADD KEY `updated` (`updated`);

ALTER TABLE `PageCategoryTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `PageTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `Rights`
 ADD PRIMARY KEY (`itemname`);

ALTER TABLE `StoreAttribute`
 ADD PRIMARY KEY (`id`), ADD KEY `use_in_filter` (`use_in_filter`), ADD KEY `display_on_front` (`display_on_front`), ADD KEY `position` (`position`), ADD KEY `use_in_variants` (`use_in_variants`), ADD KEY `use_in_compare` (`use_in_compare`), ADD KEY `name` (`name`);

ALTER TABLE `StoreAttributeOption`
 ADD PRIMARY KEY (`id`), ADD KEY `attribute_id` (`attribute_id`), ADD KEY `position` (`position`);

ALTER TABLE `StoreAttributeOptionTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `language_id` (`language_id`), ADD KEY `object_id` (`object_id`);

ALTER TABLE `StoreAttributeTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `StoreCategory`
 ADD PRIMARY KEY (`id`), ADD KEY `lft` (`lft`), ADD KEY `rgt` (`rgt`), ADD KEY `level` (`level`), ADD KEY `url` (`url`), ADD KEY `full_path` (`full_path`);

ALTER TABLE `StoreCategoryTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`), ADD KEY `name` (`name`);

ALTER TABLE `StoreCurrency`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `StoreDeliveryMethod`
 ADD PRIMARY KEY (`id`), ADD KEY `position` (`position`);

ALTER TABLE `StoreDeliveryMethodTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `StoreDeliveryPayment`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `StoreManufacturer`
 ADD PRIMARY KEY (`id`), ADD KEY `url` (`url`);

ALTER TABLE `StoreManufacturerTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `StorePaymentMethod`
 ADD PRIMARY KEY (`id`), ADD KEY `currency_id` (`currency_id`);

ALTER TABLE `StorePaymentMethodTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `StoreProduct`
 ADD PRIMARY KEY (`id`), ADD KEY `manufacturer_id` (`manufacturer_id`), ADD KEY `type_id` (`type_id`), ADD KEY `price` (`price`), ADD KEY `max_price` (`max_price`), ADD KEY `is_active` (`is_active`), ADD KEY `sku` (`sku`), ADD KEY `created` (`created`), ADD KEY `updated` (`updated`), ADD KEY `added_to_cart_count` (`added_to_cart_count`), ADD KEY `views_count` (`views_count`);

ALTER TABLE `StoreProductAttributeEAV`
 ADD KEY `ikEntity` (`entity`), ADD KEY `attribute` (`attribute`), ADD KEY `value` (`value`(50));

ALTER TABLE `StoreProductCategoryRef`
 ADD PRIMARY KEY (`id`), ADD KEY `category` (`category`), ADD KEY `product` (`product`), ADD KEY `is_main` (`is_main`);

ALTER TABLE `StoreProductConfigurableAttributes`
 ADD UNIQUE KEY `product_attribute_index` (`product_id`,`attribute_id`);

ALTER TABLE `StoreProductConfigurations`
 ADD UNIQUE KEY `idsunique` (`product_id`,`configurable_id`);

ALTER TABLE `StoreProductImage`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `StoreProductTranslate`
 ADD PRIMARY KEY (`id`), ADD KEY `object_id` (`object_id`), ADD KEY `language_id` (`language_id`);

ALTER TABLE `StoreProductType`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `StoreProductVariant`
 ADD PRIMARY KEY (`id`), ADD KEY `attribute_id` (`attribute_id`), ADD KEY `option_id` (`option_id`), ADD KEY `product_id` (`product_id`);

ALTER TABLE `StoreRelatedProduct`
 ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`);

ALTER TABLE `StoreTypeAttribute`
 ADD PRIMARY KEY (`type_id`,`attribute_id`);

ALTER TABLE `StoreWishlist`
 ADD PRIMARY KEY (`id`), ADD KEY `key` (`key`), ADD KEY `user_id` (`user_id`);

ALTER TABLE `StoreWishlistProducts`
 ADD PRIMARY KEY (`id`), ADD KEY `wishlist_id` (`wishlist_id`), ADD KEY `product_id` (`product_id`), ADD KEY `user_id` (`user_id`);

ALTER TABLE `SystemLanguage`
 ADD PRIMARY KEY (`id`), ADD KEY `code` (`code`);

ALTER TABLE `SystemModules`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `SystemSettings`
 ADD PRIMARY KEY (`id`), ADD KEY `category` (`category`), ADD KEY `key` (`key`);

ALTER TABLE `tbl_migration`
 ADD PRIMARY KEY (`version`);

ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `user_profile`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);


ALTER TABLE `accounting1c`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ActionLog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=147;
ALTER TABLE `Comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `Discount`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `DiscountCategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=391;
ALTER TABLE `DiscountManufacturer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
ALTER TABLE `grid_view_filter`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `notifications`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Order`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `OrderHistory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `OrderProduct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=110;
ALTER TABLE `OrderStatus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `Page`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
ALTER TABLE `PageCategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
ALTER TABLE `PageCategoryTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
ALTER TABLE `PageTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
ALTER TABLE `StoreAttribute`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
ALTER TABLE `StoreAttributeOption`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=163;
ALTER TABLE `StoreAttributeOptionTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=325;
ALTER TABLE `StoreAttributeTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
ALTER TABLE `StoreCategory`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=245;
ALTER TABLE `StoreCategoryTranslate`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
ALTER TABLE `StoreCurrency`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `StoreDeliveryMethod`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
ALTER TABLE `StoreDeliveryMethodTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `StoreDeliveryPayment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
ALTER TABLE `StoreManufacturer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
ALTER TABLE `StoreManufacturerTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
ALTER TABLE `StorePaymentMethod`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
ALTER TABLE `StorePaymentMethodTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
ALTER TABLE `StoreProduct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
ALTER TABLE `StoreProductCategoryRef`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=322;
ALTER TABLE `StoreProductImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=185;
ALTER TABLE `StoreProductTranslate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=355;
ALTER TABLE `StoreProductType`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `StoreProductVariant`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=300;
ALTER TABLE `StoreRelatedProduct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
ALTER TABLE `StoreWishlist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `StoreWishlistProducts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=188;
ALTER TABLE `SystemLanguage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `SystemModules`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
ALTER TABLE `SystemSettings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `user_profile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;