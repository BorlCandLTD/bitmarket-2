<?php

/**
 * @var $this SFilterRenderer
 */

/**
 * Render filters based on the next array:
 * $data[attributeName] = array(
 *	    'title'=>'Filter Title',
 *	    'selectMany'=>true, // Can user select many filter options
 *	    'filters'=>array(array(
 *	        'title'      => 'Title',
 *	        'count'      => 'Products count',
 *	        'queryKey'   => '$_GET param',
 *	        'queryParam' => 'many',
 *	    ))
 *  );
 */

// Render active filters
$active = $this->getActiveFilters();
if(!empty($active))
{
		echo CHtml::openTag('label');
		echo CHtml::encode(Yii::t('StoreModule.core', 'Текущие фильтры'));
		echo CHtml::closeTag('label');
		
		echo CHtml::closeTag('br');
		
		//var_dump($active);
		//var_dump($this->activeFiltersHtmlOptions);
		for ($i = 0; $i < count($active); $i++)
		{
			$filter = $active[$i];
			
			$url = $filter['url'];
			
			echo CHtml::checkBox('activeattrib'.$i, true, array('onchange' => "window.location='$url'"));
			echo CHtml::label( $filter['label'], 'activeattrib'.$i);
			
			echo CHtml::closeTag('br');
		}
			
		echo CHtml::closeTag('br');
		
		echo CHtml::form($this->getOwner()->model->viewUrl);
		echo CHtml::submitButton(Yii::t('StoreModule.core','Сбросить фильтр'), array('class'=>'dark-blue'));
		echo CHtml::endform();
		
		echo CHtml::closeTag('br');
}
?>
	
<?php
	$cm=Yii::app()->currency;
	/*echo $this->widget('zii.widgets.jui.CJuiSlider', array(
		'options'=>array(
			'range'=>true,
			'min'=>(int)floor($cm->convert($this->controller->getMinPrice())),
			'max'=>(int)ceil($cm->convert($this->controller->getMaxPrice())),
			'disabled'=>(int)$this->controller->getMinPrice()===(int)$this->controller->getMaxPrice(),
			'values'=>array($this->currentMinPrice, $this->currentMaxPrice),
			'slide'=>'js: function( event, ui ) {
				$("#min_price").val(ui.values[0]);
				$("#max_price").val(ui.values[1]);
			}',
		),
		'htmlOptions'=>array(
			'style'=>'margin:5px',
		),
	), true);*/
?>
<?php echo CHtml::form() ?>

	
	<label>
		<?php echo Yii::t('StoreModule.core', 'Цена').' (в '.$cm->active->symbol.')'; ?>
	</label>
	<div class="noUiSlider" data-min-range="<?php echo (int)floor($cm->convert($this->controller->getMinPrice())); ?>" data-max-range="<?php echo (int)ceil($cm->convert($this->controller->getMaxPrice())); ?>" data-min-value="<?php echo $this->currentMinPrice; ?>" data-max-value="<?php echo $this->currentMaxPrice; ?>"></div>
	<span class="price-range-min"></span>
	<span class="price-range-max"></span>
	
	<br/>
	<br/>
	<input type="submit" value="Применить">
<?php echo CHtml::endForm() ?>


<?php
if(!empty($manufacturers['filters']) || !empty($attributes))

	// Render manufacturers
	if(!empty($manufacturers['filters']))
	{
		echo CHtml::closeTag('br');
		
		echo CHtml::openTag('label');
		echo CHtml::encode(Yii::t('StoreModule.core', 'Производитель'));
		echo CHtml::closeTag('label');
		
		echo CHtml::closeTag('br');

		foreach($manufacturers['filters'] as $filter)
		{
			$url = Yii::app()->request->addUrlParam('/store/category/view', array($filter['queryKey'] => $filter['queryParam']), $manufacturers['selectMany']);
			$queryData = explode(';', Yii::app()->request->getQuery($filter['queryKey']));
			
			// Filter link was selected.
			if(in_array($filter['queryParam'], $queryData))
			{
				// Create link to clear current filter
				$url = Yii::app()->request->removeUrlParam('/store/category/view', $filter['queryKey'], $filter['queryParam']);
				
				echo CHtml::checkBox('man'.$filter['queryParam'], true, array('onchange' => "window.location='$url'"));
				echo CHtml::label( $filter['title'].' <span class="grey">('.($filter['count'] > 0 ? $filter['count'] : 0).')</span>', 'man'.$filter['queryParam']);
			}
			else {
				echo CHtml::checkBox('man'.$filter['queryParam'], false, array('onchange' => "window.location='$url'"));
				echo CHtml::label( $filter['title'].' <span class="grey">('.($filter['count'] > 0 ? $filter['count'] : 0).')</span>', 'man'.$filter['queryParam']);
			}
			
			echo CHtml::closeTag('br');
		}
	}

	// Display attributes
	foreach($attributes as $attrData)
	{
		//var_dump($attrData);
		echo CHtml::closeTag('br');
		
		echo CHtml::openTag('label');
		echo CHtml::encode($attrData['title']);
		echo CHtml::closeTag('label');
		
		echo CHtml::closeTag('br');
		
		foreach($attrData['filters'] as $filter)
		{
			$url = Yii::app()->request->addUrlParam('/store/category/view', array($filter['queryKey'] => $filter['queryParam']), $attrData['selectMany']);
			$queryData = explode(';', Yii::app()->request->getQuery($filter['queryKey']));

			// Filter link was selected.
			if(in_array($filter['queryParam'], $queryData))
			{
				// Create link to clear current filter
				$url = Yii::app()->request->removeUrlParam('/store/category/view', $filter['queryKey'], $filter['queryParam']);
				
				echo CHtml::checkBox('attr'.$filter['queryParam'], true, array('onchange' => "window.location='$url'"));
				echo CHtml::label( $filter['title'].' <span class="grey">('.($filter['count'] > 0 ? $filter['count'] : 0).')</span>', 'attr'.$filter['queryParam']);
			}
			else {
				echo CHtml::checkBox('attr'.$filter['queryParam'], false, array('onchange' => "window.location='$url'"));
				echo CHtml::label( $filter['title'].' <span class="grey">('.($filter['count'] > 0 ? $filter['count'] : 0).')</span>', 'attr'.$filter['queryParam']);
			}

			echo CHtml::closeTag('br');
		}
	}

//if(!empty($manufacturers['filters']) || !empty($attributes))
	//echo CHtml::closeTag('div');