<?php
/**
 * SListView class file.
 */

Yii::import('system.web.widgets.pagers.CLinkPager');

class SLinkPager extends CLinkPager
{
	public $internalPageCssClass = 'page-button';
	
	public $nextPageLabel = '<i class="icons icon-right-dir"></i>';
	public $prevPageLabel = '<i class="icons icon-left-dir"></i>';
	public $firstPageLabel = false;
	public $lastPageLabel = false;
	
	public $header = '';

	public function run()
	{
		$this->registerClientScript();
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
		echo $this->header;
		echo CHtml::tag('div',$this->htmlOptions,implode("\n",$buttons));
		echo $this->footer;
	}
	
	protected function createPageButton($label,$page,$class,$hidden,$selected)
	{
		if($hidden || $selected)
			$class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
		return CHtml::link('<div class="'.$class.'">'.$label.'</div>',$this->createPageUrl($page));
	}
}
