<?php

$this->layout = '//layouts/layout1';

?>

<?php if (YII_DEBUG===true && $error): ?>
	<h2><?=$error['type']?></h2>

	<div class="error">
		<?=$error['message']?><br><br>
	</div>

<?php else: ?>

	<h2><?=Yii::t('core','Ошибка')?></h2>

	<div class="error">
		<?=Yii::t('core','Ошибка обработки запроса.')?><br><br>
	</div>
<?php endif ?>